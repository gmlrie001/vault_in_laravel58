{!! Form::label($name, $label) !!}
{!! Form::text($name, null, array('placeholder' => $label, 'class' => 'form-control font-awesome-picker')) !!}