<?php namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Support\Facades\Schema;

class TableExport implements FromQuery
{
    use Exportable;
    protected $model = null;

    function __construct($tableName) {
        if(Schema::hasTable($tableName)) {
            $tableNameSingular = studly_case(strtolower(str_singular($tableName)));

            $modelNamespace = str_replace(["{", "}"], ["", ""], "App\Models\{$tableNameSingular}");

			try  {
                $this->model = (new $modelNamespace);
            } catch (Exception $e){
                Session::flash('error', 'Model not found');
                Redirect::to('/vault')->send();
            }
        }else{
            Session::flash('error', 'Table does not exist');
            Redirect::to('/vault')->send();
        }
    }
    
    public function query()
    {
        return $this->model::query();
    }
}

