<?php

  return [

    'Blogs' => [

      'title' => 'Blog',
      'page_id' => '7',
      'icon' => 'fa fa-feed',
  
      'main_link_database_tables' => [
        'blogs' => [
          'title' => 'Posts',
          'icon' => 'fa fa-newspaper',
          'specific_id' => '',
          'sub_database_tables' => [
            'blog_galleries',
            // 'blog_share_links',
          ]
        ],
      ],
      'tab_database_tables' => [
        'blog_galleries',
        // 'blog_share_links',
      ]

    ],

  ]['Blogs'];
