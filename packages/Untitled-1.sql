https://developers.google.com/fonts/docs/developer_api#details

Details
The JSON response (refer to sample above) is composed of an array named "items" which contains objects with information about each font family.

A family object is composed of 4 fields:

kind: The kind of object, a webfont object
family: The name of the family
subsets: A list of scripts supported by the family
variants: The different styles available for the family
version: The font family version.
lastModified: The date (format "yyyy-MM-dd") the font family was modified for the last time.
files: The font family files (with all supported scripts) for each one of the available variants.
By combining the information for each family it is easy to create a Fonts API request. For example assuming we have a reference to the family object for Anonymous Pro:

[code]

var apiUrl = [];
apiUrl.push('https://fonts.googleapis.com/css?family=');
apiUrl.push(anonymousPro.family.replace(/ /g, '+'));
if (contains('italic', anonymousPro.variants)) {
  apiUrl.push(':');
  apiUrl.push('italic');
}
if (contains('greek', anonymousPro.subsets)) {
  apiUrl.push('&subset=');
  apiUrl.push('greek');
}

// url: 'https://fonts.googleapis.com/css?family=Anonymous+Pro:italic&subset=greek'
var url = apiUrl.join('');

[code]

The list of families is returned in no particular order by default. It is possible however to sort the list using the sort parameter:

https://www.googleapis.com/webfonts/v1/webfonts?sort=popularity

The possible sorting values are:

alpha: Sort the list alphabetically
date: Sort the list by date added (most recent font added or updated first)
popularity: Sort the list by popularity (most popular family first)
style: Sort the list by number of styles available (family with most styles first)
trending: Sort the list by families seeing growth in usage (family seeing the most growth first)
