<?php

if (!file_exists(__DIR__.'/../.env') && (isset($_ENV['APP_KEY']) || empty($_ENV))) {
    $handle = shell_exec('cd .. && copy .env.vault .env && php artisan key:generate --ansi && cd public');
    dd($handle);
}
