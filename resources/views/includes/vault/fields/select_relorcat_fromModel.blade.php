<?php
    if (isset($use_parent)) {
        $tableNameSingular = studly_case(strtolower(str_singular($entry->parentTable)));

        $parentModelNamespace = str_replace(["{", "}"], ["", ""], "App\Models\{$tableNameSingular}");
        $parentModel = (new $parentModelNamespace);
        $parent = $parentModel::find(Session::get('relation_id'));

        $parentField = $parent->parentOrder;
        $parentValue = $parent->{$parentField};

        $modelNamespace = str_replace(["{", "}"], ["", ""], "App\Models\{$options}");
        $model = (new $modelNamespace);
    
        $options = $model::where($parentField, $parentValue)->pluck('title', 'id')->all();
    } else {
        $modelNamespace = str_replace(["{", "}"], ["", ""], "App\Models\{$options}");
        $model = (new $modelNamespace);
    
        $options = $model::pluck('title', 'id')->all();
    }
?>
{!! Form::label($name, $label) !!}
{!! Form::select($name, $options, null, array('placeholder' => $label, 'class' => 'form-control')) !!}

<script>
    $('#{{$name}}').ddslick();    
</script>

<script>
    var elm = document.querySelector( 'select#{{$name}}' );
    console.log( elm );
    elm.addEventListener( 'change', function(event) {
        event.stopPropagation();
        var n = document.querySelector( 'input[name=title]' );
        console.log( n );
        var e = [].slice.call( this.selectedOptions )
        n.value = e[0].innerText;
        console.log( n );
        console.log( e);
    }, false );
</script>
