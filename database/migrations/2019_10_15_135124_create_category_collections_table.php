<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryCollectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_collections', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('title')->nullable();

            $table->integer('category_id')->unsigned()->nullable();
            
            $table->integer('collection_id')->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->enum('status', ['PUBLISHED', 'UNPUBLISHED', 'DRAFT', 'SCHEDULED'])->default('PUBLISHED')->nullable();
            $table->dateTime('status_date')->nullable();
            $table->integer('order')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_collections');
    }
}
