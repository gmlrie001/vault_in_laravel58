<?php namespace App\Http\Controllers\Vault;

/* Base Controller Include */
use App\Http\Controllers\Services\VaultController;

/* Facade Includes */
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
// use Illuminate\Support\Facades\Arr;
use Illuminate\Database\Eloquent\Collection;


/* Support */
use Illuminate\Support\Arr;

/* HTTP Request */
use Illuminate\Http\Request;

/* Google Fonts Model */
use App\Models\GoogleFont;

/* Helper Class */
use App\Helpers\GoogleFonts\GoogleFontsHelper;

class GoogleFontController extends VaultController
{
    public $dbTable;
    private $json_file_path;

    public function __construct()
    {
        parent::__construct();

        $this->dbTable = new GoogleFont;

        $this->json_file_path = 'assets/json/test.json';

        return $this;
    }

    /* Listing within the Vault */
    public function __invoke()
    {
        if ($this->dbTable::count() > 0) {
            $font_collection = $this->dbTable::status();
            $font_db = $font_collection->paginate($this->paginateLimit($font_collection->get()));
            $this->data['font_db'] = $font_db;

            list(
              $this->data['categories'],
              $this->data['variants'],
              $this->data['subsets']
            ) = self::setupFilters($font_collection->get());

            return view('includes.pages.cards', $this->data);
        } else {
            return $this->update_local();
        }
    }

    /* Update Vault Google Fonts DB via the API */
    public function update()
    {
        $this->perf_stats('start');

        include(app_path('Helpers/GoogleFonts/GoogleFontsHelper.php'));
        $pth = 'assets/json/test.json';
        $googleFonts = new GoogleFontsHelper($pth);

        unset($pth);
        $googleFonts->set_api_key('AIzaSyAvMVD7pM9FyquH84i-CiBddZ0IAiFfAMk');

        /* BEWARE: The fetch_from_api method will overwrite the local copy if DB path not reset */
        if (! $googleFonts->font_file_exists()) {
            // $path = explode('.', $googleFonts->get_localPath());
            // $extn = array_pop($path);
            // $googleFonts->set_localPath($path[0].'_'.date('YmdHis', strtotime(now())).'.'.$extn);
            // unset($path, $extn);
            $googleFonts->fetch_from_api();
            $contents = file_get_contents($googleFonts->get_localPath());
            $result   = json_decode($contents, true);
        } else {
            $contents = file_get_contents(public_path($googleFonts->get_localPath()));
            $result   = json_decode($contents, true);
        }

        if ($result != null && (gettype($result) == 'array' || gettype($result) == 'object')) {
            $fonts = json_decode($googleFonts->get_contents(), true);
            $font_collection = $fonts['items'];

            $font_db = new $this->dbTable;
            foreach ($font_collection as $key=>$font) {
                $db = $font_db::updateOrCreate(['family' => $font['family']], $font);
                unset($db);
            }

            unset($contents, $result);
            $this->perf_stats();
            return Redirect::to('/parse_google_fonts_file')->with('message', 'Successfully updated fonts DB!');
        }

        unset($contents, $result);
        $this->perf_stats();
        return Redirect::to('/parse_google_fonts_file')->with('error', 'Error while attempting to update fonts DB!');
    }

    /**
     * Update Vault Google Fonts DB from a local file copy of previous API call.
     * Assumes that google fonts JSON file exists on the path:
     *    /assets/json/google_fonts(_YmdHis)?.json
     */
    public function update_local()
    {
        $pth = $this->json_file_path;
        $googleFonts = new GoogleFontsHelper($pth, 'AIzaSyAvMVD7pM9FyquH84i-CiBddZ0IAiFfAMk');
        unset($pth);

        $googleFonts->set_api_key();

        if (! $googleFonts->font_file_exists()) {
            $path = explode('.', $googleFonts->get_localPath());
            $extn = array_pop($path);
            $googleFonts->set_localPath($path[0].'_'.date('YmdHis', strtotime(now())).'.'.$extn);
            unset($path, $extn);
        } else {
            /* Attempt to LOAD data from recent Google Fonts JSON file stored locally */
            $contents = file_get_contents(
                public_path($googleFonts->get_localPath())
            );
            $result   = json_decode($contents, true);
        }

        if ($result != null && (gettype($result) == 'array' || gettype($result) == 'object')) {
            $fonts = $result;// $fonts = json_decode($googleFonts->get_contents(), true);
            $font_collection = $fonts['items'];
            unset($contents, $result);

            $font_db = new $this->dbTable;
            foreach ($font_collection as $key=>$font) {
                $db = $font_db::updateOrCreate(['family' => $font['family']], $font);
            }

            $this->perf_stats();
            if ($font_db != null && $db != null) {
                return Redirect::to('/parse_google_fonts_file')->with('message', 'Successfully updated fonts DB!');
            } else {
                return Redirect::to('/parse_google_fonts_file')->with('error', 'Error while attempting to update fonts DB!');
            }
        } else {
            return Redirect::to('/parse_google_fonts_file')->with('error', 'Something went wrong while attempting to update fonts DB!');
        }
        
        $this->perf_stats();
        return Redirect::back()->with('error', 'Something really, really went wrong!');
    }

    private function get_rowCount($collectn = [])
    {
        return count($collectn);
    }

    private function paginateLimit($collectn = [])
    {
        $tenth = floor($collectn->count() * 0.1);

        return (int) $tenth + $tenth % 3 + 1;
    }

    /* Update Vault Google Fonts DB via the API */
    public function filter(Request $request)
    {
        $input_filters = $request->all();
        $categoryName  = (isset($input_filters['category'])) ? $input_filters['category'] : null;
        $variant_query = (isset($input_filters['variants'])) ? $input_filters['variants'] : null;
        $subset_query  = (isset($input_filters['subsets'])) ? $input_filters['subsets'] : null;

        $filtered = $this->dbTable::status()
              ->where(
                  function ($query) use ($categoryName, $variant_query, $subset_query) {
                      $query->where('category', 'LIKE', $categoryName)
                            ->orWhereJsonContains('variants', $variant_query)
                            ->orWhereJsonContains('subsets', $subset_query);
                  }
              );
        $this->data['font_db'] = $filtered->paginate(
            $this->paginateLimit($filtered->get())
        )->appends($input_filters);

        list(
          $this->data['categories'],
          $this->data['variants'],
          $this->data['subsets']
        ) = self::setupFilters($filtered->get());
  
        return view('includes.pages.cards', $this->data);
    }

    /* Update Vault Google Fonts DB via the API */
    public function search(Request $request)
    {
        $input_filters = $request->all();
        $search_query = (isset($input_filters['query'])) ? $input_filters['query'] : null;

        $filtered = $this->dbTable::status()
              ->where(
                  function ($query) use ($search_query) {
                      $query->where('family', 'LIKE', '%' . e($search_query) . '%');
                  }
              );
    
        $this->data['font_db'] = $filtered->paginate(36)->appends($input_filters);
        list(
          $this->data['categories'],
          $this->data['variants'],
          $this->data['subsets']
        ) = self::setupFilters($filtered->get());

        return view('includes.pages.cards', $this->data);
    }

    public function ordering($col, $dir)
    {
        $filtered = $this->dbTable::status()->orderBy($col, $dir);
        $this->data['font_db'] = $filtered->paginate(36);

        list(
          $this->data['categories'],
          $this->data['variants'],
          $this->data['subsets']
        ) = self::setupFilters($filtered->get());

        return view('includes.pages.cards', $this->data);
    }

    /* Static Method for generating the FILTER Fields */
    private static function setupFilters($fontsCollection)
    {
        if ($fontsCollection != null && $fontsCollection instanceof Collection) {
            $fonts = $fontsCollection;

            $categories = [];
            $variants = [];
            $subsets = [];

            foreach ($fonts as $key=>$font) {
                $categories[] = $font->category;
                $variants[]   = $font->variants;
                $subsets[]    = $font->subsets;
            }

            $unique_categories = array_values(array_unique($categories));
            $unique_variants   = array_unique(array_flatten($variants));
            $unique_subsets    = array_unique(array_flatten($subsets));
  
            return [
              Arr::sort($unique_categories),
              Arr::sort(array_values($unique_variants)),
              Arr::sort(array_values($unique_subsets))
            ];
        };

        return;
    }

    private function perf_stats($when='end')
    {
        $perfstats = [
          'Current Timestamp'    => time(),
          'Current Memory Usage' => memory_get_usage()
        ];

        if (in_array($when, ['start', 'begin', 'introduction', 'go', 'initial'])) {
            $start = time();
            $this->data['start'] = $start;
            unset($start);

            $this->data['memini'] = memory_get_usage();
        } elseif (in_array($when, ['end', 'finish', 'conclude', 'stop', 'term'])) {
            $end = time();
            $this->data['end'] = $end;
            unset($end);

            $this->data['memend'] = memory_get_usage();

            $this->data['time_taken'] = time() - $this->data['start'];
        }

        $perfstats['CurrentSystemCPULoad'] = ($this->is_os_windows()) ? null : $this->get_load();
            
        echo('<pre>');
        print_r($perfstats);
        echo('</pre>');

        return;
    }

    private function is_os_windows()
    {
        return strtoupper(substr(PHP_OS, 0, 3)) == 'WIN' ||
                strtoupper(substr(php_uname('s'), 0, 3)) == 'WIN';
    }

    private function get_load()
    {
        return sys_getloadavg();
    }
}
