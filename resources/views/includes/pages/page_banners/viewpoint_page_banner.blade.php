

@if( Request::is('/') )

    @if($banner_image || $mobile_banner_image)
        @if( $banner_image )
    <section class="container-fluid d-none d-sm-block px-0 position-relative home-banner">
        <div>
            <a href="/" title="link to other page">
                <img class="img-fluid" src="{{ asset( $banner_image ) }}" alt="Viewpoint Home Banner">
            </a>
        </div>
    </section>
        @endif
        @if( $mobile_banner_image )
    <section class="container-fluid d-block d-sm-none px-0 position-relative home-banner">
        <div>
            <a href="/" title="link to other page"><img class="img-fluid" src="{{ asset( $mobile_banner_image ) }}" alt=""></a>
        </div>
    </section>
        @endif
    @else
    <section class="container-fluid px-0 position-relative page-banner">
        <div class="container">
            <div class="col-12 position-absolute banner-text">
                @if($title_icon)
                <img class="img-fluid d-inline" src="{{ $title_icon }}" alt="what we do">
                @endif
                <h1 class="d-inline">{{ $page_title }}</h1>
            </div>
        </div>
    </section>
    @endif

@else
    <section class="container-fluid px-0 position-relative page-banner">
        <div class="container">
            <div class="col-12 position-absolute banner-text">
                @if($title_icon)
                <img class="img-fluid d-inline" src="{{ $title_icon }}" alt="what we do">
                @endif
                <h1 class="d-inline">{{ $page_title }}</h1>
            </div>
        </div>
    </section>
    @endif

@endif
