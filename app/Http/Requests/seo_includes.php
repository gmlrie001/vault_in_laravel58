<?php

return [
  'seo_title'       => 'required|string',
  'seo_description' => 'required|string',
  'seo_keywords'    => 'required|regex:/^(.+,?.+)?$/isgU'
];
