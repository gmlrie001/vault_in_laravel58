<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;

class Blog extends Model
{
    use SoftDeletes;
    
    public $orderable = false;
    public $orderField = "pubdate";
    public $titleField = "title";
    public $statusField = "status";
    public $hasStatus = true;
    public $orderDirection = "desc";
    public $parentOrder = "";
    public $parentTable = "";
    public $orderOptions = ['title', 'updated_at'];
    public $relationships = [
        'blog_galleries' => 'galleries',
        'blog_share_links' => 'share_links',
    ];
    public $mainDropdownField = "url_title";
    public $imageDropdownField = "listing_image";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'url_title',
        'pubdate',
        // 'listing_description',
        'description',
        'listing_image',
        'article_image',
        'seo_title',
        'seo_keywords',
        'seo_description',
        'status',
        'status_date',
    ];
    
    public $fields = [
    //  ['field_name', 'label', 'field_type', 'options_model', 'options_relationship', 'width', 'height', 'container_class', 'can_remove'],
        ['title', 'Title', 'title', '', '', '', '', 'col-xs-12 col-md-6', ''],
        ['url_title', 'URL Title', 'url_title', '', '', '', '', 'col-xs-12 col-md-6', ''],
        ['pubdate', 'Publication Date', 'date', '', '', '', '', 'col-xs-12 col-md-6', ''],

        ['open_parent', 'Item Desciption', ''],
            // ['open_row', '',''],
            //     ['listing_description', 'Shorter LISTING PAGE Description', 'textarea', '', '', '', '', 'col-xs-12 col-md-12', ''],
            // ['close_row', '',''],
            ['open_row', '',''],
                ['description', 'Description', 'wysiwyg', '', '', '', '', 'col-xs-12 col-md-12', ''],
            ['close_row', '',''],
        ['close_parent', 'Item Desciption', ''],

        ['open_parent', 'Listing and Article Image', ''],
            ['open_row', '',''],
                ['listing_image', 'Listing', 'image', '', '', '800', '600', 'col-xs-12 col-md-6', 'can_remove'],
                ['article_image', 'Article', 'image', '', '', '800', '600', 'col-xs-12 col-md-6', 'can_remove'],
            ['close_row', '',''],
        ['close_parent', 'Listing and Article Image', ''],

        ['open_parent', 'SEO Information', ''],
            ['open_row', '', ''],
                ['seo_title', 'SEO Title', 'text', '', '', '', '', 'col-xs-12 col-md-6', ''],
            ['close_row', '', ''],
            ['open_row', '', ''],
                ['seo_description', 'SEO Description', 'textarea', '', '', '', '', 'col-xs-12 col-md-6', ''],
                ['seo_keywords', 'SEO Keywords', 'tags', '', '', '', '', 'col-xs-12 col-md-6', ''],
            ['close_row', '', ''],
        ['close_parent', 'SEO Information', ''],

        ['open_row', '',''],
            ['status', 'Status', 'status', '', '', '', '', 'col-xs-12 col-md-6', ''],
        ['close_row', '',''],
    ];

    public function galleries()
    {
        return $this->hasMany(BlogGallery::class, 'blog_id')->where('status', 'PUBLISHED')->orWhere('status', 'SCHEDULED')
                    ->where('status_date', '<=', now());
    }

    public function share_links()
    {
        return $this->hasMany(BlogShareLink::class, 'blog_id')->where('status', 'PUBLISHED')->orWhere('status', 'SCHEDULED')
                    ->where('status_date', '<=', now());
    }

    /**
     * Scope a query to only include certain status'.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeStatus($query)
    {
        return $query->where('status', 'PUBLISHED')->orWhere('status', 'SCHEDULED')
                     ->where('status_date', '<=', now());
    }
}
