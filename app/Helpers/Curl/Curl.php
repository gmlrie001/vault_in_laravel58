<?php

namespace App\Helpers\Curl;

/* Facade Includes */
//use Illuminate\Support\Facades\DB;
//use Illuminate\Support\Facades\URL;

class Curl {
    // Communication with self or other servers

    /**
     * URL
     */
    public $url;

    /**
     * Communication method: "GET" || "POST" || "PUT" || "DELETE"
     */
    public $method;

    /**
     * Query data, setup as key-value pairs
     */
    public $data = [];

    /**
     * CURL handler
     */
    private $curlHandle;

    /**
     * Result from the communication
     */
    public $response;


    /**
     * CURL constructor:
     *   Communication with self or other servers
     * 
     */
    public function __construct( ...$opts ) {
        if ( empty( $opts ) || is_null( $opts ) ) return;

        //dd( $opts[0] );
        list( $this->url, $this->method, $this->data ) = $opts[0];
        //if ( $this->url ) $this->run();
        return $this->_curl_init();

    }

    public function set_action( $u ) {
        $this->url = $u;
    }
    public function get_action() {
        return $this->url;
    }

    public function set_method( $m = 'GET' ) {
        $this->method = $m;
    }
    public function get_method() {
        return $this->method;
    }

    public function set_data( $arr ) {
        if ( ! is_array( $arr ) || empty( $arr ) ) return;
        foreach( $arr as $k=>$v ) {
            $this->data[$k] = ( ! is_array( $v ) )
                      ? $v : $this->set_data( $v );
        }
    }
    public function get_data() {
        return $this->data;
    }

    public function run() {
        $ch = $this->_curl_init();
        if ( $this->url ) {
            $this->_set_options( $ch );
            $this->_curl_execute();
            $this->_curl_close();
        }
        return $this->response;
    }


    private function _curl_init() {
        $this->curlHandle = curl_init();
        return $this->curlHandle;
    }

    public function set_options( $addn = [] ) {
        curl_setopt( $this->curlHandle, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $this->curlHandle, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $this->curlHandle, CURLOPT_URL, $this->url );

        $this->_curl_combineOptions(
            $this->_default_options(), $this->_set_addtional_options( $addn )
        );

    }
    private function _default_options() {
        $q =  ( is_array( $this->data ) ) ? http_build_query( $this->data ) : [];

        // Default CURL options
        return [
			CURLOPT_POST           => 1,
			CURLOPT_HEADER         => 0,
			CURLOPT_URL            => $this->url,
			CURLOPT_FRESH_CONNECT  => 1,
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_FORBID_REUSE   => 1,
			CURLOPT_TIMEOUT        => 4,
			CURLOPT_POSTFIELDS     => $q
        ];
    }
    private function _set_addtional_options( $arr = [] ) {
        return array_merge( $arr, [
            CURLOPT_USERAGENT => 'MonzamediaGoogleRecaptcha/1.0 (+' . $_SERVER['HTTP_REFERER'] . ') Laravel/5'
        ] );
    }
    private function _curl_combineOptions( $default, $additional ) {
        curl_setopt_array( $this->curlHandle, ( $default + $additional ) );
    }

    private function _curl_execute() {
        $this->response = curl_exec( $this->curlHandle );
    }

    private function _curl_close() {
        curl_close( $this->curlHandle );
    }

    private function _curl_getReponse() {
        return curl_header( $this->curlHandle );
    }

    private function _handleGRResponse( $result, $response ) {
		if ( $this->response['status'] != 200 ) return;

		try {
			if ( preg_match( '/json/iU', $this->response[''], $m ) ) {
				$handler = json_decode( $result );
				$success = $handler->success;
			}
		} catch( Exception $e ) {
			echo 'Caught exception: ',  $e->getMessage(), "\n";
			$handler = [];
			$success = false;
		}

		return [ 'success' => $success, 'handler' => $handler ];
	}

}
