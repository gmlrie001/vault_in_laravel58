<!-- Site SEO -->
<meta name="keywords" content="{{---- $seo->seo_keywords --}}">
<meta name="description" content="{{-- $seo->seo_description --}}">
<meta name="canonical" content="{{-- $seo->url --}}">

<!-- Open Graph Tags -->
<meta property="og:title" content="{{-- $page->title --}}" />
<meta property="og:type" content="website" />
<meta property="og:url" content="{{-- Request::url() --}}" />

@if( ! isset( $social_img ) )
<meta property="og:image" content="{{-- url('/') . '/' . $page->featured_image --}}" />
@elseif( ! isset( $page->image ) && isset( $social_img ) )
<meta property="og:image" content="{{-- url('/') . '/' . $social_img --}}" />
@elseif( ! isset( $social_img ) && isset( $page->image ) )
<meta property="og:image" content="{{-- $page->image --}}" />
@else
<meta property="og:image" content="{{-- $site_settings->logo --}}" />
@endif

<meta property="og:description" content="{{-- strip_tags($page->seo_description) --}}" />
<meta property="og:site_name" content="{{-- $site_settings->site_name --}}" />
