<?php namespace App\Http\Controllers\Services;

/* Base Controller Include */
use App\Http\Controllers\Controller;

/* Facade Includes */
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Request;

use Config;

/* Model Includes */
use App\Models\Address;
use App\Models\Site;
use App\Models\ContactAddress as Contact;
use App\Models\Social;
use App\Models\ShareLink;

/* Request Includes */

class PageController extends Controller
{
    public $share_to = [
        'facebook',
        'twitter',
        'pinterest',
        'linkedin',
        'stumbleupon',
        'whatsapp',
        'email'
    ];
    // private $share_to  = [];

    public function __construct()
    {
        $protocol   = (! empty($_SERVER['HTTPS']) &&
            ($_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443)) ? "https://" : "http://";
        $domainName = $_SERVER['HTTP_HOST'] . '/';

        $this->data['sm_args'] =  [
            'url' => Request::url(),
            'via' => Request::url(),
            'text' => null,
            'desc' => null,
            'title' => null,
            'image' => null,
            'user_id' => null,
            'provider' => $protocol . $domainName,
            'language' => null,
            'category' => null,
            'hash_tags' => null,
            'phone_number' => null,
            'email_address' => null,
            'cc_email_address' => null,
            'bcc_email_address' => null,
        ];

        $this->middleware(function ($request, $next) {
            $this->data['site_settings'] = $this->_settings();
            $this->data['sharing_to']    = $this->share_to;

            /* Setup site locale and timezone */
            $t = $this->data['site_settings']->set_user_prefered_timezone;
            $temp   = (null !== $t) ? $t : 25;
            $array  = Config::get('locales.timezones');
            $usr_tz = $array[$temp];

            Config::set('app.timezone', $usr_tz);
            Config::set('app.locale', $this->data['site_settings']->set_user_prefered_locale);

            unset($t,  $temp, $array, $usr_tz);

            /* Global Data Required Site Wide @ All Times */
            $this->data['address']     = Contact::status()->get();
            // $this->data['breadcrumbs'] = $this->breadcrumbs(Request::segments());
            $this->data['socials']     = Social::status()->orderBy('order', 'asc')->get()->unique('social_media_url');

            // $sharingLinks              = new ShareLink();
            // $this->data['share_links'] = $sharingLinks->getSharePlatforms();
            $this->data['share_links'] = $this->share_to;

            return $next($request);
        });
    }

    private function _settings()
    {
        return Site::all()->first();
    }

    private function relations_exist($mdl=null, $rel=[])
    {
        if (null === $mdl || $mdl === '' || is_empty($rel)) {
            return;
        }
        $m = new $mdl;
        if ($rel->count() === 1) {
            return $m::has($rel[0])->where('status', 'PUBLISHED')
            ->orWhere('status', 'SCHEDULED')->where('status_date', '<=', now())->orderBy('order', 'asc');
        }

        foreach ($rel as $key => $relation) {
            if ($key === 0) {
                // DO Something Here ...!
            } else {
                // DO Something ELSE Here ...!
            }
        }
    }

    private function breadcrumbs($segments)
    {
        $bc = "";

        if (sizeof($segments)) {
            $sep = '<i class="fa fa-caret-right p-fafa-breadcrumb"></i>';
            $bc = '<ol class="breadcrumb">';
            $bc .= '<li class="breadcrumb-item"><a href="/">Home</a>'.$sep.'</li>';

            $prepend = "/";
            $TXT     = "";

            foreach ($segments as $k=>$segment) {
                $last_piece = end($segments);
                $text = ucfirst(str_replace(array( ".php", "-", "_" ), array( "", " ", " " ), $segment));

                if ($last_piece !== $segment && $k === (count($segments) - 1)) {
                    if ($segment == 'about') {
                        // $bc .= '<li class="breadcrumb-item"><a href="#">'.$text.'</a>'.$sep.'</li>';
                        continue;
                    } elseif ($segment == 'user-support') {
                        // $bc .= '<li class="breadcrumb-item"><a href="#">'.$text.'</a>'.$sep.'</li>';
                        $prepend .= "user-support/";
                    } elseif ($segment == 'user-guides') {
                        // $bc .= '<li class="breadcrumb-item"><a href="#">'.$text.'</a>'.$sep.'</li>';
                        $prepend .= "user-guides/";
                    } elseif ($segment == 'using-the-workbook') {
                        // $bc .= '<li class="breadcrumb-item"><a href="#">'.$text.'</a>'.$sep.'</li>';
                        $prepend .= "using-the-workbook/";
                    } elseif ($segment == 'mathematical-concepts') {
                        // $bc .= '<li class="breadcrumb-item"><a href="#">'.$text.'</a>'.$sep.'</li>';
                        $prepend .= "mathematical-concepts/";
                    } elseif ($segment == 'user-support') {
                        // $bc .= '<li class="breadcrumb-item"><a href="#">'.$text.'</a>'.$sep.'</li>';
                        $prepend .= "user-support/";
                    } elseif ($segment == 'workbook-notes') {
                        // $bc .= '<li class="breadcrumb-item"><a href="#">'.$text.'</a>'.$sep.'</li>';
                        $prepend .= "workbook-notes/";
                    } elseif ($segment == 'shop') {
                        // $bc .= '<li class="breadcrumb-item"><a href="#">'.$text.'</a>'.$sep.'</li>';
                        $prepend .= "shop/";
                    } elseif ($segment == 'login') {
                        // $bc .= '<li class="breadcrumb-item"><a href="#">'.$text.'</a>'.$sep.'</li>';
                        $prepend .= "login/";
                        $TXT .= "Login ";
                    } else {
                        $bc .= '<li class="breadcrumb-item"><a href="'.$prepend.$segment.'">'.$TXT.$text.'</a>'.$sep.'</li>';
                    }
                } else {
                    $bc .= '<li class="breadcrumb-item active">'.$TXT.$text.'</li>';
                }
            }
            $bc .=  '</ol>';
        }

        return $bc;
    }

    public function socialShareLinks($sm_prms)
    {
        $url               = (null !== $sm_prms['url']) ? urlencode($sm_prms['url']) : null;
        //(null !== $sm_prms['url']) ? str_replace(':', '%3A', $sm_prms['url']): null;
        $via               = (null !== $sm_prms['via']) ? urlencode($sm_prms['url']) : null;
        //str_replace(':', '%3A', $sm_prms['via']): null;
        $text              = (null !== $sm_prms['text']) ? $sm_prms['text']: null;
        $text              = str_replace(' ', '%20', $text);
        $desc              = $text;
        $title             = (null !== $sm_prms['title']) ? $sm_prms['title']: null;
        $title             = str_replace(' ', '%20', $title);
        $image             = (null !== $sm_prms['image']) ? $sm_prms['image']: null;
        $provider          = (null !== $sm_prms['provider']) ? $sm_prms['provider']: null;
        $user_id           = (null !== $sm_prms['user_id']) ? $sm_prms['user_id']: null;
        $category          = (null !== $sm_prms['category']) ? $sm_prms['category']: null;
        $language          = (null !== $sm_prms['language']) ? $sm_prms['language']: null;
        $hash_tags         = (null !== $sm_prms['hash_tags']) ? $sm_prms['hash_tags']: null;
        $phone_number      = (null !== $sm_prms['phone_number']) ? $sm_prms['phone_number']: null;
        $email_address     = (null !== $sm_prms['email_address']) ? $sm_prms['email_address']: null;
        $cc_email_address  = (null !== $sm_prms['cc_email_address']) ? $sm_prms['cc_email_address']: null;
        $bcc_email_address = (null !== $sm_prms['bcc_email_address']) ? $sm_prms['bcc_email_address']: null;

        return [
        /* Add.this: */
            'add.this'           => [ 'http://www.addthis.com/bookmark.php?url=' . $url, '' ],
        /* Blogger: */
            'blogger'            => [ 'https://www.blogger.com/blog-this.g?u=' . $url . '&n=' . $title . '&t=' . $desc, 'fa-blogger' ],
        /* Buffer: */
            'buffer'             => [ 'https://buffer.com/add?text=' . $text . '&url=' . $url, 'fa-buffer' ],
        /* Diaspora: */
            'diaspora'           => [ 'https://share.diasporafoundation.org/?title=' . $title . '&url=' . $url, 'fa-diaspora' ],
        /* Digg: */
            'digg'               => [ 'http://digg.com/submit?url=' . $url . '&title=' . $text, 'fa-digg' ],
        /* Douban: */
            'douban'             => [ 'http://www.douban.com/recommend/?url=' . $url . '&title=' . $text, 'fa-douban' ],
        /* Email: */
            'email'              => [ 'mailto:' . $email_address . '?subject=' . $title . '&body=' . $title . '+--+'. $desc . '+.+Go+to+the+link:+' . $url, 'fa-envelope' ],
        /* Evernote: */
            'evernote'           => [ 'http://www.evernote.com/clip.action?url=' . $url . '&title=' . $text, 'fa-evernote' ],
        /* Getpocket: */
            'getpocket'          => [ 'https://getpocket.com/edit?url=' . $url, 'fa-getpocket' ],
        /* Facebook: */
            'facebook'           => [ 'http://www.facebook.com/sharer.php?u=' . $url, 'fa-facebook-f' ],
        /* Flattr: */
            'flattr'             => [ 'https://flattr.com/submit/auto?user_id=' . $user_id . '&url=' . $url . '&title=' . $title . '&description=' . $text . '&language=' . $language . '&tags=' . $hash_tags . '&hidden=HIDDEN&category=' . $category, 'fa-flattr' ],
        /* Flipboard: */
            'flipboard'          => [ 'https://share.flipboard.com/bookmarklet/popout?v=2&title=' . $text . '&url=' . $url, 'fa-flipboard' ],
        /* Gmail: */
            'gmail'              => [ 'https://mail.google.com/mail/?view=cm&to=' . $email_address . '&su=' . $title . '&body=' . $url . '&bcc=' . $bcc_email_address . '&cc=' . $cc_email_address, 'fa-gmail' ],
        /* Google.bookmarks: */
            'google.bookmarks'   => [ 'https://www.google.com/bookmarks/mark?op=edit&bkmk=' . $url . '&title=' . $title . '&annotation=' . $text . '&labels=' . $hash_tags . '', 'fa-google-bookmarks' ],
        /* Instapaper: */
            'instapaper'         => [ 'http://www.instapaper.com/edit?url=' . $url . '&title=' . $title . '&description=' . $desc, 'fa-instapaper' ],
        /* Line.me: */
            'line.me'            => [ 'https://lineit.line.me/share/ui?url=' . $url . '&text=' . $text, 'fa-line-me' ],
        /* LinkedIn: */
            'linkedin'           => [ 'https://www.linkedin.com/shareArticle?mini=true&url=' . $url . '&title=' . $title . '&summary=' . $text . '&source=' . $provider, 'fa-linkedin' ],
        /* Livejournal: */
            'livejournal'        => [ 'http://www.livejournal.com/update.bml?subject=' . $text . '&event=' . $url, 'fa-livejournal' ],
        /* Hacker.news: */
            'hacker.news'        => [ 'https://news.ycombinator.com/submitlink?u=' . $url . '&t=' . $title, 'fa-hacker-news' ],
        /* Ok.ru: */
            'ok.ru'              => [ 'https://connect.ok.ru/dk?st.cmd=WidgetSharePreview&st.shareUrl=' . $url, 'fa-ok-ru' ],
        /* Pinterest: */
            'pinterest'          => [ 'http://pinterest.com/pin/create/button/?url=' . $url, 'fa-pinterest-p' ],
        /* Google.plus: */
            'google.plus'        => [ 'https://plus.google.com/share?url=' . $url . '&text=' . $text . '&hl=' . $language, 'fa-google-plus' ],
        /* Qzone: */
            'qzone'              => [ 'http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?url=' . $url, 'fa-qzone' ],
        /* Reddit: */
            'reddit'             => [ 'https://reddit.com/submit?url=' . $url . '&title=' . $title, 'fa-reddit' ],
        /* Renren: */
            'renren'             => [ 'http://widget.renren.com/dialog/share?resourceUrl=' . $url . '&srcUrl=' . $url . '&title=' . $text . '&description=' . $desc, 'fa-renren' ],
        /* Skype: */
            'skype'              => [ 'https://web.skype.com/share?url=' . $url . '&text=' . $text, 'fa-skype' ],
        /* SMS: */
            'sms'                => [ 'sms:' . $phone_number . '?body=' . $text, 'fa-sms' ],
        /* Stumbleupon: */
            'stumbleupon'        => [ 'http://www.stumbleupon.com/submit?url=' . $url . '&title=' . $text, 'fa-stumbleupon' ],
        /* Surfingbird.ru: */
            'surfingbird.ru'     => [ 'http://surfingbird.ru/share?url=' . $url . '&description=' . $desc . '&screenshot=' . $image . '&title=' . $title, 'fa-surfingbird-ru' ],
        /* Telegram.me: */
            'telegram.me'        => [ 'https://t.me/share/url?url=' . $url . '&text=' . $text . '&to=' . $phone_number, 'fa-telegram-me' ],
        /* Threema: */
            'threema'            => [ 'threema://compose?text=' . $text . '&id=' . $user_id, 'fa-threema' ],
        /* Tumblr: */
            'tumblr'             => [ 'https://www.tumblr.com/widgets/share/tool?canonicalUrl=' . $url . '&title=' . $title . '&caption=' . $desc . '&tags=' . $hash_tags, 'fa-tumblr' ],
        /* Twitter: */
            'twitter'            => [ 'https://twitter.com/intent/tweet?url=' . $url . '&text=' . $text . '&via=' . $via . '&hashtags=' . $hash_tags, 'fa-twitter' ],
        /* VK: */
            'vk'                 => [ 'http://vk.com/share.php?url=' . $url . '&title=' . $title . '&comment=' . $desc, 'fa-vk' ],
        /* Weibo: */
            'weibo'              => [ 'http://service.weibo.com/share/share.php?url=' . $url . '&appkey=&title=' . $title . '&pic=&ralateUid=', 'fa-weibo' ],
        /* Whatsapp: */
            'whatsapp'           => [ 'whatsapp://send?text=' . $url, 'fa-whatsapp' ],
        /* Xing: */
            'xing'               => [ 'https://www.xing.com/app/user?op=share&url=' . $url, 'fa-xing' ],
        /* Yahoo: */
            'yahoo'              => [ 'http://compose.mail.yahoo.com/?to=' . $email_address . '&subject=' . $title . '&body=' . $text, 'fa-yahoo' ],
        ];
    }

    public function _socshr($data_page, $st=null)
    {
        $sharing_links = [];

        $share_to = (null !== $st) ? $st : $this->share_to;
        $sm_args = $this->data['sm_args'];

        if (null !== $data_page->url) {
            $sm_args['url'] = str_replace(':', '%3A', $data_page->url);
        }
        $sm_args['title'] = $data_page->title;
        $sm_args['title'] = urlencode($sm_args['title']);

        $sm_args['text']  = strip_tags($data_page->description);
        $sm_args['text']  = urlencode($sm_args['text']);

        //dump( urlencode( $sm_args['text'] ) );//str_replace( ' ', '%20', $sm_args['title'] );

        $sm_args['desc']  = $sm_args['text'];
        $sm_args['image'] = (null !== $data_page->featured_image) ? $data_page->featured_image : null;

        if (is_array($share_to)) {
            foreach ($share_to as $shr) {
                $sharing_links[$shr] = $this->socialShareLinks($sm_args)[$shr];
            }
            return array_reverse($sharing_links);
        }
        return $sharing_links;
    }

    public function setupSEO($pageInfo, $image=null, $url_title=null)
    {
        $this->data['seo']['seo_title']       = $pageInfo->seo_title;
        $this->data['seo']['seo_keywords']    = $pageInfo->seo_keywords;
        $this->data['seo']['seo_description'] = $pageInfo->seo_description;
        $this->data['seo']['title']           = $pageInfo->title;
        $this->data['seo']['description']     = $pageInfo->description;

        $img = (! is_null($image))
                ? urlencode(ltrim($image, '/')) : urlencode(ltrim($this->data['site_settings']->logo, '/'));
        $url = (is_null($url_title)) ? '/'. urlencode($url_title) : '';

        $this->data['seo']['image'] = $pageInfo->image = url('/') .'/'. $img;
        $this->data['seo']['url']   = $pageInfo->url   = url('/') . $url;
    }
}
