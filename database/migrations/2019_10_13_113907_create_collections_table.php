<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collections', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('title')->nullable();
            $table->text('url_title')->nullable();

            $table->text('video_link')->nullable();

            $table->longText('description')->nullable();

            $table->integer('category_id')->unsigned()->nullable();

            $table->string('seo_title')->nullable();
            $table->text('seo_keywords')->nullable();
            $table->text('seo_description')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->enum('status', ['PUBLISHED', 'UNPUBLISHED', 'DRAFT', 'SCHEDULED'])->default('PUBLISHED')->nullable();
            $table->dateTime('status_date')->nullable();
            $table->integer('order')->default(1);
        });

        Schema::create('collection_galleries', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('title')->nullable();
            $table->longText('description')->nullable();

            $table->text('gallery_image')->nullable();
            $table->text('gallery_video_link')->nullable();

            $table->integer('collection_id')->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->enum('status', ['PUBLISHED', 'UNPUBLISHED', 'DRAFT', 'SCHEDULED'])->default('PUBLISHED')->nullable();
            $table->dateTime('status_date')->nullable();
            $table->integer('order')->default(1);
        });

        Schema::create('collection_related_items', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('related_id')->nullable();
            $table->integer('collection_id')->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->enum('status', ['PUBLISHED', 'UNPUBLISHED', 'DRAFT', 'SCHEDULED'])->default('PUBLISHED')->nullable();
            $table->dateTime('status_date')->nullable();
            $table->integer('order')->default(1);
        });

        Schema::create('collection_share_links', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('title_display')->nullable();
            $table->string('platform')->nullable();

            $table->integer('collection_id')->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->enum('status', ['PUBLISHED', 'UNPUBLISHED', 'DRAFT', 'SCHEDULED'])->default('PUBLISHED')->nullable();
            $table->dateTime('status_date')->nullable();
            $table->integer('order')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collections');
        Schema::dropIfExists('collection_galleries');
        Schema::dropIfExists('collection_related_items');
        Schema::dropIfExists('collection_share_links');
    }
}
