<?php

use Illuminate\Database\Seeder;
use App\Models\User;

// use Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $check = User::where('email', "production@monzamedia.com")->exists();

        // if (! $check) {
        //     $data = [
        //         [
        //             "id"             => 1,
        //             "email"          => "production@monzamedia.com",
        //             "password"       => "@m5tel.06",
        //             "mobile"         => "+27 (0)21 465 8739",
        //             "name"           => "Vault",
        //             "surname"        => "Monzamedia",
        //             "subscribed"     => 0,
        //             "user_groud_id"  => 1,
        //             "admin_type"     => 'super-admin',
        //             "remember_token" =>  Hash::make("remember_admin_password"),
        //             "status"         => "PUBLISHED",
        //             "status_date"    => null,
        //             "order"          => 1
        //         ],
        //         [
        //             "id"             => 2,
        //             "email"          => "dev2@monzamedia.com",
        //             "password"       => "!Qaz2wsx",
        //             "mobile"         => "+27 (0)21 465 8739",
        //             "name"           => "Developer",
        //             "surname"        => "Monzamedia",
        //             "subscribed"     => 0,
        //             "user_groud_id"  => 3,
        //             "admin_type"     => 'developer',
        //             "remember_token" =>  Hash::make("remember_developer_password"),
        //             "status"         => "PUBLISHED",
        //             "status_date"    => null,
        //             "order"          => 2
        //         ],
        //     ];
        // }

        if (! $check) {
            $user = new User();
            
            $user->email          = "production@monzamedia.com";
            $user->password       = "@m5tel.06";
            $user->mobile         = "+27 (0)21 465 8739";

            $user->name           = "Vault";
            $user->surname        = "Monzamedia";

            $user->subscribed     = 0;
            $user->user_group_id  = 1;
            $user->status         = 'PUBLISHED';
            $user->status_date    = null;
            $user->discount_type  = 0;
            $user->discount       = 0;
            $user->admin_type     = 'super-admin';
            $user->remember_token = Hash::make("remember_admin_password");
            $user->order          = 1;

            $user->save();
        }
    }
}
