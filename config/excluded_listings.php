<?php return [
    'excluded_listings' => [
        'sites',
        'pages',
        'home_settings',
        'home_features',
        'home_listings',
        'heritage_settings',
        'distribution_settings',
        'vineyard_settings',
        'wine_settings',
        'tour_settings',
        'news_settings',
        'contact_settings',
    ]
];
