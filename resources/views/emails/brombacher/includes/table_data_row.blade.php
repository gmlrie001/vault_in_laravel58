

@if( $enquiry->title && $enquiry->message )
<!-- start copy -->
<tr>
  <td align="left" bgcolor="#ffffff" style="padding: 24px; font-weight: bold; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">
    @isset( $enquiry->title )
    <p style="margin: 0;">{{ $enquiry->title }}</p>
    @else
    <p style="margin: 0;"></p>
    @endisset
  </td>
  <td align="left" bgcolor="#ffffff" style="padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">
    @isset( $enquiry->message )
    <p style="margin: 0;">{{ $enquiry->message }}</p>
    @else
    <p style="margin: 0;"></p>
    @endisset
  </td>
</tr>
<!-- end copy -->
@endif

