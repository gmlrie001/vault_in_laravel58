<?php namespace Packages\VaultSetup;

use App\Http\Controllers\Controller;

/* Facade Includes */
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
// use Illuminate\Support\Facades\Session;
// use Illuminate\Support\Facades\Schema;
// use Illuminate\Support\Facades\Route;
// use Illuminate\Support\Facades\Input;
// use Illuminate\Support\Facades\URL;

use Hash;
use Closure;

class Setup extends Controller
{
    public function __construct()
    {
        return $this;
    }

    public function db_setup()
    {
        $filename = app()->basepath() . '\\' . '.env';
        exec('cp ' . $filename . '.BU' . ' ' . $filename);

        $exit_code = \Artisan::call('key:generate', []);

        dd($_REQUEST);

        try {
            $_ENV['APP_NAME']    = '"' . $_REQUEST['sitename'] . '"';
            $_ENV['DB_HOST']     = $_REQUEST['hostname'];
            $_ENV['DB_DATABASE'] = $_REQUEST['dbname'];
            $_ENV['DB_USERNAME'] = $_REQUEST['username'];
            $_ENV['DB_PASSWORD'] = (isset($_REQUEST['password'])) ? $_REQUEST['password'] : "";

            $fileHandle = fopen($filename, 'w');
            foreach ($_ENV as $key=>$value) {
                $line_data = $key . "=" . $value . "\r\n";
                fwrite($fileHandle, $line_data);
                unset($line_data);
            }
            fclose($fileHandle);

            return 0;
        } catch (Exception $e) {
            exit(
            $e->getMessage()
          );
        }
    }

    public function admin_setup()
    {
        $user_model = ( new \App\Models\User );

        try {
            $user_model->name          = ($_REQUEST['firstname'] != null) ? $_REQUEST['firstname']: null;
            $user_model->surname       = ($_REQUEST['lastname'] != null) ? $_REQUEST['lastname']: null;
            $user_model->email         = ($_REQUEST['email'] != null) ? $_REQUEST['email']: null;
            $user_model->mobile        = ($_REQUEST['mobile'] != null) ? $_REQUEST['mobile']: null;
            $user_model->password      = ($_REQUEST['password'] != null) ? $_REQUEST['password']: null;
            $user_model->user_group_id = 2;
            $user_model->admin_type    = 'admin';
            $user_model->subscribed    = 0;
            $user_model->discount      = 0;
            $user_model->discount_type = 0;
            $user_model->order         = $user_model->count() + 1;
            $user_model->remember_token = Hash::make('$user_model->remember_token =');

            if ($user_model->save()) {
                // $user_model->update(['password'=> $_REQUEST['password']]);
                // dd($_REQUEST, Hash::make($_REQUEST['password']), get_defined_vars());
                return 0;
            }
            return 1;
        } catch (Exception $e) {
            exit(
            $e->getMessage()
          );
        }
    }
}
