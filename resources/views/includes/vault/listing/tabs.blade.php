@php
  $excludeTables = config('excluded_listings.excluded_listings');
  $currentUrlPath = Request::path();
  $levels = config('admin_levels');
  $levels = $levels['admin_levels'];

  $userLevel = Session::get('user.admin_type');
  $userAccess = $levels[$userLevel]['access'];

  $tab_list = array_reverse($tab_list);

//   if ( ! Session::has( 'prev_relation_id' ) ) {
//       Session::put( 'prev_relation_id', Session::get( 'relation_id' ) );
//   }

//   if ( ! Session::has( 'tab_list' ) ) {
//       Session::put( 'tab_list', $tab_list );

//   } else {
//       Session::forget( 'tab_list' );
//       Session::put( 'tab_list', $tab_list );
//   }

function test_print($item, $key)
{
    echo "$key holds $item\n";
}

array_walk_recursive($tab_list, 'test_print');
@endphp
<div class="col-xs-12 padding-0 tab-container">
    <div class="tabs">
        @foreach($tab_list as $key => $tabInfo)
            @php
                $tabDescripter = key($tabInfo);
                $tabValues = $tabInfo[$tabDescripter];
                $tabTbl = key($tabValues);
                $tabId = $tabValues[$tabTbl];
            @endphp   
            @if($tabId == 0)
                @if(in_array($tabTbl, $userAccess))
                    @php
                        Session::forget( 'tab_list', 'relation_id', 'prev_relation_id' );
                    @endphp
                    @if(!in_array($tabTbl, $excludeTables))
                        @if($currentUrlPath != "vault/".$tabTbl)
                            <a href="/vault/{{$tabTbl}}">{{str_replace("_", " ", $tabTbl)}}</a>
                        @else
                            <a class="active" id="active_tab" href="/vault/{{$tabTbl}}">{{str_replace("_", " ", $tabTbl)}}</a>
                        @endif
                    @endif
                @endif
            @else
                @if(in_array($tabTbl, $userAccess))
                    @if($tabDescripter == 'edit')
                        @if($currentUrlPath != "vault/".$tabTbl."/edit/".$tabId)
                            <a href="/vault/{{$tabTbl}}/edit/{{$tabId}}">Edit {{str_replace("_", " ", str_singular($tabTbl))}}</a>
                        @else
                            <a class="active" id="active_tab" href="/vault/{{$tabTbl}}/edit/{{$tabId}}">Edit {{str_replace("_", " ", str_singular($tabTbl))}}</a>
                        @endif
                    @else
                        @if($currentUrlPath != "vault/".$tabTbl."/relation/".$tabId)
                            <a href="/vault/{{$tabTbl}}/relation/{{$tabId}}">{{str_replace("_", " ", $tabTbl)}}</a>
                        @else
                            <a class="active" id="active_tab" href="/vault/{{$tabTbl}}/relation/{{$tabId}}">{{str_replace("_", " ", $tabTbl)}}</a>
                        @endif
                    @endif
                    {{--  <a href="/vault/{{$tabTbl}}/edit/{{$tabId}}">Edit {{str_replace("_", " ", str_singular($tabTbl))}}</a>  --}}
                @endif
            @endif
        @endforeach
        @if(!in_array($table, $excludeTables) && isset($tabId) && $tabId != 0)
            @if($currentUrlPath != "vault/".$table."/relation/".$tabId)
                <a href="/vault/{{$table}}/relation/{{$tabId}}">{{str_replace("_", " ", $table)}}</a>
            @else
                <a class="active" id="active_tab" href="/vault/{{$table}}/relation/{{$tabId}}">{{str_replace("_", " ", $table)}}</a>
            @endif
        @endif
        
        @if(isset($entry))
            <a href="#" class="active" id="active_tab">{{$method}} {{str_replace("_", " ", str_singular($table))}}</a>
            @if(sizeof($relationships))
                @foreach($relationships as $tbl => $relationship)
                    @if(in_array($tbl, $userAccess))
                        @if(!in_array($tbl, $excludeTables))
                            <a href="/vault/{{$tbl}}/relation/{{$entry->id}}">{{str_replace("_", " ", $relationship)}}</a>
                        @endif
                    @endif
                @endforeach
            @endif
        @endif
    </div>
    <a class="pull-right add-btn tooltipster" data-tooltipster='{"side":"top","animation":"fade"}' data-tooltip-content='#tooltip_content_add' href="/vault/{{$table}}/add/">Add entry</a>
</div>
<script>
    $(window).on("load",function(){
        $(".tabs").mCustomScrollbar({
          axis:"x",
          theme:"dark-3",
          scrollButtons: {
            enable: true, 
            scrollType: 'stepless'
          }, 
          advanced:{
            autoExpandHorizontalScroll:true //optional (remove or set to false for non-dynamic/static elements)
          }
        });
        $('.tabs').mCustomScrollbar('scrollTo', '#active_tab');
      });
</script>

<style id="debugging">
  pre.sf-dump {
    position: relative !important;
    bottom: 0;
    left: 0;
    right: 0;
    max-width: 95%;
    width: 100%;
    height: auto;
    padding: 50px !important;
    margin: 15px 30px;
    overflow: auto !important;
    z-index: -1 !important;
  }
  pre.sf-dump:first-of-type {
    margin-top: 5vmax;
  }
</style>
@php
/*
  dump(
    Session::all(),
    $tab_list,
    get_defined_vars(),
    Request::all()
  );
*/
@endphp
