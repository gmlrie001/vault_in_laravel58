try {
  var ordering = document.querySelector('#ordering');
  ordering.addEventListener('input', ordering_link, false);
} catch (err) {
  console.warn("\r\nWarning: " + err + "\r\n");
}

function ordering_link(event) {
  if (event == undefined || event == null || typeof (event) != 'object') return;

  var evt = event;
  evt.preventDefault();

  var h;
  h = evt.target.getAttribute('data-base-href');

  var v, i, s, t;
  v = evt.target.value;
  i = evt.target.selectedIndex;
  s = evt.target.children[i];
  t = s.innerText;

  var tmp, url;
  tmp = ['/', h, '/', encodeURI(v)];
  url = tmp.join('');

  delete(evt);
  delete(i);
  delete(s);
  delete(t);
  delete(tmp);

  setTimeout(() => {
    window.location.href = url;
  }, 100);

  return;
}

document.addEventListener(
  'click',
  (evt) => {
    evt.stopPropagation();
    if (evt.target.classList.contains('close-modal')) {
      var p = evt.target.parentNode.closest('#filter-modal'),
        r = evt.target.parentNode.nextElementSibling;

      p.classList.remove('show');
      r.parentNode.removeChild(r);

      return;
    } else if (evt.target.classList.contains('mobile-modal-activate')) {
      var m = document.querySelector('#filter-modal'),
        f = document.querySelector('.filters-container'),
        c = f.cloneNode(true);

      m.appendChild(c);
      c.classList.remove('d-none');
      c.classList.add('center-in-modal');
      m.classList.add('show');

      return;
    }
  },
  false
);

const debounce = (func, delay) => {
  let inDebounce;

  return function () {
    const context = this;
    const args = arguments;

    clearTimeout(inDebounce);

    inDebounce = setTimeout(
      () => func.apply(context, args),
      delay
    );
  }
}
/**
 * USAGE:
 debounceBtn.addEventListener('click', debounce(function() {
  console.info('Hey! It is', new Date().toUTCString());
 }, 3000));
;
 */

const throttle = (func, limit) => {
  let lastFunc;
  let lastRan;

  return function () {
    const context = this;
    const args = arguments;

    if (!lastRan) {
      func.apply(context, args)
      lastRan = Date.now()
    } else {
      clearTimeout(lastFunc)
      lastFunc = setTimeout(
        () => {
          if ((Date.now() - lastRan) >= limit) {
            func.apply(context, args)
            lastRan = Date.now()
          }
        },
        limit - (Date.now() - lastRan)
      )
    }
  }
}
/**
 * USAGE:
 throttleBtn.addEventListener('click', throttle(function() {
  return console.log('Hey! It is', new Date().toUTCString());
 }, 1000));
 */

var i = 0;
var o = 11;
var m = 1;
var n = 1;
var es = [].slice.call(document.querySelectorAll('.card.hidden'));
var d = document;
var t = 'link';
window.addEventListener('scroll', throttle(function () {
  var chk = Math.floor(document.querySelector('.cardcols').clientHeight / 10) * 10;
  var offset = window.scrollY + window.innerHeight;
  var height = document.documentElement.offsetHeight;

  if (offset >= height - 150) {
    if (es.slice(i, o).length == 0) {
      window.removeEventListener('scroll', (evt) => {
        evt.preventDefault();
        return;
      }, false);
    }
    es.slice(i, o).forEach((e) => {
      e.classList.remove('hidden');
      var font2load = e.children[0].style.fontFamily
        .split(',')[0]
        .toString()
        .replace(/\s+?/ig, '+')
        .replace(/\"/ig, '');
      var gfonts_url_base = 'http://fonts.googleapis.com/css?family',
        gfonts_font_sel = font2load,
        gfonts_font_var = []; //<?php //echo('["' . implode('", "', $font->variants) . '"]');?>;

      var url_build, g, s;
      url_build = gfonts_url_base + '=' + gfonts_font_sel; // + ':' + gfonts_font_var.join(',');

      try {
        g = d.createElement(t),
          g.rel = "stylesheet";
        g.href = url_build + '&display=swap';
        s = d.getElementsByTagName(t)[0];
        s.parentNode.insertBefore(g, s);
      } catch (err) {
        console.clear();
        console.warn('\r\n' + err + '\r\n');
      }
    });
    i += 3 * m + 1;
    o += i + 4;
    m++;
    n += 0.25;
  }
}, 500), false);

function live_search() {
  // Declare variables
  var input, filter, ul, li, a, i, txtValue;
  input = document.getElementById('font-search');
  filter = input.value.toUpperCase();
  ul = document.getElementById("myUL");
  li = ul.getElementsByTagName('article');
  // Loop through all list items, and hide those who don't match the search query
  for (i = 0; i < li.length; i++) {
    a = li[i]; //.getElementsByTagName("a")[0];
    txtValue = a.getAttribute('data-font-family'); //a.textContent || a.innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      li[i].style.display = "";
      li[i].style.backgroundColor = "#1a1a1a";
      li[i].style.color = "#fefefe";
      li[i].style.borderColor = "#fefefe";
      li[i].querySelector('.cta').style.borderColor = "#fefefe";
    } else {
      li[i].style.display = "none";
    }
  }
}

function selected() {
  // Declare variables
  var input, filter, ul, li, a, i, txtValue;
  input = document.getElementById('font-category');
  filter = input.value.toUpperCase();
  ul = document.getElementById("myUL");
  li = ul.getElementsByTagName('article');
  // Loop through all list items, and hide those who don't match the search query
  for (i = 0; i < li.length; i++) {
    a = li[i]; //.getElementsByTagName("a")[0];
    txtValue = a.getAttribute('data-category'); //a.textContent || a.innerText;
    console.log(txtValue.toUpperCase());
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      li[i].style.display = "";
      li[i].style.backgroundColor = "#1a1a1a";
      li[i].style.color = "#fefefe";
      li[i].style.borderColor = "#fefefe";
      li[i].querySelector('.cta').style.borderColor = "#fefefe";
    } else {
      li[i].style.display = "none";
    }
  }
}

function font_variant(elem, style) {
  var kindaStyle = style.match(/(\d{3}|\w+)/ig);
  var p = elem.parentNode.closest('.card-body');
  p.style.fontStyle = 'unset';
  p.style.fontWeight = 'unset';
  kindaStyle.forEach((e) => {
    if (isNaN(parseInt(e))) {
      if (e == 'regular') {
        p.style.fontWeight = 'normal';
      } else {
        p.style.fontStyle = e;
      }
    } else {
      p.style.fontWeight = parseInt(e);
    }
  });
}
