<?php 
  if(isset($use_parent)){
      $tableNameSingular = studly_case(strtolower(str_singular($entry->parentTable)));

      $parentModelNamespace = str_replace(["{", "}"], ["", ""], "App\Models\{$tableNameSingular}");
      $parentModel = (new $parentModelNamespace);
      $parent = $parentModel::find(Session::get('relation_id'));

      $parentField = $parent->parentOrder;
      $parentValue = $parent->{$parentField};

      $modelNamespace = str_replace(["{", "}"], ["", ""], "App\Models\{$options}");
      $model = (new $modelNamespace);
  
      $options = $model::where($parentField, $parentValue)->pluck('name', 'id')->all();
  }else{
      $modelNamespace = str_replace(["{", "}"], ["", ""], "App\Models\{$options}");
      $model = (new $modelNamespace);
  
      $options = $model::pluck('name', 'id')->all();
  }
?>
{!! Form::label($name, $label) !!}
{!! Form::select($name, $options, null, array('placeholder' => $label, 'class' => 'form-control')) !!}

<script>
    $('#{{$name}}').ddslick();    
</script>
