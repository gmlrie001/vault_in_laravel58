<div class="pull-left function-button">
    <span class="tooltipster" data-tooltipster='{"side":"top","animation":"fade"}' data-tooltip-content="#tooltip_content_export">
        <img src="/assets/images/vault/template/functions/export-icon.svg" />
        <span>Export</span>
        <img src="/assets/images/vault/template/functions/dropdown-icon.svg" />
    </span>
    <div class="function-dropdown">
        <a target="_blank" href="/vault/{{$table}}/export/trash/excel">Export Excel</a>
        <a target="_blank" href="/vault/{{$table}}/export/trash/pdf">Export PDF</a>
    </div>
</div>