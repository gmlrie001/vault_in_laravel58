<?php return [
    'admin_levels' => [
        'super-admin' => [
            'title' => 'Super Admin',
            'description' => 'Super Admin description',
            'access' => [
                'sites',
                'socials',
                'share_links',
                'site_fonts',

                'users',
                'user_groups',
                'group_users',

                'pages',
                    'page_banners',
                        'page_banner_blocks',

                'categories',
                    'category_collections',
                'collections',
                    'collection_options',
                    'collection_galleries',
                    'collection_related_items',
                    'collection_share_links',
    
                'blogs',
                    'blog_galleries',
                    'blog_share_links',

                // 'contact_addresses',
                'contact_enquiries',
                // 'contact_settings',
            ]
        ],
        'developer' => [
            'title' => 'Developer',
            'description' => 'Developer description',
            'access' => [
                'sites',
                'socials',
                'share_links',
                'site_fonts',

                'users',
                'user_groups',

                'pages',
                    'page_banners',
                        'page_banner_blocks',

                'categories',
                    'category_collections',
                'collections',
                    'collection_options',
                    'collection_galleries',
                    'collection_related_items',
                    'collection_share_links',
    
                'blogs',
                    'blog_galleries',
                    'blog_share_links',

                // 'contact_addresses',
                'contact_enquiries',
                // 'contact_settings',
            ]
        ],
        'admin' => [
            'title' => 'Admin',
            'description' => 'Admin description',
            'access' => [
                'sites',
                'socials',
                // 'share_links',
                'site_fonts',

                // 'users',
                // 'user_groups',

                'pages',
                    'page_banners',
                        'page_banner_blocks',

                'categories',
                    'category_collections',
                'collections',
                    'collection_options',
                    'collection_galleries',
                    'collection_related_items',
                    'collection_share_links',
        
                'blogs',
                    'blog_galleries',
                'blog_share_links',
    
                // 'contact_addresses',
                'contact_enquiries',
                // 'contact_settings',
            ]
        ],
        'marketing-admin' => [
            'title' => 'Marketing Admin',
            'description' => 'Marketing Admin description',
            'access' => [
                // 'sites',
                // 'socials',
                // 'share_links',

                // 'users',
                // 'user_groups',

                'pages',
                    'page_banners',
                        'page_banner_blocks',

                'home_settings',
                'home_articles',
                'home_features',
                'home_listings',
        
                // 'heritage_settings',
                // 'heritage_articles',

                'wine_settings',
                'wines',

                'tours',
                'tour_settings',

                'distributions',
                
                'vineyards',
                'vineyard_galleries',

                'news_settings',
                'news',
                'news_galleries',
                'news_sub_articles',
                'news_shares',

                // 'contact_addresses',
                // 'contact_enquiries',
                // 'contact_settings',
            ]
        ],
        'post-editor' => [
            'title' => 'Post Editor',
            'description' => 'Post Editor description',
            'access' => [
                // 'sites',
                // 'socials',
                // 'share_links',

                // 'users',
                // 'user_groups',

                'pages',
                    'page_banners',
                        'page_banner_blocks',

                'home_settings',
                'home_articles',
                'home_features',
                'home_listings',
        
                'heritage_settings',
                'heritage_articles',

                // 'wine_settings',
                // 'wines',

                'tours',
                'tour_settings',

                'distributions',
                
                'vineyards',
                'vineyard_galleries',

                // 'news_settings',
                // 'news',
                // 'news_galleries',
                // 'news_sub_articles',
                // 'news_shares',

                // 'contact_addresses',
                // 'contact_enquiries',
                // 'contact_settings',
            ]
        ],
        'content-editor' => [
            'title' => 'Content Editor',
            'description' => 'Content Editor description',
            'access' => [
                // 'sites',
                // 'socials',
                // 'share_links',

                // 'users',
                // 'user_groups',

                'pages',
                    'page_banners',
                        'page_banner_blocks',

                'home_settings',
                'home_articles',
                'home_features',
                'home_listings',
        
                'heritage_settings',
                'heritage_articles',

                // 'wine_settings',
                // 'wines',

                'tours',
                'tour_settings',

                'distributions',
                
                'vineyards',
                'vineyard_galleries',

                // 'news_settings',
                // 'news',
                // 'news_galleries',
                // 'news_sub_articles',
                // 'news_shares',

                // 'contact_addresses',
                // 'contact_enquiries',
                // 'contact_settings',
            ]
        ],
        'default' => [
            'title' => 'Default',
            'description' => 'Default description',
            'access' => [
                // 'sites',
                'socials',
                'share_links',

                // 'users',
                // 'user_groups',

                'pages',
                    'page_banners',
                        'page_banner_blocks',

                // 'home_settings',
                // 'home_articles',
                // 'home_features',
                // 'home_listings',
        
                // 'heritage_settings',
                // 'heritage_articles',

                // 'wine_settings',
                // 'wines',

                // 'tours',
                // 'tour_settings',

                // 'distributions',
                
                // 'vineyards',
                // 'vineyard_galleries',

                // 'news_settings',
                // 'news',
                // 'news_galleries',
                // 'news_sub_articles',
                // 'news_shares',

                // 'contact_addresses',
                // 'contact_enquiries',
                // 'contact_settings',
            ]
        ],
    ]
];
