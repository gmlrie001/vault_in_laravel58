@extends('layouts.vault')

@section('content')
    <link href="{{ asset('/assets/css/vault/reports.css') }}" rel="stylesheet" />
    <div class="container-fluid">
        <div class="col-xs-12 col-md-4">
            <div class="col-xs-12 panel panel-primary">
                <div class="panel-heading"><span class="fa fa-area-chart"></span>&nbsp;&nbsp;&nbsp;&nbsp;All Site Visits <span class="badge pull-right">{{$all_visits}}</span></div>
            </div>
        </div>
        <div class="col-xs-12 col-md-4">
            <div class="col-xs-12 panel panel-warning">
                <div class="panel-heading"><span class="fa fa-line-chart"></span>&nbsp;&nbsp;&nbsp;&nbsp;Unique Site Visits<span class="badge pull-right">{{$distinct_visits}}</span></div>
            </div>
        </div>
        <div class="col-xs-12 col-md-4">
            <div class="col-xs-12 panel panel-info">
                <div class="panel-heading"><span class="fa fa-compass"></span>&nbsp;&nbsp;&nbsp;&nbsp;Visitor countries<span class="badge pull-right">{{$distinct_countries}}</span></div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="col-xs-12">
            {!! Form::open(['class'=>'col-xs-12 report-filters']) !!}
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <label>Filter Views:</label>
                        {!! Form::select('filter', ['0' => 'ALL views', '1' => 'UNIQUE views'], null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <label>Date Range:</label>
                        {!! Form::text('date_range', null, ['class' => 'form-control', 'id' => 'date_range']) !!}
                        <script>
                            $('#date_range').daterangepicker();
                        </script>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <label>Select Pages:</label>
                        {!! Form::select('pages[]', $pages, null, ['class' => 'form-control', 'id' => 'select_page']) !!}
                        <script>
                            $('#select_page').selectize({
                                maxItems: 3
                            });
                        </script>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <label>Select Locations:</label>
                        {!! Form::select('locations[]', $countries, null, ['class' => 'form-control', 'id' => 'select_loc']) !!}
                        <script>
                            $('#select_loc').selectize({
                                maxItems: 3
                            });
                        </script>
                    </div>
                </div>
                <div class="col-xs-12 padding-0">
                    {!! Form::button('Filter', ['type' => 'submit', 'class' => 'btn btn-primary pull-right']) !!}
                </div>
            {!! FOrm::close() !!}
        </div>
    </div>
    <div id="views-div"></div>
    <?= \Lava::render('AreaChart', 'Views', 'views-div'); ?>
@endsection