<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShareLink extends Model
{
    use SoftDeletes;
    
    public $orderable = true;
    public $orderField = "order";
    public $titleField = "title_display";
    public $statusField = "status";
    public $hasStatus = true;
    public $orderDirection = "asc";
    public $parentOrder = "";
    public $parentTable = "";
    public $orderOptions = ['title_display'];
    public $relationships = [];
    public $mainDropdownField = "platform";
    public $imageDropdownField = "";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title_display',
        'platform',
        'status',
        'status_date',
    ];
    
    public $fields = [
    //  ['field_name', 'label', 'field_type', 'options_model', 'options_relationship', 'width', 'height', 'container_class', 'can_remove'],
        ['title_display', 'Text to Display on Hover', 'text', '', '', '', '', 'col-xs-12 col-md-6', ''],
        ['platform', 'Select Sharing Media Platform', 'select_from_array', [
            'facebook'    => 'facebook',
            'twitter'     => 'twitter',
            'pinterest'   => 'pinterest',
            'linkedin'    => 'linkedin',
            'stumbleupon' => 'stumbleupon',
            'whatsapp'    => 'whatsapp',
            'email'       => 'email'
        ], '', '', '', 'col-xs-12 col-md-6', ''],
        ['open_row', '', ''],
            ['status', 'Status', 'status', '', '', '', '', 'col-xs-12 col-md-6', ''],
        ['close_row', '', '']
    ];

    public function getSharePlatforms()
    {
        $this->sharing = ShareLink::status()->orderBy('order', 'desc')->get(['platform']);

        return $this->sharing;
    }

    /**
     * Scope a query to only include certain status'.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeStatus($query)
    {
        return $query->where('status', 'PUBLISHED')->orWhere('status', 'SCHEDULED')
                     ->where('status_date', '<=', now());
    }
}
