<?php namespace App\Http\Controllers\Page;

/* Base Controller Include */
use App\Http\Controllers\Services\PageController;

//use Mailchimp;
// use \DrewM\MailChimp\MailChimp;

/* Facade Includes */
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Collection;

use Config;

/* HTTP Requests */
use Illuminate\Http\Request;

/* Model Includes */
use App\Models\Site;
use App\Models\ContactAddress as Address;
use App\Models\ContactEnquiry;
use App\Models\ContactSetting;
use App\Models\Page;
use App\Models\PageBanner;
use App\Models\PageBannerBlock;

/* Request Includes */

/* Helper Includes */
use App\Traits\SEO\SEOsTraits;
use App\Helpers\MiscHelpers;

class ContactController extends PageController
{
    use SEOsTraits;

    public $section = 'contact';
    private $settings;
    protected $miscHelper;

    public function __construct()
    {
        parent::__construct();

        $this->miscHelper = new MiscHelpers();

        $this->data['page'] = Page::find(8);
    }

    public function __invoke()
    {
        // $this->_init();

        $sm = $this->data['share_links']->pluck('platform')->toArray();
        $this->data['item_sharing'] = collect(Parent::_socshr($this->data['page'], $sm));

        $banners = PageBanner::status()->where('page_id', $this->data['page']->id);
        $this->data['banners'] = $banners->orderBy('order', 'asc')->get();
        unset($sm, $banners);

        /* SEO Stuffs */
        $this->data['seo'] = $this->setup_SEO($this->data['page']);

        $addresses = Address::status()->orderBy('order', 'asc')->get();
        $contacts = $this->miscHelper->createAddressLink($addresses);
        $this->data['main_contact'] = $contacts->shift();
        $this->data['contacts'] = $contacts;

        $settings = ContactSetting::status()->first();
        $this->data['contact_settings'] = $settings;

        unset($addresses, $contacts, $settings);

        return view('pages.contact.contact', $this->data);
    }

    // private function _init()
    // {
    //     $page_id = $this->data['vault_navitem_settings']['navitem_'.$this->section]['page_id'];
    //     $this->data['page'] = Page::find($page_id);
    // }
}
