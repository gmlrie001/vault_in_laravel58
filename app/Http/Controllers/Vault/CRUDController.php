<?php namespace App\Http\Controllers\Vault;

/* Base Controller Include */
use App\Http\Controllers\Services\VaultController;
/* Facade Includes */
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Arr;
use Illuminate\Http\Request;

/* User Model */
use App\Models\User;

class CRUDController extends VaultController
{
    protected $model = null;
    protected $createRequest = null;
    protected $updateRequest = null;

    public function __construct()
    {
        parent::__construct();

        $tableName = Route::input('tableName');
        
        if (Schema::hasTable($tableName)) {
            $tableNameSingular = studly_case(strtolower(str_singular($tableName)));
            $modelNamespace = str_replace(["{", "}"], ["", ""], "App\Models\{$tableNameSingular}");
            $this->createRequest = str_replace(["{", "}"], ["", ""], "App\Http\Requests\{$tableNameSingular}\CreateRequest");
            $this->updateRequest = str_replace(["{", "}"], ["", ""], "App\Http\Requests\{$tableNameSingular}\UpdateRequest");
            try {
                $this->model = (new $modelNamespace);
            } catch (Exception $e) {
                Session::flash('error', 'Model not found');
                Redirect::to('/vault')->send();
            }
        } else {
            Session::flash('error', 'Table does not exist');
            Redirect::to('/vault')->send();
        }
    }
    private function getTabList($tbl, $id = 0, $reference = [])
    {
        if ($id == 0 || $tbl == "") {
            return $reference;
        } else {
            $currentTableNameSingular = studly_case(strtolower(str_singular($tbl)));
            
            $currentModelNamespace = str_replace(["{", "}"], ["", ""], "App\Models\{$currentTableNameSingular}");
            $currentModel = (new $currentModelNamespace);
    
            if ($currentModel->parentTable != "") {
                $currentEntry = $currentModel::find($id);
                $reference[sizeof($reference)]['edit'] = [$tbl => (int)$id];
                $reference[sizeof($reference)]['relation'] = [$tbl => (int)$currentEntry->{$currentModel->parentOrder}];
                return $this->getTabList($currentModel->parentTable, $currentEntry->{$currentModel->parentOrder}, $reference);
            } else {
                $reference[sizeof($reference)]['edit'] = [$tbl => (int)$id];
                return $reference;
            }
        }
    }
    private function deprecated_getTabList($tbl, $id = 0, $reference = [])
    {
        if ($id == 0 || $tbl == "") {
            return $reference;
        } else {
            $currentTableNameSingular = studly_case(strtolower(str_singular($tbl)));
            
            $currentModelNamespace = str_replace(["{", "}"], ["", ""], "App\Models\{$currentTableNameSingular}");
            $currentModel = (new $currentModelNamespace);
    
            $reference[sizeof($reference)] = [$tbl => (int)$id];
    
            if ($currentModel->parentTable != "") {
                $currentEntry = $currentModel::find($id);
                return $this->getTabList($currentModel->parentTable, $currentEntry->{$currentModel->parentOrder}, $reference);
            } else {
                return $reference;
            }
        }
    }
    public function listing($tableName, $relationId = 0)
    {
        $this->data['table'] = $tableName;
        $this->data['titleField'] = $this->model->titleField;
        // $this->data['orderField'] = (Session::has('order.field')) ? Session::get('order.field') : $this->model->orderField;
        $this->data['orderField'] = $this->model->orderField;
        $this->data['orderDirection'] = $this->model->orderDirection;
        $this->data['statusField'] = $this->model->statusField;
        $this->data['mainDropdownField'] = $this->model->mainDropdownField;
        $this->data['imageDropdownField'] = $this->model->imageDropdownField;
        $this->data['can_order'] = $this->model->orderable;
        $this->data['has_status'] = $this->model->hasStatus;
        $this->data['orderOptions'] = $this->model->orderOptions;
        $this->data['showRelationships'] = false;

        Session::put('relation_id', $relationId);
        
        if ($this->model->parentTable == "") {
            $this->data['tab_list'] = [0 => ['edit' => [$tableName => 0]]];
        } elseif ($relationId == 0) {
            $this->data['tab_list'] = [0 => ['edit' => [$tableName => 0]]];
        } else {
            $this->data['tab_list'] = $this->getTabList($this->model->parentTable, $relationId);
        }

        $orderField = Schema::hasColumn($tableName, Session::get('order.field')) ? Session::get('order.field') : $this->data['orderField'];
        if (! Session::has('order.field') && null != $orderField) {
            Session::put('order.field', $orderField);
        } else {
            Session::put('order.field', $this->data['orderField']);
            $orderField = $this->data['orderField'];
        }
        $orderDirection = Schema::hasColumn($tableName, Session::get('order.field')) ? Session::get('order.direction') : $this->data['orderDirection']; //;
        if (! Session::has('order.direction') && null != $orderDirection) {
            Session::put('order.direction', $orderDirection);
        } else {
            Session::put('order.direction', $this->data['orderDirection']);
            $orderDirection = $this->data['orderDirection'];
        }

        if (Session::has('search_results')) {
            $this->data['results'] = Session::get('search_results');
            $this->data['total_items'] = sizeof(Session::get('search_results'));
            $this->data['link_orders'] = null;
            $this->data['can_order'] = false;
        } else {
            /*
            if ($relationId == 0) {
                $this->data['results'] = $this->model::orderBy($orderField, $orderDirection)->paginate(Session::get('limit'));
                $this->data['total_items'] = $this->data['results']->total();
                if ($this->model->orderable) {
                    $this->data['link_orders'] = $this->model::orderBy($orderField, $orderDirection)->pluck($this->model->orderField, $this->model->orderField);
                } else {
                    $this->data['link_orders'] = null;
                }
            } else {
                $this->data['results'] = $this->model::where($this->model->parentOrder, $relationId)->orderBy($orderField, $orderDirection)->paginate(Session::get('limit'));
                if ($this->model->orderable) {
                    $this->data['link_orders'] = $this->model::where($this->model->parentOrder, $relationId)->orderBy($orderField, $orderDirection)->pluck($this->model->orderField, $this->model->orderField);
                } else {
                    $this->data['link_orders'] = null;
                }
            }
            */
            if ($relationId == 0) {
                if ($this->model->parentOrder != "") {
                    $this->data['results'] = $this->model::where($this->model->parentOrder, null)->orderBy($orderField, $orderDirection)->paginate(Session::get('limit'));
                } else {
                    $this->data['results'] = $this->model::orderBy($orderField, $orderDirection)->paginate(Session::get('limit'));
                }
    
                $this->data['total_items'] = $this->data['results']->total();

                if ($this->model->orderable) {
                    $this->data['link_orders'] = $this->model::orderBy($orderField, $orderDirection)->pluck($this->model->orderField, $this->model->orderField);
                } else {
                    $this->data['link_orders'] = null;
                }
            } else {
                $this->data['results'] = $this->model::where($this->model->parentOrder, $relationId)->orderBy($orderField, $orderDirection)->paginate(Session::get('limit'));
                if ($this->model->orderable) {
                    $this->data['link_orders'] = $this->model::where($this->model->parentOrder, $relationId)->orderBy($orderField, $orderDirection)->pluck($this->model->orderField, $this->model->orderField);
                } else {
                    $this->data['link_orders'] = null;
                }
            }
        }

        return view('vault.crud.listing', $this->data);
    }
    
    public function add($tableName)
    {
        $this->data['method'] = 'add';
        $this->data['showRelationships'] = true;
        $this->data['table'] = $tableName;

        // if ($this->model->parentTable == "") {
        //     $this->data['tab_list'] = [0 => ['edit' => [$tableName => 0]]];
        // } elseif ($relationId == 0) {
        //     $this->data['tab_list'] = [0 => ['edit' => [$tableName => 0]]];
        // } else {
        //     $this->data['tab_list'] = $this->getTabList($this->model->parentTable, $relationId);
        // }
        if ($this->model->parentTable == "") {
            $this->data['tab_list'] = [0 => ['edit' => [$tableName => 0]]];
        } else {
            $this->data['tab_list'] = $this->getTabList($this->model->parentTable, Session::get('relation_id'));
        }

        $this->data['fields'] = $this->model->fields;
        $this->data['relationships'] = [];
        $this->data['entry'] = new $this->model();
        
        return view('vault.crud.form', $this->data);
    }
    
    public function store($tableName, Request $request)
    {
        $request = app($this->createRequest);
        $this->data['method'] = 'add';
        
        $this->data['showRelationships'] = true;
        $this->data['table'] = $tableName;
        $this->data['fields'] = $this->model->fields;
        $this->data['relationships'] = [];

        if ($this->model->parentTable == "") {
            $this->data['tab_list'] = [0 => ['edit' => [$tableName => 0]]];
        } else {
            $this->data['tab_list'] = $this->getTabList($this->model->parentTable, Session::get('relation_id'));
        }

        $entry = $this->model::create($request->all());

        if ($this->model->orderable == true) {
            if ($this->model->parentOrder == "") {
                $entry->{$this->model->orderField} = $this->model::count();
            } else {
                $entry->{$this->model->orderField} = $this->model::where($this->model->parentOrder, $entry->{$this->model->parentOrder})->count();
            }
        }
        $entry->save();
        Session::flash('message', 'Entry has been added successfully');
        
        $this->data['entry'] = new $this->model();
            
        return view('vault.crud.form', $this->data);
    }
    
    public function edit($tableName, $id)
    {
        $this->data['method'] = 'update';
        $this->data['showRelationships'] = true;
        $this->data['table'] = $tableName;
        $this->data['fields'] = $this->model->fields;
        $this->data['relationships'] = $this->model->relationships;
        $this->data['entry'] = $this->model::find($id);

        if ($this->model->parentTable == "") {
            $this->data['tab_list'] = [0 => ['edit' => [$tableName => 0]]];
        } else {
            $this->data['tab_list'] = $this->getTabList($this->model->parentTable, Session::get('relation_id'));
        }

        return view('vault.crud.form', $this->data);
    }
    
    public function update($tableName, $id)
    {
        $request = app($this->updateRequest); // FormRequest
        $this->data['method'] = 'update';
        $this->data['showRelationships'] = true;
        $this->data['table'] = $tableName;
        $this->data['entry'] = $this->model::find($id);

        if ($this->model->parentTable == "") {
            $this->data['tab_list'] = [0 => ['edit' => [$tableName => 0]]];
        } else {
            $this->data['tab_list'] = $this->getTabList($this->model->parentTable, Session::get('relation_id'));
        }

        $this->data['fields'] = $this->model->fields;
        $this->data['relationships'] = $this->model->relationships;
        
        $entry = $this->model::find($id);
        $entry->update($request->all());
        
        Session::flash('message', 'Entry has been updated successfully');
        
        $this->data['entry'] = $this->model::find($id);
        return view('vault.crud.form', $this->data);
    }
    
    public function duplicate($tableName, $id)
    {
        $this->data['method'] = 'add';
        $this->data['showRelationships'] = true;

        $this->data['entry'] = $this->model::find($id);

        if ($this->model->parentTable == "") {
            $this->data['tab_list'] = [0 => ['edit' => [$tableName => 0]]];
        } else {
            $this->data['tab_list'] = $this->getTabList($this->model->parentTable, Session::get('relation_id'));
        }

        $this->data['table'] = $tableName;
        $this->data['fields'] = $this->model->fields;
        $this->data['relationships'] = [];

        return view('vault.crud.form', $this->data);
    }

    public function deep_duplicate($tableName, $id)
    {
        // $this->data['method'] = 'update';
        // $this->data['showRelationships'] = true;

        $relations = [];
        $current_model = $this->model::find($id);

        $this->data['duplicate'] = $this->model::find($id)->replicate();
        $this->data['duplicate']->order = $this->model::count() + 1;
        $this->data['duplicate']->save();

        $new_id = $this->data['duplicate']->id;
        $id = $new_id;
        $relationships = array_values($current_model->relationships);

        foreach ($relationships as $key=>$relations) {
            $model_relations = [];
            foreach ($current_model->$relations as $relation) {
                $duplicate_relation = $relation->replicate();
                $thisID = $duplicate_relation->parentOrder;
                unset($duplicate_relation->$thisID);
                $duplicate_relation->$thisID = $new_id;
                $model_relations[] = $duplicate_relation;
            }
            $this->data['duplicate']->$relations()->saveMany($model_relations);
            unset($model_relations);
        }

        unset($relationships);

        $this->data['entry'] = $this->data['duplicate'];
        $this->data['entry']->refresh();

        $this->data['method'] = 'update';//'deep_copy'
        $this->data['showRelationships'] = true;

        // if ($this->model->parentTable == "") {
        //     $this->data['tab_list'] = [0 => [$tableName => 0]];
        // } else {
        //     $this->data['tab_list'] = $this->getTabList($this->model->parentTable, $this->data['entry']->{$this->model->parentOrder});
        // }
        // if ($this->model->parentTable == "") {
        //     $this->data['tab_list'] = [0 => ['edit' => [$tableName => 0]]];
        // } elseif ($relationId == 0) {
        //     $this->data['tab_list'] = [0 => ['edit' => [$tableName => 0]]];
        // } else {
        //     $this->data['tab_list'] = $this->getTabList($this->model->parentTable, $relationId);
        // }
        if ($this->model->parentTable == "") {
            $this->data['tab_list'] = [0 => ['edit' => [$tableName => 0]]];
        } else {
            $this->data['tab_list'] = $this->getTabList($this->model->parentTable, Session::get('relation_id'));
        }

        $this->data['table'] = $tableName;
        $this->data['fields'] = $this->model->fields;
        $this->data['relationships'] = [];

        return Redirect::to(preg_replace('/(\/relation\/\d+[(\?|\&|\#)\w+(=.*)*]*?)$/iuS', '', URL::previous()) . '/edit/' . $new_id)
               ->with('message', 'Model duplication successful. Please update the entry accordingly');
        // return view('vault.crud.form', $this->data);
        //return Redirect::back()->with('message', 'Model duplication successful. Please update the entry accordingly!');
    }

    public function delete($tableName, $id)
    {
        $entry = $this->model::find($id);
        if ($this->model->orderable == true) {
            if ($this->model->parentOrder == "") {
                $this->model::where($this->model->orderField, '>', $entry->{$this->model->orderField})->decrement($this->model->orderField);
            } else {
                $this->model::where($this->model->parentOrder, $entry->{$this->model->parentOrder})->where($this->model->orderField, '>', $entry->{$this->model->orderField})->decrement($this->model->orderField);
            }
        }
        $entry->delete();
        return Redirect::back()->with('message', 'Entry deleted successfully');
    }
    
    public function deletePermanent($tableName, $id)
    {
        $entry = $this->model::withTrashed()->find($id);
        $entry->forceDelete();
        return Redirect::back()->with('message', 'Entry deleted permanently');
    }

    public function deleteSelected($tableName, Request $request)
    {
        // $selectedItems = explode(',', Input::get('ids'));
        $selectedItems = explode(',', $request->input('ids'));
        $results = $this->model::find($selectedItems);//whereIn('id', $selectedItems)->get();
        
        // dd(get_defined_vars());
        if ($this->model->orderable == true) {
            if ($this->model->parentOrder == "") {
                foreach ($results as $key => $result) {
                    $this->model::where('order', '>', $result->order)->decrement('order');
                    $this->model::where('id', $result->id)->delete();
                }
            } else {
                foreach ($results as $key => $result) {
                    $this->model::where($this->model->parentOrder, $result->{$this->model->parentOrder})->where('order', '>', $result->order)->decrement('order');
                    $this->model::where($this->model->parentOrder, $result->{$this->model->parentOrder})->where('id', $result->id)->delete();
                }
            }
        } else {
            foreach ($results as $key => $result) {
                // $this->model::where('id', $result->id)->delete();
                $this->model::find($result->id)->delete();
            }
        }

        return Redirect::back()->with('message', 'Successfully removed');
    }
    public function deletePermanentSelected($tableName, Request $request)
    {
        // $selectedItems = explode(',', Input::get('ids'));
        $selectedItems = explode(',', $request->input('ids'));
        $results = $this->model::withTrashed()->find($selectedItems);//whereIn('id', $selectedItems)->get();

        // dd(get_defined_vars());

        if (count($selectedItems) === 0) {
            return Redirect::back()->with('error', 'Nothing Selected!');
        }

        foreach ($results as $key => $result) {
            // $this->model::where('id', $result->id)->forceDelete();
            $this->model::withTrashed()->find($result->id)->forceDelete();
        }

        return Redirect::back()->with('message', 'Successfully deleted');
    }
    public function restore($tableName, $id)
    {
        $entry = $this->model::withTrashed()->where('id', $id)->first();
        $entry->restore();

        if ($this->model->orderable == true) {
            if ($this->model->parentOrder == "") {
                $entry->{$this->model->orderField} = $this->model::count();
            } else {
                $entry->{$this->model->orderField} = $this->model::where($this->model->parentOrder, $entry->{$this->model->parentOrder})->count();
            }
        }

        $entry->save();
        $entry->restore();

        return Redirect::back()->with('message', 'Entry restored');
    }
}
