@if( $page->banners && count( $page->banners ) > 0 )
<div class="container-fluid p-0 overflow-hidden">
    <div class="row">
        <div class="col-12 page-slider">
        @foreach( $page->displayBanners as $key=>$banner )
            <div>
            @foreach( $banner->displayBlocks as $block )
                <div class="row m-0">
                    <a href="{{ route( 'home' ) }}" target="_blank" title="Banner 1" class="p-0 col-12 col-lg-12">
                        <img class="img-fluid d-none d-lg-block" src="/{{ ltrim( $block->banner_image, '/' ) }}" alt="{{ $block->title }}" />
                        <img class="img-fluid d-block d-lg-none" src="/{{ ltrim( $block->mobile_image, '/' ) }}" alt="{{ $block->title }}" />
                    </a>
                </div>
            @endforeach
            </div>
        @endforeach
        </div>
    </div>
    <a title="Goto Homepage" href="{{ route( 'home' ) }}" class="banner-logo">
        <img class="img-fluid" src="{{ ltrim( $site_settings->logo, '/' ) }}" />
    </a>
</div>
@endif

{{-- dd( get_defined_vars(), $page->banners ) --}}

