@php
    $levels = config('admin_levels');
    $options = [];
    $optionData = [];
    foreach($levels['admin_levels'] as $key => $level){
        $options[$key] = $level['title'];
        $optionData[$key]['data-description'] = $level['description'];
    }
@endphp

{!! Form::label($name, $label) !!}
{!! Form::select($name, $options, null, array('placeholder' => $label, 'class' => 'form-control'), $optionData) !!}

<script>
    $('#{{$name}}').ddslick();    
</script>