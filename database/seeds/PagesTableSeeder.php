<?php

use Illuminate\Database\Seeder;
use App\Models\Page;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        date_default_timezone_set("Africa/Johannesburg");

        if (DB::table('pages')->exists()) {
            DB::table('pages')->delete();
        }

        $data = [
            [ 'id'=>1, 'title'=>"Site Settings", 'description'=>null, 'seo_title'=>"Site Settings", 'seo_keywords'=>"Site Settings", 'seo_description'=>"<p>Site Settings</p>", 'created_at'=>date('Y-m-d H:i:s'), 'updated_at'=>date('Y-m-d H:i:s'), 'deleted_at'=>null, 'status'=>"PUBLISHED", 'status_date'=>null, 'order'=>1, ],
            [ 'id'=>2, 'title'=>"Home", 'description'=>null, 'seo_title'=>"Home Page", 'seo_keywords'=>"Home Page", 'seo_description'=>"<p>Home Page</p>", 'created_at'=>date('Y-m-d H:i:s'), 'updated_at'=>date('Y-m-d H:i:s'), 'deleted_at'=>null, 'status'=>"PUBLISHED", 'status_date'=>null, 'order'=>2, ],
            [ 'id'=>8, 'title'=>"Contact", 'description'=>null, 'seo_title'=>"Contact", 'seo_keywords'=>"Contact", 'seo_description'=>"<p>Contact</p>", 'created_at'=>date('Y-m-d H:i:s'), 'updated_at'=>date('Y-m-d H:i:s'), 'deleted_at'=>null, 'status'=>"PUBLISHED", 'status_date'=>null, 'order'=>8, ],
        ];
        //
        
        if (DB::table('pages')->get()->count() == 0) {
            DB::table('pages')->insert($data);
        }
    }
}
