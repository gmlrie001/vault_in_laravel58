<nav class="@if( Request::is( '/' ) ) d-none collapse hidden @endif d-flex flex-lg-row flex-column">

  <div class="title m-b-md mr-auto">
    <h1 class="font-weight-bold">Vault</h1>
  </div>

  <div class="links d-flex justify-content-around ml-auto">
    <a rel="noopener noreferrer" target="_blank" class="font-weight-medium" title="Go to the Documentation" href="http://dev.pretty-docs.co.za">Documentation</a>
    <a class="font-weight-medium" title="Setup your Site&apos;s Vault" href="{{ route( 'db_setup' ) }}">Vault Setup</a>
    <a  class="font-weight-medium"title="Access your Site&apos;s Vault" href="{{ route( 'the_vault' ) }}">Vault</a>
  </div>

</nav>
