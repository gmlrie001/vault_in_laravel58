<?php

  return [

    'Collections' => [

      'title' => 'Collections',
      'page_id' => '3',
      'icon' => 'fa fa-gift',
  
      'main_link_database_tables' => [
        'categories' => [
          'title' => 'Categories',
          'icon' => 'fa fa-sitemap',
          'specific_id' => '',
          'sub_database_tables' => [
            'categories',
            'category_collections',
          ]
        ],

        'collections' => [
          'title' => 'Collection Products',
          'icon' => 'fa fa-gift',
          'specific_id' => '',
          'sub_database_tables' => [
            'collection_options',
            'collection_related_items',
            'collection_share_links',
            'collection_galleries',
          ]
        ],

      ],
      'tab_database_tables' => [
        'categories',
        'category_collections',
        'collection_options',
        'collection_related_items',
        'collection_share_links',
        'collection_galleries',
      ]

    ],

  ]['Collections'];
