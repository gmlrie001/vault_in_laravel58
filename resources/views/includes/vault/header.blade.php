<div class="col-xs-12 header-content padding-0">
    <div>
        <a class="vault-logo" href="/vault">
            <img src="/assets/images/vault/logo/vault-logo-white.svg" />
        </a>
    </div>
    <div class="pull-right hidden-md hidden-lg">
        <a class="hamburger" href="#">
           <span></span>
           <span></span>
           <span></span>
        </a> 
    </div>
    <a href="/vault/logout">
        <span>Logout</span>
        <img src="/assets/images/vault/template/logout.svg" />
    </a>
    <a href="{{url('/')}}" target="_blank">
        <span>{{rtrim(str_replace("https://","",str_replace("http://","",url('/'))), '/')}}</span>
        <img src="/assets/images/vault/template/go-to-site.svg" />
    </a>
    @isset( $notifications )
    <a class="hidden collapse d-none" title="Notifications" href="#" data-notifications hidden>
        <span style="
            width:  40px;
            height: 40px;
            line-height: 40px;
            font-size: calc( 29px * 0.618 );
        ">
            Notifications
        </span>
        <i class="fa fa-bell-o" style="
                font-size: calc( 40px * 1.618 / 2 );
                padding-left: 1rem;
                vertical-align: middle;
                color: #000;
        ">
        @if( count( $notifications ) > 0 )
          <sup class="header-alert-circle notifications-count">{{ count( $notifications ) }}</sup>
        @endif
        </i>
    </a>
    @endisset
</div>

<script>
$( document ).ready( () => {
    var do_nothing = true;
    try {
        var nav_notifier = document.querySelector( 'a[data-notifications]' ),
        notifier_window = document.querySelector( '.dashboard_main > .col-xs-12.col-md-3.pull-right' );
        nav_notifier.addEventListener( 'click', ( event ) => {
            try {
                if ( ! do_nothing ) notifier_window.classList.toggle( 'show' );
            } catch( e ) {/* console.warn( "\r\n" + e.getMessage() + "\r\n" ); */}
        }, false );
    } catch( e ) {/* console.warn( "\r\n" + e.getMessage() + "\r\n" ); */}
} );
</script>
