<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;

class CollectionRelatedItem extends Model
{
    use SoftDeletes;
    
    public $orderable = true;
    public $orderField = "order";
    public $titleField = "title";
    public $statusField = "status";
    public $hasStatus = true;
    public $orderDirection = "asc";
    public $parentOrder = "collection_id";
    public $parentTable = "collections";
    public $orderOptions = ['title', 'updated_at'];
    public $relationships = [];
    public $mainDropdownField = "title";
    public $imageDropdownField = "";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'related_id',
        'collection_id',
        'status',
        'status_date',
    ];
    
    public $fields = [
    //  ['field_name', 'label', 'field_type', 'options_model', 'options_relationship', 'width', 'height', 'container_class', 'can_remove'],
        ['title', 'Title', 'hidden', '', '', '', '', 'col-xs-12 col-md-6 d-none collapse hidden', ''],
        ['related_id', 'Related Item', 'select_relorcat_fromModel', 'Collection', '', '', '', 'col-xs-12 col-md-6', ''],

        ['open_row', '',''],
          ['collection_id', '', 'parent', '', '', '', '', 'col-xs-12 col-md-6 d-none collapse hidden', ''],
          ['status', 'Status', 'status', '', '', '', '', 'col-xs-12 col-md-6', ''],
        ['close_row', '',''],
    ];

    public function collection()
    {
        return $this->belongsTo(Collection::class, 'collection_id')->withDefault();
    }

    /**
     * Scope a query to only include certain status'.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeStatus($query)
    {
        return $query->where('status', 'PUBLISHED')->orWhere('status', 'SCHEDULED')
                     ->where('status_date', '<=', now());
    }
}
