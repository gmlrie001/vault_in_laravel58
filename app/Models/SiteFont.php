<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;

class SiteFont extends Model
{
    use SoftDeletes;
    
    public $orderable = true;
    public $orderField = "order";
    public $titleField = "title_font";
    public $statusField = "status";
    public $hasStatus = true;
    public $orderDirection = "asc";
    public $parentOrder = "";
    public $parentTable = "";
    public $orderOptions = ['title_font', 'body_font', 'cta_font'];
    public $relationships = [];
    public $mainDropdownField = "body_font";
    public $imageDropdownField = "";

    /**
     * This sets the PRIMARY KEY for the current model.
     *
     * @var string
     */
    // protected $primaryKey = 'family';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title_font',
        'body_font',
        'cta_font',
        'status',
        'status_date',
    ];

    public $fields = [
    //  ['field_name', 'label', 'field_type', 'options_model', 'options_relationship', 'width', 'height', 'container_class', 'can_remove'],
        // ['open_parent', 'Font Selector', ''],
        //     ['open_row', '',''],
        //         ['font_selector', '', 'font_cards', '', '', '', '', 'col-xs-12 col-md-12', ''],
        //     ['close_row', '',''],
        // ['close_parent', 'Font Selector', ''],

        // ['open_row', '',''],
        //     ['status', 'Status', 'status', '', '', '', '', 'col-xs-12 col-md-6', ''],
        // ['close_row', '',''],
    ];

    /**
     * Scope a query to only include certain status'.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeStatus($query)
    {
        return $query->where('status', 'PUBLISHED')->orWhere('status', 'SCHEDULED')
                     ->where('status_date', '<=', now());
    }

    /**
     * Scope a query to only include certain status'.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOrdering($query)
    {
        return $query->status()->orderBy($this->orderField, $this->orderDirection);
    }
}
