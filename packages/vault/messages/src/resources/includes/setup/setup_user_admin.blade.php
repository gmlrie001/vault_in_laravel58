@extends( 'layouts.page' )

@push( 'pageStyles' )
<style>
main * {
  transition: all 0.618s !important;
  transition-timing-function: ease;
}
.page-container {
  display: grid;
  grid-template-areas:
    "header top_navigation"
    "sidebar_navigation main_contain"
    "footer footer";
  grid-template-columns: 15vw 85vw;
  grid-template-rows: 10vh 90vh 0;
}
header,
header.header {
  grid-area: header;
  grid-column: 1;
  background-color: red;
}
nav {
  grid-area: top_navigation;
  grid-column: 2;
  background-color: grey;
  padding: calc( ( 10vh - 30px ) * 0.5 );
}
nav ul,
nav ul li a {
  line-height: 30px;
  padding: 0 !important;
}
main aside {
  grid-area: sidebar_navigation;
  grid-column: 1;
  background-color: black;
}
main .vault-content {
  grid-area: main_contain;
  grid-column: 2;
  background-color: white;
}
main .container.vault-content {
  top: 0;
  right: 0;
  left: 0;
  bottom: 0;
  max-width: 65%;
  max-height: 100vh;
  height: auto;
  margin: auto;
}
footer {
  grid-area: footer;
}
.d-contents {
  display: contents;
}
[hidden], .hidden {
  display: none;
}
.no-list-style {
  list-style: none;
}
@media only screen and (max-width: 576px) {
  .page-container {
    grid-template-areas:
    "header"
    "top_navigation"
    "sidebar_navigation"
    "main_contain";
  grid-template-columns: auto;
  grid-template-rows: auto auto auto auto;
  }
}
.side-nav .navbar {
  display: none !important;
}
</style>
@endpush

@section( 'content' )
<section class="container vault-content m-auto">
  <div class="row">
    <div class="col-md-6 offset-md-3">

      <span class="anchor" id="formRegister"></span>

      <hr class="mb-5">

      <!-- form card register -->
      <div class="card card-outline-secondary">

        <div class="card-header">
          <h3 class="mb-0">Vault Admin Setup</h3>
        </div>

        <div class="card-body">

          <form class="form" role="form" method="POST" action="{{ route('add_user_admin') }}" autocomplete="off">

            @csrf

            <div class="form-group">
              <label for="firstname">First Name</label>
              <input type="text" name="firstname" class="form-control" id="firstname" placeholder="First Name" required="">
            </div>

            <div class="form-group">
              <label for="lastname">Last Name</label>
              <input type="text" name="lastname" class="form-control" id="lastname" placeholder="Database Name" required="">
            </div>

            <div class="form-group">
              <label for="email">Email</label>
              <input type="email" name="email" class="form-control" id="email" placeholder="Email Address" required="" autocapitalization="off"  autocomplete="off">
            </div>

            <div class="form-group">
              <label for="mobile">Mobile</label>
              <input type="mobile" name="mobile" class="form-control" id="mobile" placeholder="Mobile" required="" autocapitalization="off"  autocomplete="off">
            </div>

            <div class="form-group">
              <label for="password">Password</label>
              <input type="password" class="form-control" id="password" name="password" placeholder="Password" autocapitalization="off"  autocomplete="off">
            </div>

            <div class="form-group">
              <label for="verifyPassword">Verify Password</label>
              <input type="password" class="form-control" id="verifyPassword" name="verifyPassword" placeholder="Verify Password" autocapitalization="off" autocomplete="off">
            </div>

            <div class="collapse d-none hidden" hidden>
              {!! Honeypot::generate('my_time', 'my_name') !!}
            </div>

            @isset( $errors )
            <!-- @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
              </ul>
            </div>
            @endif -->
            @endisset

            <div class="form-group clearfix">
              <button type="submit" class="btn btn-success btn-lg left-right">Create Admin. Account</button>
              <a href="/vault/login" class="btn btn-skip btn-lg float-right">SKIP</a>
            </div>
          </form>
        </div>
      </div><!-- /form card register -->

      <hr class="mb-5">

    </div>
  </div>
</section>
@endsection
