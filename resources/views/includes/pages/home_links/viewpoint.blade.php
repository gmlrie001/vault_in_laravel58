<section class="container-fluid d-none d-md-block section-margin home-cta">
    <div class="container">
        <div class="row">
            @foreach( $home_links as $k=>$item )
            <a href="{{ $item->link }}" class="col-12 col-md-6" title="{{ $item->title }}">
                <div class="text-center">
                    <div class="cta-block keyAnimation">
                        <div class="blue-border">
                        @isset( $item->link_image )
			    @if( ( $k % 2 ) === 0 )
                            <div class="image ltr">
			    @else
                            <div class="image">
			    @endif
                                <img class="img-fluid" src="{{ $item->link_image }}" alt="{{ $item->title }}">
                            </div>
                        @endisset
                            <div class="title">
                                <h1>{{ $item->title }}</h1>
                            </div>
                            <div class="description">
                                <p>{!! str_limit( strip_tags( $item->description ), 150 ) !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            @endforeach
        </div>
    </div>
</section>

<section class="container-fluid d-block d-md-none section-margin home-cta">
    <div class="container">
        <div class="row callToAction">
            @foreach( $home_links as $m=>$item )
            <a href="{{ $item->link }}" class="col-12 col-md-6" title="{{ $item->title }}">
                <div class="text-center">
                    <div class="cta-block keyAnimation">
                        <div class="blue-border">
                        @isset( $item->link_image )
			    @if( ( $m % 2 ) === 0 )
                            <div class="image ltr">
			    @else
                            <div class="image">
			    @endif
                                <img class="img-fluid" src="{{ $item->link_image }}" alt="{{ $item->title }}">
                            </div>
                        @endisset
                            <div class="title">
                                <h1>{{ $item->title }}</h1>
                            </div>
                            <div class="description">
                                <p>{!! str_limit( strip_tags( $item->description ), 150 ) !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            @endforeach
        </div>
    </div>
</section>

