<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoogleFontsTable extends Migration
{
    /**
     * Run the migrations.
     *   https://www.googleapis.com/webfonts/v1/webfonts?key=API_KEY
     *
     * kind: The kind of object, a webfont object
     * family: The name of the family
     * subsets: A list of scripts supported by the family
     * variants: The different styles available for the family
     * version: The font family version.
     * lastModified: The date (format "yyyy-MM-dd") the font family was modified for the last time.
     * files: The font family files (with all supported scripts) for each one of the available variants.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('google_fonts', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('kind')->nullable();
            $table->string('family')->nullable();
            $table->string('category')->nullable();
            $table->mediumText('variants')->nullable(); //array
            $table->mediumText('subsets')->nullable(); //array
            $table->string('version')->nullable();
            $table->date('lastModified')->nullable();
            $table->json('files')->nullable(); //json

            $table->timestamps();
            $table->softDeletes();

            $table->enum('status', ['PUBLISHED', 'UNPUBLISHED', 'DRAFT', 'SCHEDULED'])->default('PUBLISHED')->nullable();
            $table->dateTime('status_date')->nullable();
            $table->integer('order')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('google_fonts');
    }
}
