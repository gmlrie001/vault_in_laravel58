<?php

namespace App\Http\Middleware;

use Closure;
/* Facade Includes */
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

/* Model Includes */

/* Request Includes */

class VaultCheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }
}
