<?php namespace App\Http\Controllers\Vault;

/* Base Controller Include */
use App\Http\Controllers\Services\VaultController;

/* Facade Includes */
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

class CRUDRelationController extends VaultController {
    protected $model = null;
    protected $createRequest = null;
    protected $updateRequest = null;

    function __construct() {
        parent::__construct();
        $tableName = Route::input('tableName');

        if(Schema::hasTable($tableName)) {
            $tableNameSingular = studly_case(strtolower(str_singular($tableName)));

            $modelNamespace = str_replace(["{", "}"], ["", ""], "App\Models\{$tableNameSingular}");
            $this->createRequest = str_replace(["{", "}"], ["", ""], "App\Http\Requests\{$tableNameSingular}\CreateRequest");
            $this->updateRequest = str_replace(["{", "}"], ["", ""], "App\Http\Requests\{$tableNameSingular}\UpdateRequest");
            
            try  {
                $this->model = (new $modelNamespace);
            } catch (Exception $e){
                Session::flash('error', 'Model not found');
                Redirect::to('/vault')->send();
            }
        }else{
            Session::flash('error', 'Table does not exist');
            Redirect::to('/vault')->send();
        }
    }

    public function listing($tableName, $relationId){
        $this->data['table'] = $tableName;

        $this->data['titleField'] = $this->model->titleField;
        $this->data['orderField'] = $this->model->orderField;
        $this->data['statusField'] = $this->model->statusField;
        
        $this->data['mainDropdownField'] = $this->model->mainDropdownField;
        $this->data['imageDropdownField'] = $this->model->imageDropdownField;

        $this->data['can_order'] = $this->model->orderable;
        $this->data['has_status'] = $this->model->hasStatus;

        $this->data['orderOptions'] = $this->model->orderOptions;
        $this->data['showRelationships'] = false;
        $this->data['is_relation'] = true;
        $this->data['relation_table'] = $this->model->parentTable;

        $tableNameSingular = studly_case(strtolower(str_singular($this->model->parentTable)));

        $modelNamespace = str_replace(["{", "}"], ["", ""], "App\Models\{$tableNameSingular}");
        $this->modelRelation = (new $modelNamespace);

        $this->data['parent_relation_id'] = $this->modelRelation::find($relationId)->{$this->modelRelation->parentOrder};
        
        $this->data['relation_id'] = $relationId;
        
        Session::put('relation_id', $relationId);

        $orderField = Schema::hasColumn($tableName, Session::has('order.field')) ? Session::get('order.field') : $this->model->orderField;
        $orderDirection = Schema::hasColumn($tableName, Session::has('order.field')) ? Session::get('order.direction') : $this->model->orderDirection;
        
        $this->data['results'] = $this->model::where($this->model->parentOrder, $relationId)->orderBy($orderField, $orderDirection)->paginate(Session::get('limit'));
        if($this->model->orderable){
            $this->data['link_orders'] = $this->model::where($this->model->parentOrder, $relationId)->orderBy($orderField, $orderDirection)->pluck($this->model->orderField, $this->model->orderField);
        }else{
            $this->data['link_orders'] = null;
        }
        	
        return view('vault.crud.listing', $this->data);
    }
}