<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CategoryCollection extends Model
{
    use SoftDeletes;
    
    public $orderable = true;
    public $orderField = "order";
    public $titleField = "title";
    public $statusField = "status";
    public $hasStatus = true;
    public $orderDirection = "asc";
    public $parentOrder = "category_id";
    public $parentTable = "categories";
    public $orderOptions = ['title', 'order', 'updated_at'];
    public $relationships = [ 'category_collections' => 'category_collections' ];
    // public $relationships = [];
    public $mainDropdownField = "category_id";
    public $imageDropdownField = "";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'category_id',
        'collection_id',
        'status',
        'status_date',
    ];

    /**/
    public function category()
    {
        return $this->belongsTo(Category::class, 'id', 'category_id');
    }
    /**/
    public function category_collections()
    {
        return $this->hasMany(Collection::class, 'id', 'collection_id')->orderBy('order', 'asc');
    }
    public function displayCategoryCollection()
    {
        return $this->hasMany(Collection::class, 'id', 'collection_id')->where('status', 'PUBLISHED')->orWhere('status', 'SCHEDULED')->where('status_date', '>=', now())->orderBy('order', 'asc');
    }

    public $fields = [
        //  ['field_name', 'label', 'field_type', 'options_model', 'options_relationship', 'width', 'height', 'container_class', 'can_remove'],
            ['title', '', 'hidden', '', '', '', '', 'col-xs-12 col-md-6 d-none collapse hidden', ''],
            ['collection_id', 'Collection Item', 'select_cate_prod', 'Collection', '', '', '', 'col-xs-12 col-md-6', ''],
/*
            ['open_parent', 'SEO Information', ''],
                ['open_row', '', ''],
                    ['seo_title', 'SEO Title', 'text', '', '', '', '', 'col-xs-12 col-md-6', ''],
                ['close_row', '', ''],
                ['open_row', '', ''],
                    ['seo_description', 'SEO Description', 'textarea', '', '', '', '', 'col-xs-12 col-md-6', ''],
                    ['seo_keywords', 'SEO Keywords', 'tags', '', '', '', '', 'col-xs-12 col-md-6', ''],
                ['close_row', '', ''],
            ['close_parent', 'SEO Information', ''],
            ['open_parent', 'Image Options', ''],
                ['category_image', 'Banner Image', 'image', '', '', '1400', '370', 'col-xs-12 col-md-6', 'can_remove'],
                ['menu_image', 'Menu Image', 'image', '', '', '160', '200', 'col-xs-12 col-md-6', 'can_remove'],
                ['featured_image', 'Featured Image', 'image', '', '', '480', '360', 'col-xs-12 col-md-6', 'can_remove'],
            ['close_parent', 'Image Options', ''],
*/
            ['open_row', 'Status'],
                ['category_id', '', 'parent', '', '', '', '', 'col-xs-12 col-md-6 d-none collapse hidden', ''],
                ['status', 'Status', 'status', '', '', '', '', 'col-xs-12 col-md-6', ''],
            ['close_row', 'Status'],
        ];
}
