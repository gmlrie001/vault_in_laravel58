<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_articles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();

            $table->longText('description')->nullable();

            $table->text('article_image')->nullable();
            $table->text('article_image_mobile')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->enum('status', ['PUBLISHED', 'UNPUBLISHED', 'DRAFT', 'SCHEDULED'])->default('PUBLISHED')->nullable();
            $table->dateTime('status_date')->nullable();
            $table->integer('order')->default(1);
        });

        Schema::create('home_article_banners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();

            $table->longText('description')->nullable();

            $table->text('article_image')->nullable();
            $table->text('article_image_mobile')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->enum('status', ['PUBLISHED', 'UNPUBLISHED', 'DRAFT', 'SCHEDULED'])->default('PUBLISHED')->nullable();
            $table->dateTime('status_date')->nullable();
            $table->integer('order')->default(1);
        });

        Schema::create('home_features', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->text('link')->nullable();
            $table->string('link_text')->nullable();

            $table->longText('description')->nullable();
            $table->text('feature_image')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->enum('status', ['PUBLISHED', 'UNPUBLISHED', 'DRAFT', 'SCHEDULED'])->default('PUBLISHED')->nullable();
            $table->dateTime('status_date')->nullable();
            $table->integer('order')->default(1);
        });

        Schema::create('home_feature_banners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();

            $table->longText('description')->nullable();

            $table->text('article_image')->nullable();
            $table->text('article_image_mobile')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->enum('status', ['PUBLISHED', 'UNPUBLISHED', 'DRAFT', 'SCHEDULED'])->default('PUBLISHED')->nullable();
            $table->dateTime('status_date')->nullable();
            $table->integer('order')->default(1);
        });

        Schema::create('home_listings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();

            $table->text('listing_link')->nullable();
            $table->text('listing_image')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->enum('status', ['PUBLISHED', 'UNPUBLISHED', 'DRAFT', 'SCHEDULED'])->default('PUBLISHED')->nullable();
            $table->dateTime('status_date')->nullable();
            $table->integer('order')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_articles');
        Schema::dropIfExists('home_article_banners');
        Schema::dropIfExists('home_features');
        Schema::dropIfExists('home_feature_banners');
        Schema::dropIfExists('home_listings');
    }
}
