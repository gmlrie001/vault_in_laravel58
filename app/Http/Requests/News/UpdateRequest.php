<?php namespace App\Http\Requests\News;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'url_title' => 'required|unique:news',
            'published_date' => 'nullable|date',
            'my_name'   => 'honeypot',
            'my_time'   => 'required|honeytime:0'
        ];
    }
    
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'url_title.required' => 'URL Title (or Slug) must not be blank.',
            'title.unique' => 'URL Title (or Slug) must be unqiue.',
            'my_time.required' => '',
            'my_name.honeypot'  => '',
            'my_time.honeytime'  => ''
        ];
    }
}
