<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Session;

use Closure;

class CheckSetOrder
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Session::has('order')){
        	Session::put('order.field', 'order');
        	Session::put('order.direction', 'asc');
        }else {
        	Session::put('order.field', Session::get('order.field'));
        	Session::put('order.direction', Session::get('order.direction'));
        }
        
        return $next($request);
    }
}
