if (document.addEventListener || typeof document.addEventListener !== "undefined") {
  document.addEventListener("DOMContentLoaded", (evt) => {
    run(close_message);
  });
} else if (document.attachEvent || typeof document.attachEvent !== "undefined") {
  document.attachEvent("onreadystatechange", (evt) => {
    if (document.readyState === "complete") {
      run(close_message)
    }
  });
}

function run(callback) {
  var elem_sel, close_elem;
  elem_sel = '.alert .fa.fa-times';
  close_elem = elements(elem_sel);

  /* MAKE pointer style & ADD click listener to close bar */
  close_elem.map((b) => {
    b.style.cursor = "pointer";
    if (b.addEventListener) {
      b.addEventListener('click', (evt) => {
        callback(evt)
      }, true);
    } else if (b.attachEvent) {
      b.attachEvent('onclick', (evt) => {
        callback(evt)
      });
    }
  });
  return;
}

function close_message(elm) {
  var evt, elp;
  evt = elm;
  elp = tree_retrace_parent(get_parent(evt.srcElement), 'alert');

  if (null !== elp) close_element(elp);
  if (evt.srcElement.removeEventListener) {
    evt.srcElement.removeEventListener('click', (evt) => {}, false);
  } else {
    if (evt.srcElement.detachEvent) {
      evt.srcElement.detachEvent('onclick', (evt) => {});
    }
  }
  return;
}

function elements(sel = null) {
  return (null !== sel) ? [].slice.call(document.querySelectorAll(sel)) : null;
}

function element(sel = null) {
  return (null !== sel) ? document.querySelector(sel) : null;
}

function get_parent(elem) {
  console.log(elem);
  console.log(elem.parentNode);
  return elem.parentNode;
}

function close_element(elm = null) { //, { a:null, v:null } ) {
  var style_string, attr = 'style';
  style_string = 'display: none !important;';
  style_string += 'animation-duration: 0.75s !important;';

  if (null === elm || typeof elm === undefined) return;

  return (!elm.classList.contains('elm.classList')) ? elm.classList.add('elementFadeOut') : null;
}

function tree_retrace_parent(e, s) {
  /**
   * e - is the actual node element;
   * s - is the selector string (class or any other valid selector);
   * and return parent_element containing selector s || null.
   */

  i = 1, imax = 15;
  p = get_parent(e); //.parentNode;

  if (p.classList.contains(s)) return p;

  do {
    p = get_parent(p); //.parentNode;
    i++
  } while ((!p.classList.contains(s)) && (i < imax));

  return p || null;
}
