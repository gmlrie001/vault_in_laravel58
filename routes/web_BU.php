<?php

use App\Helpers\GoogleFonts\GoogleFontsHelper;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Arr;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* ========================================= For FontsClient =========================================*/
// $URL = 'https://www.googleapis.com/webfonts/v1/webfonts?key=AIzaSyAvMVD7pM9FyquH84i-CiBddZ0IAiFfAMk';
// $c = curl_init();
// curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
// curl_setopt($c, CURLOPT_URL, $URL);
// $contents = curl_exec($c);
// curl_close($c);
/* ========================================= For FontsClient =========================================*/

/* ========================================= DB Fonts Update =========================================*/
/* Testing updateOrCreate method */
// $columns  = array_keys($font_collection[0]);
// $indexOf  = array_search('family', $columns) || null;
// if ($indexOf != null || gettype($indexOf) == 'integer') {
//     unset($columns[$indexOf]);
// }
// $existing = $font_db::where('family', $font_collection[0]['family'])->first();
// ->where(function ($query) use ($font_collection) {
//     $query->orWhere('kind', $font_collection[0]['kind']);
//     $query->orWhere('category', $font_collection[0]['category']);
//     $query->orWhere('version', $font_collection[0]['version']);
//     $query->orWhere('lastModified', $font_collection[0]['lastModified']);
//     $query->orWhere('variants', $font_collection[0]['variants']);
//     $query->orWhere('subsets', $font_collection[0]['subsets']);
//     $query->orWhere('files', $font_collection[0]['files']);
// })->first();
// if ($existing->where('kind', $font_collection[0]['kind'])->first()) {}
// $db = $font_db::updateOrCreate();
// $existing = $font_db::updateOrCreate(array_keys($font[0]), array_values($font[0]));
/* ========================================= DB Fonts Update =========================================*/

/* ============================================================================================ */
/* Listing */
// 'vault/parse_google_fonts_file',
Route::get(
    'vault/parse_google_fonts_file',
    'Vault\GoogleFontController'
)->name('fonts_listing');

/* ============================================================================================ */
/* Update */
// 'vault/parse_google_fonts_file/update',
Route::get(
    'vault/parse_google_fonts_file/update',
    'Vault\GoogleFontController@update'
)->name('fonts_update');

/* ============================================================================================ */
/* Search */
// 'vault/parse_google_fonts_file/search',
Route::any(
    'vault/parse_google_fonts_file/search',
    'Vault\GoogleFontController@search'
)->name('fonts_search');

/* ============================================================================================ */
/* Filter */
// 'vault/parse_google_fonts_file/filter',
Route::any(
    'vault/parse_google_fonts_file/filter',
    'Vault\GoogleFontController@filter'
)->name('fonts_filter');

/* ============================================================================================ */
/* Filter Ordering: LAST-MODIFIED and VERSION */
// 'vault/parse_google_fonts_file/orderBy/{col}/{dir}',
Route::any(
    'vault/parse_google_fonts_file/orderBy/{col}/{dir}',
    'Vault\GoogleFontController@ordering'
)->name('fonts_ordering');

/* ============================================================================================ */

/* ============================================================================================ */
/* Filter: CATEGORY */
// Route::get('/parse_google_fonts_file/filter/category/{categoryName}', function ($categoryName) {
//     $filtered = App\Models\GoogleFont::where('category', $categoryName)->paginate(35);
//     $this->data['font_db'] = $filtered;
//     // dd($categoryName, $filtered);
//     return view('includes.pages.cards', $this->data);
// })->name('filter_by_category_google_fonts');
/* ============================================================================================ */
/* Filter: VARIANTS */
// Route::get('/parse_google_fonts_file/filter/variants/{variant}', function ($variant) {
//     $variant_query = explode(',', $variant);
//     // dd($variant, explode(',', $variant));
//     $filtered = App\Models\GoogleFont::whereJsonContains('variants', $variant_query)->paginate(35);
//     $this->data['font_db'] = $filtered;
//     // dd($variant, $filtered->first());
//     return view('includes.pages.cards', $this->data);
// })->name('filter_by_variant_google_fonts');
/* ============================================================================================ */
/* Filter: SUBSETS */
// Route::get('/parse_google_fonts_file/filter/subsets/{subset}', function ($subset) {
//     $subset_query = explode(',', $subset);
//     // dd($subset, explode(',', $subset));
//     $filtered = App\Models\GoogleFont::whereJsonContains('subsets', $subset_query)->paginate(35);
//     $this->data['font_db'] = $filtered;
//     // dd($subset, $filtered->first());
//     return view('includes.pages.cards', $this->data);
// })->name('filter_by_subset_google_fonts');
/* ============================================================================================ */
