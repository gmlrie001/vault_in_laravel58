<?php namespace App\Http\Controllers\Services;

/* Base Controller Include */
use App\Http\Controllers\Controller;

/* Facade Includes */
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

/* Model Includes */

/* Request Includes */

class VaultController extends Controller {
    public function __construct(){
        $this->middleware(function ($request, $next) {
            if(Session::get('user.id') != null && Session::get('user.admin_type') == 0){
                Session::put('user.id', Session::get('user.id'));
                Session::put('user.name', Session::get('user.name'));
                Session::put('user.surname', Session::get('user.surname'));
                Session::put('user.admin_type', Session::get('user.admin_type'));
            }else{
                return Redirect::to('/vault/login')->send();
            }
            return $next($request);
        });   
    }
}