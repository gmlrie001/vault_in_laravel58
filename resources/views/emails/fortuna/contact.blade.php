@extends('layouts.emails.brombacher')

@section('content')

  <!-- start preheader -->
  <div class="preheader" style="display:none;max-width:0;max-height:0;overflow:hidden;font-size:1px;line-height:1px;color:#fff;opacity:0;">
    You have received an enquiry from {{url('/')}}
  </div>
  <!-- end preheader -->

  <!-- start body -->
  <table border="0" cellpadding="0" cellspacing="0" width="100%">

    <!-- start logo -->
    <tr>
      <td align="center" bgcolor="#e9ecef">
        <!--[if (gte mso 9)|(IE)]>
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">

        <tr>
        <td align="center" valign="top" width="600">
        <![endif]-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
          <tr>
            <td align="center" valign="top" style="padding:36px 24px;">
              <a href="{{url('/')}}" target="_blank" style="display:inline-block;">
                <img src="/{{$logo}}" alt="Logo" border="0" width="96" style="display:block;width:96px;max-width:96px;min-width:96px;">
              </a>
            </td>
          </tr>
        </table>
        <!--[if (gte mso 9)|(IE)]>
        </td>
        </tr>

        </table>
        <![endif]-->
      </td>
    </tr>
    <!-- end logo -->

    <!-- start hero -->
    <tr>
      <td align="center" bgcolor="#e9ecef">
        <!--[if (gte mso 9)|(IE)]>
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">

        <tr>
        <td align="center" valign="top" width="600">
        <![endif]-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
          <tr>
            <td align="left" bgcolor="#ffffff" style="padding:36px 24px 12px;font-family:'Source Sans Pro', Helvetica, Arial, sans-serif;border-top:3px solid #d4dadf;">
              <h1 style="margin:0;font-size:32px;font-weight:700;letter-spacing:-1px;line-height:48px;">Contact Enquiry</h1>
            </td>
          </tr>
        </table>
        <!--[if (gte mso 9)|(IE)]>
        </td>
        </tr>

        </table>
        <![endif]-->
      </td>
    </tr>
    <!-- end hero -->

    <!-- start copy block -->
    <tr>
      <td align="center" bgcolor="#e9ecef">
        <!--[if (gte mso 9)|(IE)]>
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">

        <tr>
        <td align="center" valign="top" width="600">
        <![endif]-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">

          <!-- start copy -->
          <tr>
            <td align="left" bgcolor="#ffffff" style="padding:12px 24px 24px;font-weight:bold;font-family:'Source Sans Pro', Helvetica, Arial, sans-serif;font-size:16px;line-height:24px;">
              <p style="margin:0;">Name:</p>
            </td>
            <td align="left" bgcolor="#ffffff" style="padding:12px 24px 24px;font-family:'Source Sans Pro', Helvetica, Arial, sans-serif;font-size:16px;line-height:24px;">
              <p style="margin:0;">{{ $enquiry->name }}</p>
            </td>
          </tr>
          <!-- end copy -->

          <!-- start copy -->
          <tr>
            <td align="left" bgcolor="#ffffff" style="padding:12px 24px 24px;font-weight:bold;font-family:'Source Sans Pro', Helvetica, Arial, sans-serif;font-size:16px;line-height:24px;">
              <p style="margin:0;">Surname:</p>
            </td>
            <td align="left" bgcolor="#ffffff" style="padding:12px 24px 24px;font-family:'Source Sans Pro', Helvetica, Arial, sans-serif;font-size:16px;line-height:24px;">
              <p style="margin:0;">{{ $enquiry->surname }}</p>
            </td>
          </tr>
          <!-- end copy -->

          <!-- start copy -->
          <tr>
            <td align="left" bgcolor="#ffffff" style="padding:12px 24px 24px;font-weight:bold;font-family:'Source Sans Pro', Helvetica, Arial, sans-serif;font-size:16px;line-height:24px;">
              <p style="margin:0;">Email:</p>
            </td>
            <td align="left" bgcolor="#ffffff" style="padding:12px 24px 24px;font-family:'Source Sans Pro', Helvetica, Arial, sans-serif;font-size:16px;line-height:24px;">
              <p style="margin:0;">{{ $enquiry->email }}</p>
            </td>
          </tr>
          <!-- end copy -->
          
          <!-- start copy -->
          <tr>
            <td align="left" bgcolor="#ffffff" style="padding:12px 24px 24px;font-weight:bold;font-family:'Source Sans Pro', Helvetica, Arial, sans-serif;font-size:16px;line-height:24px;">
              <p style="margin:0;">Message:</p>
            </td>
            <td align="left" bgcolor="#ffffff" style="padding:12px 24px 24px;font-family:'Source Sans Pro', Helvetica, Arial, sans-serif;font-size:16px;line-height:24px;">
              <p style="margin:0;">{{ $enquiry->message }}</p>
            </td>
          </tr>
          <!-- end copy -->

        </table>
        <!--[if (gte mso 9)|(IE)]>
        </td>
        </tr>

        </table>
        <![endif]-->
      </td>
    </tr>
    <!-- end copy block -->

@endsection