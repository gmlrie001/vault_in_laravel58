<?php namespace App\Http\Controllers\Page;

/* Base Controller Include */
use App\Http\Controllers\Services\PageController;

/* Facade Includes */
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

/* Illuminate Support */
use Illuminate\Support\Collection;

use App\Repositories\Repository;

/* HTTP Requests */
use Illuminate\Http\Request;

/* Model Includes */
use App\Models\Page;
use App\Models\PageBanner;
use App\Models\PageBannerBlock;

use App\Models\Category;
use App\Models\Collection as Product;


/* Custom Helper Functions */
use App\Helpers\Strings\StringHelper;
use App\Helpers\DateTime\DateHelper;

/* Custom Traits For Vault Models */
use App\Traits\SEO\SEOsTraits;
use App\Helpers\MiscHelpers;

class CollectionController extends PageController
{
    use SEOsTraits;
    private $settings;

    public function __construct()
    {
        parent::__construct();

        $this->data['page'] = Page::find(6);
    }

    public function __invoke($url_title)
    {
        $sm = $this->data['share_links']->pluck('platform')->toArray();
        $this->data['item_sharing'] = collect(Parent::_socshr($this->data['page'], $sm));

        $banners = PageBanner::status()->where('page_id', $this->data['page']->id);
        $this->data['banners']  = $banners->get();
                
        $product = ( new Repository(new Product) )->showByUrlTitle($url_title);
        $this->data['product'] = $product->load('options', 'related_items', 'galleries');
        // dd($this->data['product']->category);
        $banners = PageBanner::status()->where('page_id', $this->data['page']->id);

        // SETUP this section\'s SEO information.
        $this->data['seo'] = $this->setup_SEO($this->data['page']);

        $this->data['page']->title = 'Collections';
        $sm  = $this->data['product']->sharing_links->pluck('platform')->toArray();
        $sm = ['facebook', 'twitter', 'pinterest', 'linkedin'];
        // dd($sm);
        $tmp = collect(parent::_socshr($this->data['product'], $sm));
        $this->data['sharing_links'] = $tmp->reverse();

        $related = [];
        foreach ($this->data['product']->related_items as $rel) {
            $related[] = Product::findOrFail($rel->related_id);
        }
        $this->data['related_items'] = $related;

        unset($sm, $tmp, $banners, $product, $related, $this->data['share_links']);

        return view('pages.collections.article', $this->data);
    }
}
