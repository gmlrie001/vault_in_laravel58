<?php

/*
|--------------------------------------------------------------------------
| Vault Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Dashboard */
Route::get('vault', 'DashboardController@index')->name('the_vault');

/* Image Manipulation Routes */
Route::any('vault/image/crop/{dir}', 'ImageManipulationController@imageCrop');
Route::any('vault/temp/image/upload', 'ImageManipulationController@tempImageUpload');
Route::post('vault/image/upload/{dir}', 'ImageManipulationController@imageUpload');
Route::get('vault/image/remove/{table}/{field}/{id}', 'ImageManipulationController@removeImage');

/* Change show limit Routes */
Route::get('vault/limit/set/{count}', 'LimitController');

/* Change display order field Routes */
Route::get('vault/order/set/{direction}/{field}', 'OrderFieldController');

/* Status Routes */
Route::get('vault/{tableName}/status/change/{id}', 'StatusController@change');
Route::get('vault/{tableName}/change-selected-status', 'StatusController@changeSelectedStatus');

/* Export Routes */
Route::get('vault/{tableName}/export/pdf', 'ExportController@createPdf');
Route::get('vault/{tableName}/export/excel', 'ExportController@createExcel');
Route::get('vault/create-selected-pdf/{tableName}', 'ExportController@createSelectedPdf');
Route::get('vault/create-selected-excel/{tableName}', 'ExportController@createSelectedExcel');
Route::get('vault/{tableName}/export/trash/pdf', 'ExportController@createTrashPdf');
Route::get('vault/{tableName}/export/trash/excel', 'ExportController@createTrashExcel');
Route::get('vault/create-selected-pdf/trash/{tableName}', 'ExportController@createTrashSelectedPdf');
Route::get('vault/create-selected-excel/trash/{tableName}', 'ExportController@createTrashSelectedExcel');

/* Search Routes */
Route::post('vault/{tablename}/search', 'SearchController@search');
Route::post('vault/{tablename}/search/trash', 'SearchController@searchTrash');

/* Move Entry Up Routes */
Route::get('vault/{tableName}/up/{id}', 'OrderController@moveUp');

/* Move Entry Down Routes */
Route::get('vault/{tableName}/down/{id}', 'OrderController@moveDown');

/* Bulk Reorder Routes */
Route::post('vault/{tableName}/bulk/order', 'OrderController@bulkOrder');

/* Drag reorder Routes */
Route::post('vault/{tableName}/reorder/list', 'OrderController@reorderList');

/* Restore Order Routes */
Route::get('vault/{tableName}/order/restore', 'OrderController@restoreOrder');


/* CRUD */
/* Listing routes */
Route::get('vault/{tableName}', 'CRUDController@listing');
Route::get('vault/{tableName}/trash', 'TrashController@listing');
Route::get('vault/{tableName}/relation/{relationId}', 'CRUDController@listing');

/* Add routes */
Route::get('vault/{tableName}/add', 'CRUDController@add');
Route::post('vault/{tableName}/add', 'CRUDController@store');

/* Edit routes */
Route::get('vault/{tableName}/edit/{id}', 'CRUDController@edit');
Route::post('vault/{tableName}/edit/{id}', 'CRUDController@update');

/* Duplicate Routes */
Route::get('vault/{tableName}/duplicate/{id}', 'CRUDController@duplicate');
Route::post('vault/{tableName}/duplicate/{id}', 'CRUDController@store');

/* Duplicate Routes */
Route::get('vault/{tableName}/deep_duplicate/{id}', 'CRUDController@deep_duplicate');
Route::post('vault/{tableName}/deep_duplicate/{id}', 'CRUDController@update');

/* Delete Routes */
Route::get('vault/{tableName}/delete/{id}', 'CRUDController@delete');
Route::get('vault/{tableName}/delete-selected', 'CRUDController@deleteSelected');
Route::get('vault/{tableName}/delete/{id}/permanent', 'CRUDController@deletePermanent');
Route::get('vault/{tableName}/delete-selected/permanent', 'CRUDController@deletePermanentSelected');

/* Restore Routes */
Route::get('vault/{tableName}/restore/{id}', 'CRUDController@restore');

/* Report Routes */
Route::get('vault/report/page_views', 'ReportController@pageViews');
Route::post('vault/report/page_views', 'ReportController@filterPageViews');

//+=============================================================================+

/* Get All Tables In DB */
// Route::get(
//     'vault/db/tables',
//     function () {
//         $optionsArray = timezone_identifiers_list();
//         $tables = DB::select('SHOW TABLES');
//         dd($optionsArray[24], $tables);
//         $db = 'Tables_in_' . Config::get('database.connections.mysql.database');
//         foreach ($tables as $table) {
//             echo $table->{$db} . "<br>";
//         }
//     }
// )->name('db_tables');

// ARTISAN Command::SEEDER
// Route::get(
//     'vault/command/seeder',
//     function () { /* php artisan seeder */
//         \Artisan::call(
//             'db:seed',
//             [ '--force' => true ]
//         );
//         $output = trim(Artisan::output());
//         return;
//     }
// )->name('command_seeder');

/* Get All Tables In DB */
// Route::get(
//     'vault/db/migrations',
//     function () {
//         \Artisan::command(
//             'migrate',
//             [ '--force' => true ]
//         );
//     }
// )->name('db_migrations');

//+=============================================================================+
