<?php



namespace App\Http\Requests\HomeSetting;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{

    /**

     * Determine if the user is authorized to make this request.

     *

     * @return bool

     */

    public function authorize()
    {
        return true;
    }



    /**

     * Get the validation rules that apply to the request.

     *

     * @return array

     */

    public function rules()
    {
        return [

            // 'site_name' => 'required|string',

            // 'site_domain' => 'filled|string',

            // 'logo' => 'required',

            // 'mobile_logo' => 'required',

            // 'footer_logo' => 'required',

            // 'copyright' => 'required|string',

            // 'tax_rate' => 'filled|numeric',

            'my_name'   => 'honeypot',

            'my_time'   => 'required|honeytime:0'

        ];
    }

    

    /**

     * Get the error messages for the defined validation rules.

     *

     * @return array

     */

    public function messages()
    {
        return [

            // 'site_name.required' => 'Site Name must not be blank.',

            // 'site_name.string' => 'Site Name must be text.',

            // 'site_domain.filled' => 'Site Domain must not be blank.',

            // 'site_domain.string' => 'Site Domain must be text.',

            // 'logo.required' => 'Logo is required',

            // 'mobile_logo.required' => 'Mobile Logo is required',

            // 'footer_logo.required' => 'Footer Logo is required',

            // 'copyright.required' => 'Copyright must not be blank.',

            // 'copyright.string' => 'Copyright must be text.',

            // 'tax_rate.filled' => 'Tax Rate must not be blank.',

            // 'tax_rate.numeric' => 'Tax Rate must be a number.',

            'my_time.required' => '',

            'my_name.honeypot'  => '',

            'my_time.honeytime'  => ''

        ];
    }
}
