<div class="col-xs-12 padding-0 function-row">
    @include('includes.vault.listing.trash.search')
    @include('includes.vault.listing.trash.export_import')
    @include('includes.vault.listing.order_by')
    @include('includes.vault.listing.page_limit')
</div>
<script>
    $(".function-button > span").click(function(){
        $('.function-dropdown').slideUp(500);
        if($(this).parent().find('.function-dropdown').css('display') == 'block'){
            $(this).parent().find('.function-dropdown').slideUp(500);
        }else{
            $(this).parent().find('.function-dropdown').slideDown(500);
        }
    });

    $(".function-search input").focus(function(){
        $(".function-search").addClass('active');
    }).blur(function(){
        $(".function-search").removeClass('active');
    });
</script>