@extends('layouts.vault')

@section('content')
    <div class="container-fluid content">
        @include('includes.vault.listing.trash.tabs')

        <div class="col-xs-12 listing-container padding-0">
            @include('includes.vault.listing.trash.functions')
            <div class="col-xs-12 padding-0 listing-headings">
                <span class="col-md-1 text-center"></span>
                <span class="col-md-2 text-center">Order</span>
                <span class="col-md-4">Title</span>
                <span class="col-md-2 text-center">Status</span>
                <span class="col-md-2 text-center">Last Modified</span>
                <span class="col-md-1 text-right">Edit</span>
            </div>
            <div class="col-xs-12 padding-0 list">
                @foreach($results as $result)
                    @include('includes.vault.listing.trash.listing_row', ['result' => $result])
                @endforeach
            </div>
        </div>
    </div>
    @include('includes.vault.listing.pagination')
    @include('includes.vault.listing.tooltips')
    @include('includes.vault.listing.trash.mass_operation')
    <script>
        $("input[type='checkbox']").change(function(){
            if($(this).is(':checked')){
                $(this).parent().addClass('active');
            } else {
                $(this).parent().removeClass('active');
            }
        });
        
        $(".resizeme").fitText(1, { minFontSize: '9px', maxFontSize: '15px' });

        $(".listing-row .accord").click(function(e){
            e.preventDefault();
            $(this).toggleClass('fa-plus fa-minus');
            $(this).closest(".listing-row").toggleClass('active');
            $(this).closest(".listing-row").find('.listing-dropdown').slideToggle();
        });
        
        $('.mass-operation').on('change', function(event){
            if($('.mass-operation:checked').length > 0){
                $('.selected-options').fadeIn();
            }else {
                $('.selected-options').fadeOut();
            }
        });

        $('.get-selected-items').on('click', function(event) {
            event.preventDefault();
            var selected = [];
            $('.mass-operation:checked').each(function(i) {
                selected[i] = $(this).attr('value');
            });
            var route = $(this).attr('href');
            route = route+'?ids='+selected;
            window.location.href = route;
        });
    </script>
@endsection