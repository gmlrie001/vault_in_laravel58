<?php

namespace App\Http\Requests\Login;

use Illuminate\Foundation\Http\FormRequest;

class VaultLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'    => 'required|email|exists:users,email',
            'password' => 'required',
            'my_name'  => 'honeypot',
            'my_time'  => 'required|honeytime:0'
        ];
    }
    
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'password.required' => 'Password is required.',
            'email.required'    => 'Email address is required.',
            'email.email'       => 'Please entera valid email address.',
            'email.exists'      => 'Email address does not exist.',
            'my_time.required' => '',
            'my_name.honeypot'  => '',
            'my_time.honeytime'  => ''
        ];
    }
}
