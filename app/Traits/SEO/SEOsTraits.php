<?php namespace App\Traits\SEO;

/* Facades */
use Illuminate\Support\Facades\URL;

/* HTTP Requests */
use Illuminate\Http\Request;

trait SEOsTraits
{
    public function setup_SEO($pageInfo, $title=null, $description=null, $image=null)
    {
        $server_protocol = (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 'ON')) ? 'https://' : 'http://' ;
        $server_domain   = rtrim($_SERVER['HTTP_HOST'], '/');
        $server_path     = ltrim($_SERVER['REQUEST_URI'], '/');

        $canonical_url   = $server_protocol . $server_domain . '/'. $server_path;

        $req = new Request();
        $seo = collect([]);

        $a = (! isset($pageInfo) || is_null($pageInfo)) ?: [
            'seo_title' => (isset($pageInfo->seo_title) || ! is_null($pageInfo->seo_title))
                                ? $pageInfo->seo_title : $pageInfo->title,
            'seo_keywords' => $pageInfo->seo_keywords,
            'seo_description' =>  (isset($pageInfo->seo_description) || ! is_null($pageInfo->seo_description))
                                ? strip_tags($pageInfo->seo_description) : null,
            'title' => (isset($title)) ? $title : $pageInfo->seo_title,
            'description' => (isset($pageInfo->seo_description)) ? strip_tags($pageInfo->seo_description) : null,
            'image' => (isset($image)) ? URL::to($image) : (isset($pageInfo->image)) ? URL::to($pageInfo->image) : null,
            'url' => (isset($canonical_url) && $canonical_url) ? $canonical_url : null,
        ];

        foreach ($a as $k=>$v) {
            $seo->$k = $v;
        }
        
        return $seo;
    }
}
