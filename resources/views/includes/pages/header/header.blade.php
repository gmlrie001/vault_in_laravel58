<?php
global $requestUri;
global $documentRoot;

$requestUri   = $_SERVER['REQUEST_URI'];
$documentRoot = $_SERVER['DOCUMENT_ROOT'];

$route_array = [
  'home'      => [ 'home', 'home/index.php' ],
  'hire'      => [ 'hire', 'hire/index.php' ],
  'catalogue' => [ 'catalogue', 'catalogue/index.php' ],
  'film'      => [ 'film', 'film/index.php' ],
  'news'      => [ 'news', 'news/index.php' ],
  'clients'   => [ 'clients', 'clients/index.php' ],
  'gallery'   => [ 'gallery', 'gallery/index.php' ],
  'contact'   => [ 'contact', 'contact/index.php' ]
];
?>

<header>
  <nav class="navbar navbar-expand-md navbar-light pt-0 pt-md-3 d-none d-lg-flex">
    <a href="/home" class="landing-page-logo branding text-center put-in-front mr-auto">
      <img class="img-fluid mb-md-n5 mx-md-0 mt-md-n3 p-3" src="/{{trim( $site_settings->logo, '/' )}}" alt="{{$site_settings->site_name}}">
    </a>
    <div class="d-flex flex-column w-100 pb-5">
    <ul class="navbar-nav social-media d-flex flex-row justify-content-between ml-auto pb-3 pt-4">
    @foreach($item_sharing as $key=>$share)
      @if( $key < ( count( $item_sharing ) - 1 ) )
      <li class="mx-2">
      @else
      <li class="mr-1">
      @endif
        <a href="{{$share[0]}}" class="" title="{{ucfirst( $key )}}">
          <i class="fa {{$share[1]}}"></i>
        </a>
      </li>
    @endforeach
    </ul>
    <ul class="navbar-nav justify-content-end py-2" id="navigationDesktop">
      <li class="nav-item  none px-xl-3 px-2 @if(Request::is('home')) active @endif">
        <a class="nav-link text-uppercase none hilite" href="/home" title="home">home</a>
        <div class="ribbon home"></div>
      </li>
          <li class="nav-item  hire px-xl-3 px-2 @if(Request::is('hire')) active @endif">
        <a class="nav-link text-uppercase hire hilite" href="/hire" title="hire">hire</a>
        <div class="ribbon hire"></div>
      </li>
      <li class="nav-item  catalogue px-xl-3 px-2 @if(Request::is('catalogue')||Request::is('catalogue/*')) active @endif">
        <a class="nav-link text-uppercase catalogue hilite" href="/catalogue" title="catalogue">catalogue</a>
        <div class="ribbon catalogue"></div>
      </li>
      <li class="nav-item  film px-xl-3 px-2 @if(Request::is('film')||Request::is('film/*')) active @endif">
        <a class="nav-link text-uppercase film hilite" href="/film" title="film">film</a>
        <div class="ribbon film"></div>
      </li>
      <li class="nav-item  none px-xl-3 px-2 @if(Request::is('news')||Request::is('news/*')) active @endif">
        <a class="nav-link text-uppercase none hilite" href="/news" title="news">news</a>
        <div class="ribbon news"></div>
      </li>
      <li class="nav-item  none px-xl-3 px-2 @if(Request::is('clients')) active @endif">
        <a class="nav-link text-uppercase none hilite" href="/clients" title="clients">clients</a>
        <div class="ribbon clients"></div>
      </li>
      <li class="nav-item  none px-xl-3 px-2 @if(Request::is('gallery')) active @endif">
        <a class="nav-link text-uppercase none hilite" href="/gallery" title="gallery">gallery</a>
        <div class="ribbon gallery"></div>
      </li>
      <li class="nav-item  none px-xl-3 px-2 @if(Request::is('contact')) active @endif">
        <a class="nav-link text-uppercase none hilite" href="/contact" title="contact">contact</a>
        <div class="ribbon contact"></div>
      </li>
      <li class="nav-item social-sharing ml-lg-5 ml-0">
        <a href="javascript:void(0)" class="nav-link shareIcon pr-0">
          <img class="img=fluid icon-share" src="http://dev.trendyvault.co.za/assets/images/share.png " alt="Open Social Modal" style="fill:#f06464;" onclick="
            var a = document.querySelector( '.share-modal' ),
                p = a.parentNode;
            if ( p.classList.contains( 'fade' ) ) p.classList.toggle( 'fade' );
            setTimeout( ()=>{ p.classList.toggle( 'show' ) }, 100 );
          ">
        </a>
      </li>
    </ul>
    </div>
  </nav>
  <nav class="container navbar navbar-expand-md navbar-light justify-content-between d-flex d-lg-none">
    <a href="javascript:void(0)" class="mr-auto shareIcon">
      <img class="img=fluid icon-share" src="{{asset( 'assets/images/share.png' )}} " alt="Open Social Modal" style="fill:#f06464;" 
        onclick="
          var a = document.querySelector( '.share-modal' ),
          p = a.parentNode;
          if ( p.classList.contains( 'fade' ) ) p.classList.toggle( 'fade' );
          setTimeout( ()=>{ p.classList.toggle( 'show' ) }, 100 );
      ">
    </a>
    <a href="/home" class="landing-page-logo branding mx-auto text-center put-in-front" style="z-index:50900;">
      <img class="img-fluid" src="/{{ltrim( $site_settings->logo_mobile, '/' )}}" alt="{{$site_settings->site_name}}">
    </a>
    <div class="ml-auto navicon-container navToggle" onclick="toggle_class(this, 'change');">
      <div class="bar1"></div>
      <div class="bar2"></div>
      <div class="bar3"></div>
    </div>
  </nav>
</header>

</div>

<div class="mobile-nav-overlay collapse text-center position-absolute shadow-1 w-100 pt-5 pb-4" id="navigationMobile">
  <div class="navbar-nav">
  @php
    foreach ($route_array as $key=>$navLink):
      $hiliteKind = ($key == 'hire' || $key == 'catalogue' || $key == 'shop' || $key == 'film') ? $key : 'none';
  @endphp
    <div 
      class="nav-item mx-auto @if ($requestUri == '/'.$navLink[0] || $requestUri == '/'.$navLink[1]) active @endif"
    >
      <a class="nav-link text-uppercase {{$hiliteKind}} hilite" href="/{{$navLink[0]}}" title="{{$key}}">{{$key}}</a>
    </div>
  @endforeach

    <ul class="navbar-nav social-media d-flex flex-row justify-content-between mx-auto pt-4 pb-2">
    @foreach($item_sharing as $key=>$share)
      @if( $key < ( count( $item_sharing ) - 1 ) )
      <li class="mx-2">
      @else
      <li class="mr-1">
      @endif
        <a href="{{$share[0])" class="" title="{{ucfirst( $key ))">
          <i class="fa {{$share[1])"></i>
        </a>
      </li>
    @endforeach
    </ul>

  </div>
</div>

<div class="container">

<script type="text/javascript">
function toggle_class(x, c) {
    x.classList.toggle(c);
}
$(".navToggle").click(function (e) {
  var mobiNav = document.querySelector("#navigationMobile");
  e.preventDefault();
  setTimeout(() => {
    mobiNav.classList.toggle('show');
    mobiNav.classList.toggle('slideInLeft');
  }, 25);
});
</script>
