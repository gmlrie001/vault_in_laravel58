<?php namespace App\Http\Controllers\Vault;

/* Base Controller Include */
use App\Http\Controllers\Services\VaultController;

/* Facade Includes */
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

class TrashController extends VaultController {
    protected $model = null;

    function __construct() {
        parent::__construct();
        $tableName = Route::input('tableName');

        if(Schema::hasTable($tableName)) {
            $tableNameSingular = studly_case(strtolower(str_singular($tableName)));

            $modelNamespace = str_replace(["{", "}"], ["", ""], "App\Models\{$tableNameSingular}");
            
            try  {
                $this->model = (new $modelNamespace);
            } catch (Exception $e){
                Session::flash('error', 'Model not found');
                Redirect::to('/vault')->send();
            }
        }else{
            Session::flash('error', 'Table does not exist');
            Redirect::to('/vault')->send();
        }
    }

    public function listing($tableName){
        $this->data['table'] = $tableName;

        $this->data['titleField'] = $this->model->titleField;
        $this->data['orderField'] = $this->model->orderField;
        $this->data['statusField'] = $this->model->statusField;
        
        $this->data['mainDropdownField'] = $this->model->mainDropdownField;
        $this->data['imageDropdownField'] = $this->model->imageDropdownField;

        $this->data['can_order'] = false;

        $this->data['orderOptions'] = $this->model->orderOptions;
        $this->data['showRelationships'] = false;

        $orderField = Schema::hasColumn($tableName, Session::has('order.field')) ? Session::get('order.field') : $this->model->orderField;
        $orderDirection = Schema::hasColumn($tableName, Session::has('order.field')) ? Session::get('order.direction') : $this->model->orderDirection;
        
        $this->data['results'] = $this->model::onlyTrashed()->orderBy($orderField, $orderDirection)->paginate(Session::get('limit'));
        	
        return view('vault.trash.listing', $this->data);
    }
}