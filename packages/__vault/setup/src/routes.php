<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//+=S=E=T=U=P===================================================================+

// SITE_INITIAL_SETUP accessed on first load of site base canonical URL
Route::get(
    '/dontexist',
    function () {
        $this->data['bg_number'] = rand(1, 15);
        $this->data['seo'] = collect([]);

        // return view('setup::setup_db', $this->data);
        return view('setup::setup_step-1', $this->data);
    }
)->name('db_setup');

Route::post(
    '/dontexist/db_setup_test',
    function () {
        require($_SERVER['DOCUMENT_ROOT'] . '/..' . '/' . 'packages/vault_setup/Setup' . '.php');
        $setup = new Packages\VaultSetup\Setup;
        if ($setup->db_setup() === 0) {
            return Redirect::to('/command/migrate-pretend')->with('message', 'Migration Testing Ready and Running.');
        } else {
            return Redirect::back()->with('error', 'Please complete form correctly');
        }
    }
)->name('setup_test');

Route::get(
    '/setup/add_user_admin',
    function () {
        $this->data['bg_number'] = rand(1, 15);
        $this->data['seo'] = collect([]);
        return view('setup::setup_step-2', $this->data);
    }
)->name('setup_user_admin');

Route::post(
    '/setup/add_user_admin',
    function () {
        require($_SERVER['DOCUMENT_ROOT'] . '/..' . '/' . 'packages/vault_setup/Setup' . '.php');
        $setup = new Packages\VaultSetup\Setup;
        if ($setup->admin_setup() === 0) {
            return Redirect::to('/vault')->with('message', 'Setup Complete.');
        } else {
            return Redirect::back()->with('error', 'Please complete form correctly');
        }
    }
)->name('add_user_admin');

//+=C=O=M=M=A=N=D=S=============================================================+

// ARTISAN Command::MIGRATE
Route::get(
    '/command/migrate-pretend',
    function () { /* php artisan migrate */
        $exit_code = \Artisan::call(
            'migrate',
            [ '--pretend' => true ]
        );

        $output = trim(Artisan::output());

        echo("<h1>Migrate Pretend</h1>");
        echo("<h3>Exit Code: " . $exit_code . "</h3>");
        echo("<br><pre>" . $output . "</pre><br>");

        if ($exit_code == 0 && $output != 'Nothing to migrate.') {
            return Redirect::to('/command/migrate');
        }
    }
)->name('command_migrate_pretend');

// ARTISAN Command::MIGRATE
Route::get(
    '/command/migrate',
    function () { /* php artisan migrate */
        \Artisan::call(
            'migrate',
            [ '--seed' => true, '--force' => true ]
        );
        $output = trim(Artisan::output());
        echo("<br><pre>" . $output . "</pre><br>");

        return Redirect::to('/setup/add_user_admin');
    }
)->name('command_migrate');

//+=============================================================================+
