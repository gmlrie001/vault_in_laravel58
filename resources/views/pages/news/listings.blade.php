@extends( 'layouts.page' )

@section( 'title', $site_settings->site_name . " :: " . $page->title  )

@push( 'pageStyles' )
<style>
  body {
  @if( $news_settings->full_background_image )
    background: url( /{{ $news_settings->full_background_image }} ) center center no-repeat;
  @endif
    min-height: 100vh;
    background-attachment: fixed;
  }
  .news-block a span {
    display: block;
    width: 75px;
    color: #ffffff;
    text-align: center;
    height: 75px;
    padding-top: 2.5%;
    font-size: 18px;
    text-transform: uppercase;
    top: 0;
    left: 0;
    position: absolute;
    background: url('/assets/images/template/news-icon.png') center center no-repeat;
  }
  main {
    min-height: 85vh;
  }
  @media only screen and (max-width:992px) {
    main {
      min-height: 50vh;
    }
  }
  @media only screen and (max-width:768px) {
    main {
      min-height: 65vh;
    }
  }
  @media only screen and (max-width:576px) {
    main {
      min-height: unset;
    }
  }
</style>
@endpush

@section( 'content' )

@isset( $site_settings->logo )
<div class="container-fluid pt-2 pb-5 d-none d-lg-block">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-12 px-0">
          <a href="/">
            <img class="img-fluid" src="/{{ ltrim( $site_settings->logo, '/' ) }}" />
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
@endisset

@isset( $listings )
@php $skip = (int) $news_settings->number_of_news_items_per_row; @endphp
<div class="container-fluid">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-12 white-bg-block news-block">
          <div class="row">

            <div class="col-12">
            @if( $page->title )
              <h1 class="text-center">{{ $page->title }}</h1>
            @endif
            </div>

	    @if( $listings && count( $listings ) > 0 )
            <div class="col-12">
              <div class="row">
              @php $n = count( $listings ); @endphp
              @if( $n > 1 )
                <div class="col-12 col-lg-6">
                @for( $i = 0; $i < $n; $i += $skip )
                  @php
                    $listing = $listings[$i];
                    $date_form = explode( ' ', date( 'd M', strtotime( $listing->published_date ) ) );
                  @endphp
                  <a href="/news/{{ $listing->url_title }}">
                  @if( $date_form && is_array( $date_form ) )
                    <span>{{ $date_form[0] }}<i>{{ $date_form[1] }}</i></span>
                  @endif
                  @if( $listing->title )
                    <h4>{{ $listing->title }}</h4>
                  @endif
                  @if( $listing->summary )
                    {!! $listing->summary !!}
                  @endif
                  </a>
                @endfor
                </div>
                <div class="col-12 col-lg-6">
                @for( $j = 1; $j <= ( $n - 1 ); $j += $skip )
                  @php
                    $listing = $listings[$j];
                    $date_form = explode( ' ', date( 'd M', strtotime( $listing->published_date ) ) );
                  @endphp
                  <a href="/news/{{ $listing->url_title }}">
                  @if( $date_form && is_array( $date_form ) )
                    <span>{{ $date_form[0] }}<i>{{ $date_form[1] }}</i></span>
                  @endif
                  @if( $listing->title )
                    <h4>{{ $listing->title }}</h4>
                  @endif
                  @if( $listing->summary )
                    {!! $listing->summary !!}
                  @endif
                  </a>
                @endfor
                </div>
                @php unset( $i, $j, $n ); @endphp
	      @else
                <div class="col-12 col-lg-6">
                @foreach( $listings as $key=>$listing )
                  @php
                    $date_form = explode( ' ', date( 'd M', strtotime( $listing->published_date ) ) );
                  @endphp
                  <a href="/news/{{ $listing->url_title }}">
                  @if( $date_form && is_array( $date_form ) )
                    <span>{{ $date_form[0] }}<i>{{ $date_form[1] }}</i></span>
                  @endif
                  @if( $listing->title )
                    <h4>{{ $listing->title }}</h4>
                  @endif
                  @if( $listing->summary )
                    {!! $listing->summary !!}
                  @endif
                  </a>
                @endforeach
		</div>
              @endif
              </div>
            </div>
	    @else
            <div class="col-12">
              <div class="row">
		<div class="col-12 pt-5 m-auto text-center">
		  <h3>No News Listing Articles...</h3>
		</div>
	      </div>
            </div>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@php unset( $skip ); @endphp
@endisset

@endsection
