<section class="container section-margin">
    <div class="row home-title">
        <h1 class="col-6 col-sm-10 col-lg-11">{{ $home_feat->title }}</h1>
        <a class="col-6 col-sm-2 col-lg-1 text-right" href="{{ $home_feat->video_url }}" title="view more">+</a>
    </div>
    <div class="row home-article keyAnimation">
        <div class="col-12 col-md-6 description">
            {!! $home_feat->description !!}
        </div>
        <div class="col-12 col-md-6 image">
            <div class="border">
                <img class="img-fluid" src="/{{ $home_feat->article_image }}" alt="what-we-do">
            </div>
        </div>
    </div>
</section>

<section class="container section-margin">
    <div class="row home-title">
        <h1 class="col-6 col-sm-10 col-md-11">Our Services</h1>
        <a class="col-6 col-sm-2 col-md-1 text-right" href="/services/" title="view more">+</a>
    </div>

    <div class="row home-service">
        @foreach( $home_arts as $k=>$link )
        <div class="col-12 col-md-6 keyAnimation">
            <div class="wrap">
		@if( ( $k % 2 ) === 0 )
                <div class="image ltr">
		@else
                <div class="image">
		@endif
                    <div class="border">
                        <img class="img-fluid" src="{{ $link->article_image }}" alt="{{ $link->title }}">
                    </div>
                </div>
                <div class="row title">
                    <h2 class="col-7 col-sm-10 col-md-11">{{ $link->title }}</h2>
                    <a class="col-5 col-sm-2 col-md-1 text-right" href="{{ $link->video_url }}" title="view more">+</a>
                </div>
                <div class="description">
                    {!! str_limit( strip_tags( $link->description ), 175 ) !!}
                </div>
            </div>
        </div>
        @endforeach
    </div>
</section>
