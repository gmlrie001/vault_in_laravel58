<?php namespace App\Traits\Strings;

trait StringsTrait
{
    protected $RE_SPLIT_OPTS = ['PREG_SPLIT_NO_EMPTY','PREG_SPLIT_OFFSET_CAPTURE'];
    protected $RE_WC_SPLIT   = "/[\n\r\t ]+/isU";
    protected $RE_REPL_NOPR  = "/[[:^print:]]/isU";

    private $ellipsis = '...';

    // public function __construct( $str=null ) {
    //     if ( ! is_null( $str ) ) return;
    //     return $this->clean_string( $str );
    // }

    /* USE Laravel\'s str_limit( $str, $limit ) instead. */
    public function truncate_by_chars($text, $limit, $ellipsis='...')
    {
        if (strlen($text) > $limit) {
            // $endpos = strpos( str_replace( array( "\r\n", "\r", "\n", "\t" ), ' ', $text ), ' ', $limit );
            // if (! $endpos ) $text = trim(substr($text, 0, $endpos)) . $ellipsis;
            $text = str_limit($this->clean_string($str), $limit);
        }

        return $text;
    }

    public function clean_string($str=null)
    {
        if (null === $str) {
            return;
        }

        $tmp = $str;
        $tmp = preg_replace($this->RE_REPL_NOPR, "", $tmp);
        $tmp = preg_replace('/\s+/', " ", $tmp);
        $tmp = preg_replace('/<(script|style)[^>]*?>.*?<\\/\\1>/isU', '', $str);

        $tmp =       strip_tags($tmp);
        $tmp =             trim($tmp);
        $tmp = htmlspecialchars($tmp);

        if (get_magic_quotes_gpc()) {
            $tmp = stripslashes($tmp);
        }
        
        return $tmp;
    }

    public function truncate_by_words($text, $limit, $ellipsis='...')
    {
        $text  = $this->clean_string($text);
        $words = preg_split($this->RE_WC_SPLIT, $text, $limit + 1, PREG_SPLIT_NO_EMPTY|PREG_SPLIT_OFFSET_CAPTURE);

        if (count($words) > $limit) {
            // Ignore last element since it contains the rest of the string
            end($words);
            $last_word = prev($words);
            $text =  substr($text, 0, $last_word[1] + strlen($last_word[0])) . $ellipsis;

            return $text;
        }

        return $text;
    }

    private function countWords($text) : int
    {
        $text  = $this->clean_string($text);
        $words = preg_split($this->RE_WC_SPLIT, $text, -1, PREG_SPLIT_NO_EMPTY);

        return count($words);
    }

    public function style_last_word($inp="")
    {
        $text_string = (string) $inp;
        $text        = $this->clean_string($text_string);

        if ($text_string == "" || is_null($text_string)) {
            return;
        }

        $array     = explode(' ', $text_string);
        $last_word = array_pop($array);

        return [
            implode(' ', $array), $last_word
        ];
    }
}
