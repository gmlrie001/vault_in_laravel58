<!DOCTYPE html>
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr">

    <head>
        <!-- Fonts and Reset CSS -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" media="print" onload="this.media='all';return;">
        <link rel="" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css" media="print" onload="this.media='all';return;">

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .d-none {
              display: none !important;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            figure {
              margin-block-start: 0;
              margin-block-end: 0;
              margin-inline-start: 0;
              margin-inline-end: 0;
            }
        </style>
    </head>

    @php ob_flush(); @endphp

    <body>

      <a href="{{ url()->previous() }}">Gou Terug</a>

      <div class="container-fluid d-none">
       @forelse( $banners as $k=>$banner )
        @forelse( $banner->blocks as $key=>$block )
        <figure class="m-0 p-0 mw-100">
          <picture>
          @isset( $block->mobile_image )
            <source srcset="/{{ ltrim( $block->mobile_image ) }}" media="only screen and (max-width: 992px)">
          @endisset
          @isset( $block->banner_image )
            <source srcset="" media="only screen and (max-width: 992px)">
            <img class="img-fluid" src="/{{ ltrim( $block->banner_image, '/' ) }}" alt="{{ ucwords( $block->title ) }}">
          @endisset
          </picture>
        </figure>
        @empty
        @endforelse
       @empty
       @endforelse
      </div>

      <div class="flex-center position-ref full-height">
      @if (Route::has('login'))
          <div class="top-right links">
              @auth
                  <a href="{{ url('/home') }}">Home</a>
              @else
                  <a href="{{ route('login') }}">Login</a>
                  @if (Route::has('register'))
                      <a href="{{ route('register') }}">Register</a>
                  @endif
              @endauth
          </div>
      @endif
          <div class="content">
              <div class="title m-b-md">
                  @if( env( 'APP_NAME' ) ) 
                      {{ env( 'APP_NAME' ) }} &ndash; Vault
                  @else
                      Vault
                  @endif
              </div>
              <div class="links">
                  <a href="http://dev.pretty-docs.co.za">Docs</a>
                  <a href="/dontexist">Vault Setup</a>
                  <a href="{{ route( 'the_vault' ) }}">Vault</a>
              </div>
          </div>
      </div>

    </body>

</html>
