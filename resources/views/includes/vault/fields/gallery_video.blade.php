@php
        $modelNamespace = str_replace( ["{", "}"], ["", ""], "App\Models\{$options}" );
        $model = ( new $modelNamespace );
        $allow_media_popup = $model::pluck( 'allow_media_popup' )->first();
        $options = $allow_media_popup;
@endphp

{{-- dd( $options, $name, $label, get_defined_vars() ) --}}

@if( $allow_media_popup )
  {!! Form::label( $name, $label ) !!}
  {!! Form::text( $name, null, array( 'placeholder' => $label, 'class' => 'form-control' ) ) !!}
@endif
