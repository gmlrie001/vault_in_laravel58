@if(Session::has('message'))
	<div class="alert alert-success container-fluid">
		<div class="container container-custom">
			<p class="col-xs-12"><i class="fa fa-check" aria-hidden="true"></i>{{Session::get('message')}}<i class="fa fa-times" aria-hidden="true"></i></p>
		</div>
	</div>
@endif
@if(Session::has('error'))
	<div class="alert alert-danger container-fluid">
		<div class="container container-custom">
			<p class="col-xs-12"><i class="fa fa-info" aria-hidden="true"></i>{{Session::get('error')}}<i class="fa fa-times" aria-hidden="true"></i></p>
		</div>
	</div>
@endif
@if ($errors->any())
	@foreach ($errors->all() as $error)
		<div class="alert alert-danger container-fluid">
			<div class="container container-custom">
				<p class="col-xs-12"><i class="fa fa-info" aria-hidden="true"></i>{{ $error }}<i class="fa fa-times" aria-hidden="true"></i></p>
			</div>
		</div>
	@endforeach
@endif