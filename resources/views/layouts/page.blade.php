<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}" dir="ltr">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">

    <title>@yield( 'title' )</title>

    <!-- include( 'includes.pages.additions.header_seo' ) -->
    @include( 'includes.pages.additions.header_assets' )

  <!-- Slick Slider CSS Theme -->
    <!-- <link rel="stylesheet" href="/assets/vendor/slick-slider/slick.css" type="text/css" media="print" onload="this.media='all';">
    <link rel="stylesheet" href="/assets/vendor/slick-slider/slick-theme.css" type="text/css" media="print" onload="this.media='all';"> -->

  <!-- Site Styles -->
    <!-- <link rel="stylesheet" type="text/css" href="/assets/css/styles.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/modules/home/homepage_banner.css"> -->

  <!-- Font Awesome 5.8.1 SHIM via CDN or LOCAL -->
    <link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" media="print" onload="this.media='all';">
    <link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.8.1/css/v4-shims.css" media="print" onload="this.media='all';">

  <!-- Bootstrap Datepicker -->
    <!-- <link type="text/css" rel="stylesheet" href="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/css/bootstrap-datepicker3.min.css" media="print" onload="this.media='all';"> -->

  <!-- Magnific Popup -->
    <!-- <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.css" media="print" onload="this.media='all';"> -->

    @stack( 'pageStyles' )

  <!-- JQuery (slim version has NO AJAX or Effects)-->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js" integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8=" crossorigin="anonymous"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
  <!-- INSECURE Version: DO NOT use version < 3.4.1 -->

  <!-- Load Website Google Fonts: Single API Call | Separate Calls -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
    <script>
      WebFont.load({
        google: {
          families: [
            'Roboto+Slab:100,300,400,700',
          ]
        }
      });
    </script>

    @stack( 'pageHDRScripts' )

    <style>
    nav {
      background-color: #b71c1c;
      color: #ffffff;
    }
    nav * {
      color: #ffffff;
      line-height: 50px;
      padding: 0 1.618rem;
    }
    main {
      min-height: calc( 100vh - 10vh );
      background-color: #ffffff;
    }
    main .container.vault-content {
      background-color: #181818a1;
      margin: 3rem auto !important;
    }
    [hidden], .hidden {
      display: none;
    }
    button[type=submit],
    a.btn.btn-skip {
      height: 50px;
      margin-bottom: 15px;
      margin-top: 15px;
      padding: 0 1rem;
      border-radius: 0.375rem;
      border: none !important;
      box-shadow: -2px 2px 3px 0 rgba(10,10,10,1) !important;
      background-color: #b71c1c;
      font-size: 16px;
      font-weight: 600;
      color: #ffffff;
      line-height: 50px;
      letter-spacing: calc( 1em * ( 20 / 1000 ) );
    }
    a.btn.btn-skip:hover,
    button:hover,
    a.btn.btn-skip:active,
    button[type=submit]:hover,
    button:active,
    a.btn.btn-skip:focus,
    button[type=submit]:active,
    button:focus,
    button[type=submit]:focus {
      background-color: #b71c1c !important;
      border-color: #b71c1c !important;
    }
    a.btn.btn-skip:active,
    button:active,
    button[type=submit]:active {
      box-shadow: -1px 1px 4px 0 rgba(10,10,10,0.6) !important;
    }
    .d-contents {
      display: contents;
    }
    .no-list-style {
      list-style: none;
    }
    .side-nav .navbar {
      display: none !important;
    }
    </style>

  </head>

  <body class="position-relative">

    <!-- HEADER and NAV -->
    @include( 'includes.pages.header.bootstrap4' )

    <!-- MAIN -->
      <!-- include( 'includes.page_banners' ) -->
      <!-- include( 'includes.pages.messages.brombacher' ) -->
    @yield( 'content' )

    <!-- FOOTER -->
    <!-- include( 'includes.pages.footer.footer' ) -->

    <!-- SHARE-MODAL -->
    <!-- include('includes.pages.modals.share_follow_modal') -->

    <!-- POST Content Scripts Loading -->
    <!-- <script async defer type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script> -->

    <!-- include( 'includes.pages.additions.footer_scripts' ) -->
    @stack( 'pageScripts' )

    <script type="text/javascript">
      function get_element(selector=null) {
        return document.querySelectorAll( selector );
      }

      function truncateByWords(array, max=10) {
        var word_split_join = ' ', ellipsis = '...';
        if ( array == null || array == undefined || array == 'undefined' || array.length == 0 ) return;
        try {
          array.map(
            s => {
              var q = s.innerText.split( word_split_join );
              var n = q.splice( 0, max );
              n.push( ellipsis );
              s.innerText = n.join( word_split_join );
            }
          );
        } catch(err) { /**/ }
        return;
      }
    </script>

    <script type="text/javascript">
      document.addEventListener('DOMContentloaded', () => {
        var navLinks = [].slice.call( document.querySelectorAll( 'a:not(.card), a:not(.card)[href]' ) );
        navLinks.map( e => {
          try {
            if ( e.hasAttribute( 'href' ) && ( ! e.href.match( /\w+?\.\w{2,5}$/ig ) && ! e.href.match( /\/$/ig ) && ( ! e.href !== "" || e.href !== null ) ) ) {
              e.href += '/';
            }
          } catch( err ) {
            console.error( "\r\n" + err + "\r\n" );
          }
        });
      });
    </script>

  </body>

</html>
