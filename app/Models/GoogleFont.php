<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;

class GoogleFont extends Model
{
    use SoftDeletes;
    
    public $orderable = true;
    public $orderField = "order";
    public $titleField = "family";
    public $statusField = "status";
    public $hasStatus = true;
    public $orderDirection = "asc";
    public $parentOrder = "";
    public $parentTable = "";
    public $orderOptions = ['family', 'category'];
    public $relationships = [];
    public $mainDropdownField = "category";
    public $imageDropdownField = "";

    /**
     * This sets the PRIMARY KEY for the current model.
     *
     * @var string
     */
    // protected $primaryKey = 'family';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'kind',
        'family',
        'category',
        'variants',
        'subsets',
        'version',
        'lastModified',
        'files',
        'status',
        'status_date',
    ];

    /**
     * The attributes need to be cast as either an array or JSON.
     *
     * @var array
     */
    protected $casts = [
        'variants' => 'array',
        'subsets'  => 'array',
        'files'    => 'object',
    ];

    // public $fields = [
    // //  ['field_name', 'label', 'field_type', 'options_model', 'options_relationship', 'width', 'height', 'container_class', 'can_remove'],
    //     ['kind', 'Title', 'title', '', '', '', '', 'col-xs-12 col-md-6', ''],
    //     ['family', 'URL Title', 'url_title', '', '', '', '', 'col-xs-12 col-md-6', ''],
    //     ['category', 'Publication Date', 'date', '', '', '', '', 'col-xs-12 col-md-6', ''],

    //     ['open_parent', 'Item Desciption', ''],
    //         // ['open_row', '',''],
    //         //     ['listing_description', 'Shorter LISTING PAGE Description', 'textarea', '', '', '', '', 'col-xs-12 col-md-12', ''],
    //         // ['close_row', '',''],
    //         ['open_row', '',''],
    //             ['variants', 'Description', 'wysiwyg', '', '', '', '', 'col-xs-12 col-md-12', ''],
    //         ['close_row', '',''],
    //     ['close_parent', 'Item Desciption', ''],

    //     ['open_parent', 'Listing and Article Image', ''],
    //         ['open_row', '',''],
    //             ['subsets', 'Listing', 'image', '', '', '800', '600', 'col-xs-12 col-md-6', 'can_remove'],
    //             ['versions', 'Article', 'image', '', '', '800', '600', 'col-xs-12 col-md-6', 'can_remove'],
    //         ['close_row', '',''],
    //     ['close_parent', 'Listing and Article Image', ''],

    //     ['open_parent', 'SEO Information', ''],
    //         ['open_row', '', ''],
    //             ['lastModified', 'SEO Title', 'text', '', '', '', '', 'col-xs-12 col-md-6', ''],
    //         ['close_row', '', ''],
    //         ['open_row', '', ''],
    //             ['files', 'SEO Description', 'textarea', '', '', '', '', 'col-xs-12 col-md-6', ''],
    //         ['close_row', '', ''],
    //     ['close_parent', 'SEO Information', ''],

    //     ['open_row', '',''],
    //         ['status', 'Status', 'status', '', '', '', '', 'col-xs-12 col-md-6', ''],
    //     ['close_row', '',''],
    // ];

    // public function categories()
    // {
    //     return $this->hasMany(BlogGallery::class, 'blog_id')->where('status', 'PUBLISHED')->orWhere('status', 'SCHEDULED')
    //                 ->where('status_date', '<=', now());
    // }

    // public function subsets()
    // {
    //     return $this->hasMany(BlogGallery::class, 'blog_id')->where('status', 'PUBLISHED')->orWhere('status', 'SCHEDULED')
    //                 ->where('status_date', '<=', now());
    // }

    // public function variants()
    // {
    //     return $this->hasMany(BlogShareLink::class, 'blog_id')->where('status', 'PUBLISHED')->orWhere('status', 'SCHEDULED')
    //                 ->where('status_date', '<=', now());
    // }

    /**
     * Scope a query to only include certain status'.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeStatus($query)
    {
        return $query->where('status', 'PUBLISHED')->orWhere('status', 'SCHEDULED')
                     ->where('status_date', '<=', now());
    }

    /**
     * Scope a query to only include certain status'.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOrdering($query, $col=null, $dir=null)
    {
        $orderCol = ($col != null) ? $col : $this->orderField;
        $orderDir = ($dir != null) ? $dir : ($this->orderDirection != null) ? $this->orderDirection : 'asc';

        return $query->status()->orderBy($orderCol, $orderDir);
    }
}
