<div class="navbar">    
    <div class="padding-0">           
        <ul class="nav-list">
            <!-- DAshboard Nav -->
            <li class="main-link dashboard-link">
                <a href="/vault">
                    <i class="fa col-md-2 text-center"><img src="{{ url('/').'/assets/images/vault/logo/icon_logo.png' }}" width="20px" /></i>
                    <span>Dashboard</span>
                </a>
            </li>
            @php
                $sections = config('navItems');
                $levels = config('admin_levels');
                $levels = $levels['admin_levels'];

                $userLevel  = ( Session::has('user.admin_type') ) ? Session::get('user.admin_type') : null;
                $has_auth   = in_array( $userLevel, array_keys( $levels ) );

                if ( null != $userLevel && $has_auth ) {
                  /** 
                   * AUTHentication::CHECK true
                   * User is logged in with the correct credentials to proceed.
                   */
                    $userAccess = $levels[$userLevel]['access'];
                    $sections   = $sections['items'];
                } else {
                  /**
                   * REDIRECT (as http_response=301):- to Vault Login.
                   *
                   * This is possibly due to Session expiry or some other reason
                   * for Session variables to have been unset.
                   *
                   * //header( "HTTP/1.1 301 Moved Permanently" ); <-- Set Response CODE & TEXT
                   * //header( "Location: " . "/vault/login" ) <-- Sets the URI for redirection
                   * //exit(); <-- ENSURE that nothing else past here gets executed
                   * _OR_
                   * By using the one-liner executed below.
                   */
                    exit(
                        header( "Location: " . "/vault/login", true, 301 )
                    );
                }
            @endphp
            @foreach($sections as $key => $section)
                @php
                    $currentUrlPath = str_replace("vault/", "", Request::path());
                    $mainTables = array_keys($section['main_link_database_tables']);
                    $tabTables = $section['tab_database_tables'];
                    $mainTableActive = false;
                    foreach($mainTables as $mainTable){
                        if(stripos($currentUrlPath, $mainTable) === 0){
                            $mainTableActive = true;
                            break;
                        }
                    }
                    $tabTableActive = false;
                    foreach($tabTables as $tabTable){
                        if(stripos($currentUrlPath, $tabTable) === 0){
                            $tabTableActive = true;
                            break;
                        }
                    }

                    if($mainTableActive || $tabTableActive){
                        $setActive = true;
                    }else{
                        $setActive = false;
                    }

                    if($section['page_id'] != 0 && Request::path() == "vault/pages/edit/".$section['page_id']){
                        $setActive = true;
                        $pageActive = true;
                    }else{
                        $pageActive = false;
                    }
                    $sectionAllowed = false;
                    foreach($section['main_link_database_tables'] as $key => $mainLink){
                        if(in_array($key, $userAccess)){
                            $sectionAllowed = true;
                        }
                    }
                @endphp
                @if($sectionAllowed || $section['page_id'] != 0)
                    <li class="main-link">
                        <a href="{{$key}}">
                            <i class="{{$section['icon']}} col-md-2 text-center"></i>
                            <span>{{$section['title']}}</span>
                            @if($setActive)
                                <i class="fa fa-chevron-down drop-logo"></i>
                            @else
                                <i class="fa fa-chevron-right drop-logo"></i>
                            @endif
                        </a>
                    </li>
                    <ul id="{{$key}}" class="link-dropdown 
                        @if($setActive)
                            active
                        @endif
                    "> 
                        @if($section['page_id'] != 0)
                            @if($pageActive)
                                <li class="active"><span class="bg-colour"></span><a href="/vault/pages/edit/{{$section['page_id']}}"><i class="fa fa-file-o col-md-2 text-center" aria-hidden="true"></i><span>Page</span></a></li>
                            @else
                                <li><span class="bg-colour"></span><a href="/vault/pages/edit/{{$section['page_id']}}"><i class="fa fa-file-o col-md-2 text-center" aria-hidden="true"></i><span>Page</span></a></li>
                            @endif
                        @endif
                        @foreach($section['main_link_database_tables'] as $key => $mainLink)
                            @if(in_array($key, $userAccess))
                                @php
                                    $mainLinkTable = $key;
                                    $tabLinkTables = $mainLink['sub_database_tables'];
                                    $mainLinkTableActive = stripos($currentUrlPath, $mainLinkTable) === 0;
                                    $tabLinkTableActive = false;
                                    foreach($tabLinkTables as $tabLinkTable){
                                        if(stripos($currentUrlPath, $tabLinkTable) === 0){
                                            $tabLinkTableActive = true;
                                            break;
                                        }
                                    }

                                    if($mainLinkTableActive || $tabLinkTableActive){
                                        $setLinkActive = true;
                                    }else{
                                        $setLinkActive = false;
                                    }
                                @endphp
                                @if($setLinkActive)
                                    @if($mainLink['specific_id'] == '')
                                        <li class="active"><span class="bg-colour"></span><a href="/vault/{{$key}}"><i class="{{$mainLink['icon']}} col-md-2 text-center" aria-hidden="true"></i><span>{{$mainLink['title']}}</span></a></li>
                                    @else
                                        <li class="active"><span class="bg-colour"></span><a href="/vault/{{$key}}/edit/{{$mainLink['specific_id']}}"><i class="{{$mainLink['icon']}} col-md-2 text-center" aria-hidden="true"></i><span>{{$mainLink['title']}}</span></a></li>
                                    @endif
                                @else
                                    @if($mainLink['specific_id'] == '')
                                        <li><span class="bg-colour"></span><a href="/vault/{{$key}}"><i class="{{$mainLink['icon']}} col-md-2 text-center" aria-hidden="true"></i><span>{{$mainLink['title']}}</span></a></li>
                                    @else
                                        <li><span class="bg-colour"></span><a href="/vault/{{$key}}/edit/{{$mainLink['specific_id']}}"><i class="{{$mainLink['icon']}} col-md-2 text-center" aria-hidden="true"></i><span>{{$mainLink['title']}}</span></a></li>
                                    @endif
                                @endif
                            @endif
                        @endforeach
                    </ul>
                @endif
            @endforeach
            <!-- blank spacer -->
            <li class="main-link spacer"><a href="#"></a></li>
        </ul>
    </div>   
</div>