<?php namespace App\Http\Controllers\Vault;

/* Base Controller Include */
use App\Http\Controllers\Services\VaultController;

/* Facade Includes */
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

class OrderController extends VaultController {
    protected $model = null;

    function __construct() {
        $tableName = Route::input('tableName');

        if(Schema::hasTable($tableName)) {
            $tableNameSingular = studly_case(strtolower(str_singular($tableName)));

            $modelNamespace = str_replace(["{", "}"], ["", ""], "App\Models\{$tableNameSingular}");
            
            try  {
                $this->model = (new $modelNamespace);
            } catch (Exception $e){
                Session::flash('error', 'Model not found');
                Redirect::to('/vault')->send();
            }
        }else{
            Session::flash('error', 'Table does not exist');
            Redirect::to('/vault')->send();
        }
    }

    public function moveUp($tableName, $id){
        $entry = $this->model::find($id);
        $orderField = $this->model->orderField;

        if($this->model->orderable == true){
            if($this->model->parentOrder == ""){
                $currentItemOrder = $entry->{$orderField};
                
		        if($currentItemOrder != 1){
                    $this->model::where($orderField ,$currentItemOrder[0]-1)->increment($orderField);
                    $this->model::where('id', $entry->id)->decrement($orderField);
                }
            }else{
                $currentItemOrder = $entry->{$orderField};

                if($currentItemOrder != 1){
                    $this->model::where($this->model->parentOrder, $entry->{$this->model->parentOrder})->where($orderField ,$currentItemOrder[0]-1)->increment($orderField);
                    $this->model::where($this->model->parentOrder, $entry->{$this->model->parentOrder})->where('id', $entry->id)->decrement($orderField);
                }
            }
        }
		
		return Redirect::back()->with('message', 'Successfully reordered');
    }
    
    public function moveDown($tableName, $id){
        $entry = $this->model::find($id);
        $orderField = $this->model->orderField;

        
        if($this->model->orderable == true){
            if($this->model->parentOrder == ""){
                $totalCount = $this->model::count();
                $currentItemOrder = $entry->{$orderField};
                
		        if($currentItemOrder != $totalCount){
                    $this->model::where($orderField ,$currentItemOrder[0]+1)->decrement($orderField);
                    $this->model::where('id', $entry->id)->increment($orderField);
                }
            }else{
                $totalCount = $this->model::where($this->model->parentOrder, $entry->{$this->model->parentOrder})->count();
                $currentItemOrder = $entry->{$orderField};

                if($currentItemOrder != $totalCount){
                    $this->model::where($this->model->parentOrder, $entry->{$this->model->parentOrder})->where($orderField ,$currentItemOrder[0]+1)->decrement($orderField);
                    $this->model::where($this->model->parentOrder, $entry->{$this->model->parentOrder})->where('id', $entry->id)->increment($orderField);
                }
            }
        }
		
		return Redirect::back()->with('message', 'Successfully reordered');
    }
    
    public function bulkOrder($tableName){
        if($this->model->orderable == true){
            $current_order = Input::get('current_order', 0);
            $new_order = Input::get('new_order', 0);

            if($this->model->parentOrder == ""){
                $exists = $this->model::where($this->model->orderField, $new_order)->exists();

                if(is_numeric($new_order) && $exists){
                    
                    if($new_order && $current_order && ($current_order != $new_order)){
                        $current_row = $this->model::where($this->model->orderField, $current_order)->first();
                        $new_row = $this->model::where($this->model->orderField, $new_order)->first();
                        
                        $this->model::whereId($current_row->id)->update([$this->model->orderField => $new_order]);
                        $this->model::whereId($new_row->id)->update([$this->model->orderField => $current_order]);
                    }
                }else {
                    return Redirect::back()->with('error', 'Order Number does not exist');
                }
            }else{
                $exists = $this->model::where($this->model->orderField, $new_order)->exists();
                $entry = $this->model::find(Input::get('entry', 0));

                if(is_numeric($new_order) && $exists){
                    
                    if($new_order && $current_order && ($current_order != $new_order)){
                        $current_row = $this->model::where($this->model->parentOrder, $entry->{$this->model->parentOrder})->where($this->model->orderField, $current_order)->first();
                        $new_row = $this->model::where($this->model->parentOrder, $entry->{$this->model->parentOrder})->where($this->model->orderField, $new_order)->first();
                        
                        $this->model::whereId($current_row->id)->update([$this->model->orderField => $new_order]);
                        $this->model::whereId($new_row->id)->update([$this->model->orderField => $current_order]);
                    }
                }else {
                    return Redirect::back()->with('error', 'Order Number does not exist');
                }
            }
                
            return Redirect::back()->with('message', 'Successfully reordered');
        }else{
            return Redirect::back()->with('error', 'Entry cannot be ordered');
        }
    }

    public function reorderList($tableName){
        if($this->model->orderable == true){
            if($this->model->parentOrder == ""){
                $reorderedItem = (object) Input::all();
                if($reorderedItem->orderOrigin > $reorderedItem->orderAbove){
                    $this->model::where($this->model->orderField, '<=', $reorderedItem->orderOrigin)->where($this->model->orderField,'>', $reorderedItem->orderAbove)->increment($this->model->orderField);
                    $this->model::whereId($reorderedItem->id)->update([$this->model->orderField => ($reorderedItem->orderAbove+1)]);
                }else{
                    $this->model::where($this->model->orderField, '<=', $reorderedItem->orderAbove)->where($this->model->orderField,'>', $reorderedItem->orderOrigin)->decrement($this->model->orderField);
                    $this->model::whereId($reorderedItem->id)->update([$this->model->orderField => $reorderedItem->orderAbove]);
                }
                $reorderedList = $this->model::select(['id', $this->model->orderField])->get();
            }else{
                $reorderedItem = (object) Input::all();
                $entry = $this->model::find($reorderedItem->id);
                if($reorderedItem->orderOrigin > $reorderedItem->orderAbove){
                    $this->model::where($this->model->parentOrder, $entry->{$this->model->parentOrder})->where($this->model->orderField, '<=', $reorderedItem->orderOrigin)->where($this->model->orderField,'>', $reorderedItem->orderAbove)->increment($this->model->orderField);
                    $this->model::whereId($reorderedItem->id)->update([$this->model->orderField => ($reorderedItem->orderAbove+1)]);
                }else{
                    $this->model::where($this->model->parentOrder, $entry->{$this->model->parentOrder})->where($this->model->orderField, '<=', $reorderedItem->orderAbove)->where($this->model->orderField,'>', $reorderedItem->orderOrigin)->decrement($this->model->orderField);
                    $this->model::whereId($reorderedItem->id)->update([$this->model->orderField => $reorderedItem->orderAbove]);
                }
                $reorderedList = $this->model::where($this->model->parentOrder, $entry->{$this->model->parentOrder})->select(['id', $this->model->orderField])->get();
            }
        }
        return Response::json(array('success' => true, 'reorderedList' => $reorderedList));
    }
    public function restoreOrder($tableName){
        if($this->model->orderable == true){
            if($this->model->parentOrder == ""){
                $items = $this->model::orderBy($this->model->orderField, 'asc')->orderBy('created_at', 'asc')->get();
                $currentOrder = 1;
                foreach($items as $item){
                    $item->{$this->model->orderField} = $currentOrder;
                    $item->save();
                    $currentOrder++;
                }
            }else{
                $items = $this->model::orderBy($this->model->parentOrder, 'asc')->orderBy($this->model->orderField, 'asc')->orderBy('created_at', 'asc')->get();
                $currentOrder = 1;
                $currentParent = 0;
                foreach($items as $item){
                    if($currentParent != $item->{$this->model->parentOrder}){
                        $currentParent = $item->{$this->model->parentOrder};
                        $currentOrder = 1;
                    }
                    $item->{$this->model->orderField} = $currentOrder;
                    $item->save();
                    $currentOrder++;
                }
            }
        }
        return Redirect::back()->with('message', 'Order restored!');
    }
}