<div class="col-xs-12 padding-0 function-row">
    @include('includes.vault.listing.search')
    @include('includes.vault.listing.export_import')
    @include('includes.vault.listing.order_by')
    @include('includes.vault.listing.page_limit')
    <a class="pull-left function-button tooltipster" href="/vault/{{$table}}/trash">
        <span class="tooltipster" data-tooltipster='{"side":"top","animation":"fade"}' data-tooltip-content="#tooltip_content_search">
            <img src="/assets/images/vault/template/functions/trash-icon.svg" />
            <span>Trash</span>
        </span>
    </a>
    @if($can_order)
        <a class="pull-left function-button tooltipster" href="/vault/{{$table}}/order/restore">
            <span class="tooltipster" data-tooltipster='{"side":"top","animation":"fade"}' data-tooltip-content="#tooltip_content_reset_order">
                <img src="/assets/images/vault/template/functions/restore-order.svg" />
                <span>Restore Order</span>
            </span>
        </a>
    @endif
</div>
<script>
    $(".function-button > span").click(function(){
        $('.function-dropdown').slideUp(500);
        if($(this).parent().find('.function-dropdown').css('display') == 'block'){
            $(this).parent().find('.function-dropdown').slideUp(500);
        }else{
            $(this).parent().find('.function-dropdown').slideDown(500);
        }
    });

    $(".function-search input").focus(function(){
        $(".function-search").addClass('active');
    }).blur(function(){
        $(".function-search").removeClass('active');
    });
</script>