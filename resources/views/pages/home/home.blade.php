@extends( 'layouts.page' )

@section( 'title', $site_settings->site_name . " :: " . $page->title  )

@php
  $image_stack = [];
  foreach( $articles as $key=>$article ) {
    if( (( $key + 1 ) % 2) === 0 ):
      $image_stack[] = ( $article->article_image ) ? "url(/".ltrim( $article->article_image, '/' ).") bottom right no-repeat" : null;
    else:
      $image_stack[] = ( $article->article_image ) ? "url(/".ltrim( $article->article_image, '/' ).") bottom left no-repeat" : null;
    endif;
  }
  $article_2imgbg = implode( ', ', $image_stack );
  unset( $key, $article, $image_stack );
@endphp

@push( 'pageStyles' )
<style>
  .two-image-bg {
    background: {{ $article_2imgbg }};
    background-size: 15vw, 15vw;
  }
</style>
@endpush

@section( 'content' )

<a href="{{ url()->previous() }}">Back</a>

<div class="container-fluid py-5 two-image-bg">
  <div class="row">
    <div class="container">
      <div class="row">
        <h1 class="col-12 text-center">Introduction</h1>
        @foreach( $articles as $key=>$article )
        <div class="col-12 col-md-6 col-lg-6">
          <h2>{{ $article->title }}</h2>
          {!! $article->description !!}
        </div>
        @endforeach
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <a href="/">
      @isset( $settings->introduction_bottom_banner )
      <img class="img-fluid d-none d-lg-block" src="/{{ ltrim( $settings->introduction_bottom_banner, '/' ) }}" alt="Introdution Section Bottom Banner">
      @endisset
      @isset( $settings->introduction_bottom_banner_mobile )
      <img class="img-fluid d-block d-lg-none" src="/{{ ltrim( $settings->introduction_bottom_banner_mobile, '/' ) }}" alt="Introdution Section Bottom Banner">
      @endisset
    </a>
  </div>
</div>

<div class="container-fluid py-5">
  <div class="row">
    <div class="container">
      <div class="row justify-content-between">
        <h1 class="col-12 text-center bottom-border-display">{{ $settings->wine_title }} <span>{{ $settings->wine_subtitle }}</span></h1>
        @foreach( $features as $key=>$feature )
        <a title="Wines" class="col-12 col-md-6 col-lg-5 wine-block" href="{{ route( 'wines' ) }}">
          <h2>{{ $feature->title }}</h2>
          <span class="year">{{ $feature->type }}</span>
          <p>{{ $feature->variety }}</p>
          {!! $feature->tasting !!}
          <i>Read more...</i>
          <img class="img-fluid" src="/{{ ltrim( $feature->image, '/' ) }}" />
        </a>
        @endforeach
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <a href="/">
      @isset( $settings->wine_bottom_banner )
      <img class="img-fluid d-none d-lg-block" src="/{{ ltrim( $settings->wine_bottom_banner, '/' ) }}" alt="Wines Section Bottom Banner">
      @endisset
      @isset( $settings->wine_bottom_banner_mobile )
      <img class="img-fluid d-block d-lg-none" src="/{{ ltrim( $settings->wine_bottom_banner_mobile, '/' ) }}" alt="Wines Section Bottom Banner">
      @endisset
    </a>
  </div>
</div>

<div class="container-fluid py-5">
  <div class="row">
    <div class="container">
      <div class="row">
        <h1 class="col-12 text-center bottom-border-display">{{ $settings->tour_title }}<span></span></h1>
        <div class="col-12 col-lg-12 text-center">
          {!! $settings->tour_description !!}
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="card-columns gallery">
    @foreach( $listings as $key=>$listing )
      <a title="{{ $listing->title }}" class="card" href="/{{ ltrim( $listing->card_image, '/' ) }}">
        <img class="card-img" src="/{{ ltrim( $listing->card_image, '/' ) }}" alt="{{ $listing->title }}">
      </a>
    @endforeach
    </div>
  </div>
</div>

<div class="container-fluid py-5">
  <div class="row">
    <div class="col-12 text-center">
      <a title="Vists Tours & Events Page" class="goto-link" href="{{ route( 'tours' ) }}">View more tours & Events</a>
    </div>
  </div>
</div>
@endsection
