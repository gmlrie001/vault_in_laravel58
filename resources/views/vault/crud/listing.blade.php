@extends('layouts.vault')

@section('content')
    <div class="container-fluid content">
        @include('includes.vault.listing.tabs')

        <div class="col-xs-12 listing-container padding-0">
            @include('includes.vault.listing.functions')
            <div class="col-xs-12 padding-0 listing-headings">
                <span class="col-md-1 text-center"></span>
                <span class="col-md-2 text-center">Order</span>
                <span class="col-md-4">Title</span>
                <span class="col-md-2 text-center">Status</span>
                <span class="col-md-2 text-center">Last Modified</span>
                <span class="col-md-1 text-right">Edit</span>
            </div>
            <div class="col-xs-12 padding-0 list">
                @foreach($results as $result)
                    @include('includes.vault.listing.listing_row', ['result' => $result])
                @endforeach
            </div>
        </div>
    </div>
    @if( ! Session::has('search_results') )
        @include('includes.vault.listing.pagination')
    @endif
    @include('includes.vault.listing.tooltips')
    @include('includes.vault.listing.mass_operation')
    <script>
        $("input[type='checkbox']").change(function(){
            if($(this).is(':checked')){
                $(this).parent().addClass('active');
            } else {
                $(this).parent().removeClass('active');
            }
        });
        
        $(".resizeme").fitText(1, { minFontSize: '9px', maxFontSize: '15px' });

        $(".listing-row .accord").click(function(e){
            e.preventDefault();
            $(this).toggleClass('fa-plus fa-minus');
            $(this).closest(".listing-row").toggleClass('active');
            $(this).closest(".listing-row").find('.listing-dropdown').slideToggle();
        });
        
        if($(window).width() > 1024){
            $(".options-open").mouseenter(function(){
                $(this).find('.options').css('width', 45*$(this).find('.options').children("a").length+"px");
            }).mouseleave(function(){
                $(this).find('.options').removeAttr("style");
            });
        }else{
            $(".options-open .fa-gear").click(function(){
                $(this).closest('.listing-row').find('.mobile-options').slideToggle();
            });
        }

        $('.mass-operation').on('change', function(event){
            if($('.mass-operation:checked').length > 0){
                $('.selected-options').fadeIn();
            }else {
                $('.selected-options').fadeOut();
            }
        });

        $('.get-selected-items').on('click', function(event) {
            event.preventDefault();
            var selected = [];
            $('.mass-operation:checked').each(function(i) {
                selected[i] = $(this).attr('value');
            });
            var route = $(this).attr('href');
            route = route+'?ids='+selected;
            window.location.href = route;
        });

        
        $('.list').sortable({
            handle: '.fa-arrows-alt',
            placeholder: "ui-state-highlight",
            update: function(event, ui) {
                var data = {
                    id : ui.item.find('.fa-arrows-alt').data("id"), 
                    orderAbove : ui.item.prev().find(".fa-arrows-alt").data("order") || ui.item.next().find(".fa-arrows-alt").data("order") - 1,
                    orderOrigin : ui.item.find(".fa-arrows-alt").data("order")
                };
                
                // console.log( data );
                // console.log( data.id );
                // console.log( data.orderAbove );
                // console.log( data.orderOrigin );
                // console.log(result);

                url = "/vault/{{$table}}/reorder/list";
                
                var token = $("input[name='_token'").val();
                
                // POST to server using $.post or $.ajax
                $.ajax({
                    data: data,
                    type: 'POST',
                    headers: {'X-CSRF-TOKEN': token},
                    url: url
                }).done(function(result) {
                    // return;
                    console.log(result);
                    window.location.reload();
                }).fail(function(error) {
                    console.log(error);
                });
            }
        });
    </script>
@endsection