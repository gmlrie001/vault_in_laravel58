@extends( 'layouts.setup_pages' )

@section( 'title', 'Vault' . " :: " . 'Database SETUP' )

@push( 'pageStyles' )
<style>
:root {
  --background-color-red: #ea202a;
  --background-color-black: #000;
  --background-color-grey: #565454;
}
body {
  max-width: 100%;
  max-height: 100vh;
  overflow: hidden;
}
a, p, span, q, blockquote {
  color: inherit;
  font-size:
}
main * {
  transition: all 0.618s !important;
  transition-timing-function: ease-in-out;
}
.page-container {
  display: grid;
  grid-template-areas: 
    "header_logo header_navigation" 
    "sidebar_navigation main_content"
    "footer_none footer_none";
  grid-template-columns: 15% calc( 100% - 15% );
  grid-template-rows: 100px calc( 100vh - 100px ) 0;
}
.site-logo {
  /* display: flex;
  align-self: center; */
  max-width: 100%;
  max-height: 55px;
  height: 55px;
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  margin: auto;
}
figure {
  margin-bottom: 0 !important;
}
header,
header.header {
  grid-area: header_logo;
  grid-column: 1;
  grid-row: 1;
  background-color: #ea202a;
}
nav {
  grid-area: header_navigation;
  grid-column: 2;
  grid-row: 1;
  background-color: grey;
  padding: calc( ( 10vh - 28px ) * 0.5 ) 150px;
}
nav ul {
  max-height: 55px;
  line-height: 55px;
}
nav ul li,
.navbar-nav .nav-link {
  color: #ffffff;
  line-height: inherit;
}
nav ul,
nav ul li a {
  font: normal normal 19px 'Open Sans', sans-serif;
  line-height: 55px;
  padding: 0 !important;
  color: inherit;
  font-weight: 500;
}
nav ul li {
    margin-left: 1.618rem;
    margin-right: 1.618rem;
}
main aside {
  grid-area: sidebar_navigation;
  grid-column: 1;
  grid-row: 2;
  padding: 25px 1rem;
}
main .vault-content {
  grid-area: main_contain;
  grid-column: 2;
  grid-row: 2;
  padding: 25px;
}
footer {
  grid-area: footer_none;
}

.card .card-headeer * {
  color: #ffffff;
  line-height: inherit;
}

.d-contents {
  display: contents;
}
.d-none {
  display: none;
}
[hidden], .hidden {
  display: none;
}

.m-0 {
  margin: 0 !important;
}
.mx-5 {
  margin-left: 3rem !important;
  margin-right: 3rem !important;
}
.my-5 {
  margin-top: 3rem !important;
  margin-bottom: 3rem !important;
}
.mx-auto {
  margin-left: auto !important;
  margin-right: auto !important;
}
.my-auto {
  margin-top: auto !important;
  margin-bottom: auto !important;
}

.p-0 {
  padding: 0 !important;
}
.px-5 {
  padding-left: 3rem !important;
  padding-right: 3rem !important;
}
.py-5 {
  padding-top: 3rem !important;
  padding-bottom: 3rem !important;
}

.no-list-style {
  list-style: none;
}

.bg-col-red {
  background-color: #ea202a;
}
.bg-col-grey {
  background-color: #181818 !important;
}
.bg-col-black {
  background-color: #000000;
}
.text-white {
  color: #ffffff;
}
.text-black {
  color: #000000;
}

.text-hide {
  font: 0/0 a;
  color: transparent;
  text-shadow: none;
  background-color: transparent;
  border: 0
}

.send-to-back {
  z-index: 1;
}
.on-the-level {
  z-index: 9;
}
.bring-to-front {
  z-index: 99;
}
@media only screen and (max-width: 576px) {
  .page-container {
    grid-template-areas:
    "header"
    "top_navigation"
    "sidebar_navigation"
    "main_contain";
  grid-template-columns: auto;
  grid-template-rows: auto auto auto auto;
  }
}
/**
  * Full-width and -height no scrolling page-container 
  * contains background image
  */
  .page-container {
    max-width: 100%;
    max-height: 100vh;
    overflow: hidden;
    background-image: url( '/web_components/layouts/vault_setup/assets/images/background-images/vault_setup_bg-img_init' );
    background-position: center center;
    background-size: cover;
  }
  button,
  .btn-success,
  .btn {
    max-height: 45px;
    margin: 1rem 0 0.309rem;
    padding: 0 1.618rem;
    width: 100%;
  }
  .cta-btn-submit {
    all: unset;
    max-width: 360px;
    width: 100%;
    margin: 2.5rem 0 0.5rem;
    padding: 0.5rem 0;
    font: normal normal 19px 'Open Sans', sans-serif;
    line-height: 30px;
    font-weight: 600;
  }
  .card .card-header h3 {
  font: normal normal 23px 'Open Sans', sans-serif;
  line-height: 50x;
  font-weight: 600;
  }
</style>
@endpush

@section( 'content' )
<header class="row header no-gutters align-content-center bg-col-grey">
  <div class="col-12 branding-logo w-100 h-100">
    <h1 class="title text-hide">Vault 4.0</h1>
    <figure class="w-100">
      <!-- <source srcset="/web_components/layouts/vault_setup/assets/images/logo/mobile_logo.png" media="max-width:767px"> -->
      <source srcset="/web_components/layouts/vault_setup/assets/images/logo/full_logo.png" media="min-width:768px">
      <img class="img-fluid site-logo p-md-1" src="/web_components/layouts/vault_setup/assets/images/logo/full_logo.png" alt="Vault Logo">
    </figure>
  </div>
</header>
<nav class="navbar-nav no-list-style bg-col-red ml-autow-100 w-100">
  <ul class="navbar no-list-style justify-content-end">
    <li class="nav-item">
      <a href="/dontexist" class="nav-link">The Vault</a>
    </li>
    <li class="nav-item">
      <a href="javascript:void(0)" class="nav-link" disabled>
        Docs <span class="bg-col-red text-white">Coming Soon</span>
      </a>
    </li>
    <li class="nav-item">
      <a href="/dontexist" class="nav-link">Setup</a>
    </li>
    <li class="nav-item">
      <a href="/vault/login" class="nav-link">Login</a>
    </li>
  </ul>
</nav>

<main class="d-contents" role="main">
  <aside class="bg-col-black">
    <section class="sidenav-listing">
      <h2 class="title text-uppercase m-0 p-0">Vault Setup</h2>
      <hr>
      <ul class="navbar-nav no-list-style">
        <li class="nav-item">
          <a title="Link 1" class="nav-link" target="_self" href="#">Link 1</a>
        </li>
        <li class="nav-item">
          <a title="Link 2" class="nav-link" target="_self" href="#">Link 2</a>
        </li>
      </ul>
    </section>
  </aside>
  <section class="container vault-content m-auto">
    <div class="row">
      <div class="col-md-6 offset-md-3">
        <span class="anchor" id="formRegister"></span>
        <hr class="mb-5">
        <!-- form card register -->
        <div class="card card-outline-secondary">
            <div class="card-header bg-col-grey text-white">
              <h3 class="mb-0">Vault Setup: <?php echo($_SERVER['HTTP_HOST']); ?> (localhost)</h3>
            </div>
            <div class="card-body">
                <form class="form" role="form" method="POST" action="/vault/setup/db_setup_test" autocomplete="off">

                    <div class="form-group">
                        <label for="hostname">Hostname (if accessing remote server DB)</label>
                        <input type="text" name="hostname" class="form-control" id="hostname" placeholder="localhost (127.0.0.1)" value="127.0.0.1" readonly>
                    </div>
                    
                    <div class="form-group">
                        <label for="dbname">Database Name</label>
                        <input type="email" name="dbname" class="form-control" id="dbname" placeholder="Database Name" required="" value="vault_lara58">
                    </div>
                    
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" name="username" class="form-control" id="username" placeholder="Username" required="" value="root">
                    </div>
                    
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" placeholder="Password" value="">
                    </div>

                    <div class="form-group">
                        <label for="confirm">Verify Password</label>
                        <input type="password_confirm" class="form-control" id="confirm" placeholder="Password (again)" value="">
                    </div>

                    <div class="d-none collapse hidden">{{ Honeypot::generate( 'my_time', 'my_name' ) }}</div>

                    <div class="form-group cta-btn-submit">
                        <button type="submit" class="btn btn-success btn-lg float-right text-uppercase">Setup DB</button>
                    </div>

                    <div class="d-none collapse hidden">@csrf</div>

                </form>
            </div>
        </div>
        <!-- /form card register -->
      </div>
    </div>
  </section>
</main>

<footer class="hidden" hidden></footer>
@endsection

