<?php return [
    'statuses' => [
        'PUBLISHED' => "PUBLISHED",
        'UNPUBLISHED' => "UNPUBLISHED",
        'DRAFT' => "DRAFT",
        'SCHEDULED' => "SCHEDULED",
    ]
];