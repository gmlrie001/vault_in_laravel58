<div class="col-xs-12 col-sm-5 selected-options">
    <div class="col-xs-12 col-sm-4 padding-0">
        <div class="dropup tooltipster" data-tooltipster='{"side":"top","animation":"fade"}' data-tooltip-content="#tooltip_content_selected">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                Export
            <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                <li><a href="/vault/create-selected-excel/trash/{{$table}}" class="get-selected-items" >Export Excel</a></li>
                <li><a href="/vault/create-selected-pdf/trash/{{$table}}" class="get-selected-items">Export PDF</a></li>
            </ul>
        </div>
        <div  class="tooltip_templates" style="display: none;">
            <div id="tooltip_content_selected">
                <p>Export the selected items to PDF or Excel</p>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-4 padding-0">
        <a href="/vault/{{$table}}/delete-selected/permanent" class="get-selected-items">Delete Selected</a>
    </div>
</div>