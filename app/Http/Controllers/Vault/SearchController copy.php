<?php namespace App\Http\Controllers\Vault;

/* Base Controller Include */
use App\Http\Controllers\Services\VaultController;

/* Facade Includes */
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use TomLingham\Searchy\Facades\Searchy;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

class SearchController extends VaultController
{
    protected $model = null;

    public function search(Request $request, $tableName)
    {
        if (Schema::hasTable($tableName)) {
            $tableNameSingular = studly_case(strtolower(str_singular($tableName)));

            $modelNamespace = str_replace(["{", "}"], ["", ""], "App\Models\{$tableNameSingular}");
            
            try {
                $this->model = (new $modelNamespace);
            } catch (Exception $e) {
                Session::flash('error', 'Model not found');
                Redirect::to('/vault')->send();
            }
        } else {
            Session::flash('error', 'Table does not exist');
            Redirect::to('/vault')->send();
        }

        $searchValue = Input::get('search');
    
        $columns = Schema::getColumnListing($tableName);

        if ($tableName == "baskets") {
            $search_results = $this->model::where(function ($query) use ($searchValue, $columns) {
                foreach ($columns as $key => $column) {
                    if ($key == 0) {
                        $query->whereRaw($column.' LIKE "%'.$searchValue.'%"');
                    } else {
                        $query->orWhereRaw($column.' LIKE "%'.$searchValue.'%"');
                    }
                }
            })->orWhereHas('user', function ($query) use ($searchValue) {
                $query->where('name', 'like', '%'.$searchValue.'%')
                ->orWhere('surname', 'like', '%'.$searchValue.'%')
                ->orWhere('email', 'like', '%'.$searchValue.'%')
                ->orWhere('mobile', 'like', '%'.$searchValue.'%');
            })->get();
        } else {
            $search_results = $this->model::hydrate(
                Searchy::search($tableName)->fields($columns)->query($searchValue)->get()->toArray()
            );
            $search_results = $this->paginate($search_results);
            $search_results = $search_results->setPath('/'.$request->path())->appends($request->all());
            // dd($request->path(), $search_results);
            Session::put('search_results', $search_results);
        }

        if (sizeof($search_results)) {
            return Redirect::back()->with('search_results', $search_results)->with('tableName', $tableName);
        } else {
            return Redirect::back()->with('error', 'No results found');
        }
    }

    public function paginate($items, $perPage = 15, $page = null, $options = [])
    {
        $page = $page ?: (\Illuminate\Pagination\Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof \Illuminate\Support\Collection ? $items : \Illuminate\Support\Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }


    public function searchTrash($tableName)
    {
        $searchValue = Input::get('search');
    
        $columns = Schema::getColumnListing($tableName);

        $search_results = Searchy::search($tableName)->fields($columns)->withTrashed()->query($searchValue)->having('relevance', '>', 1)->get();
    
        if (sizeof($search_results)) {
            return Redirect::back()->with('search_results', $search_results)->with('tableName', $tableName);
        } else {
            return Redirect::back()->with('error', 'No results found');
        }
    }
}
