<!-- Style Sheets -->
<!-- Standard Bootstrap Styles -->
<link href="{{ asset('/assets/js/vendor/bootstrap-3.3.7-dist/css/bootstrap.min.css') }}" rel="stylesheet" />

<!-- Vendor Style Sheets -->

<!-- Font Awesome 4.6.3 -->
<!-- <link rel="stylesheet" href="{{ asset( '/assets/css/vendor/font-awesome-4.6.3/css/font-awesome.min.css' ) }}"> -->
<!-- Font Awesome 5.8.1 -->
<!-- <link rel="stylesheet" href="{{ asset( 'assets/vendor/font-awesome5/css/fontawesome.min.css' ) }}"> -->

<!-- Font Awesome 5.8.1 SHIM via CDN or LOCAL -->
<!-- href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" -->
<link 
   rel="stylesheet" 
  type="text/css" 
  href="{{ asset( 'assets/vendor/font-awesome5/css/all.css' ) }}"
>
<!-- href="https://use.fontawesome.com/releases/v5.8.1/css/v4-shims.css" -->
<link
   rel="stylesheet" 
  type="text/css" 
  href="{{ asset( 'assets/vendor/font-awesome5/css/v4-shims.css' ) }}"
>

<link href="{{ asset('/assets/js/vendor/tooltipster/dist/css/tooltipster.bundle.min.css') }}" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="{{ asset('/assets/js/vendor/slick-1.5.7/slick/slick.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('/assets/js/vendor/slick-1.5.7/slick/slick-theme.css') }}"/>
<link href="{{ asset('/assets/js/vendor/paulkinzett-toolbar-5112fbf/jquery.toolbar.css') }}" rel="stylesheet" />
<link href="{{ asset('/assets/js/vendor/Parsley.js-2.1.2/src/parsley.css') }}" rel="stylesheet" />
<link href="{{ asset('/assets/js/vendor/croppic-master/assets/css/croppic.css') }}" rel="stylesheet" />
<link href="{{ asset('/assets/js/vendor/jquery-ui-1.12.0.custom/jquery-ui.theme.min.css') }}" rel="stylesheet" />
<link href="{{ asset('/assets/js/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" />
<link href="{{ asset('/assets/js/vendor/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.min.css') }}" rel="stylesheet" />
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/js/vendor/multiselect-master/css/style.css') }}" />
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/js/vendor/fontawesome-iconpicker-1.0.0/dist/css/fontawesome-iconpicker.min.css') }}" />
<!-- <link type="text/css" rel="stylesheet" href="{{ asset('/assets/js/vendor/fontawesome-iconpicker-3.0.0/dist/css/fontawesome-iconpicker.min.css') }}" /> -->
<link href="{{ asset('/assets/js/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" />
<link href="{{ asset('/assets/js/vendor/selectize.js-master/dist/css/selectize.css') }}" rel="stylesheet" />
<link href="{{ asset('/assets/js/vendor/selectize.js-master/dist/css/selectize.bootstrap3.css') }}" rel="stylesheet" />
<link href="{{ asset('/assets/js/vendor/icheck-1.x/skins/flat/red.css') }}" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link type="text/css" rel="stylesheet" href="{{ asset('/assets/js/vendor/bootstrap-colorpickersliders-master/src/bootstrap.colorpickersliders.css') }}" />