@extends( 'layouts.vault' )

@section( 'content' )

@php $categories = ['display','serif','sans-serif','monospace','handwriting']; @endphp

<section id="google-font-selector" class="google-fonts">

  <div class="container-fluid my-lg-3 my-2"><!-- container-fluid -->
    <div class="row">
      <div class="col-12 col-lg page-title">
        <h1>Google Fonts Selector</h1>
      </div>
      <div class="col-12 col-lg-4 d-flex flex-lg-row flex-column ml-auto">
        <h2>
          <small>Update Google Fonts</small>
        </h2>
        <a title="Update Google Fonts" class="cta btn px-lg-5 mx-lg-4 mx-3" href="/parse_google_fonts_file/update">Update</a>
      </div>
    </div>
  </div>

  <div class="container-fluid my-lg-3 my-0"><!-- container-fluid -->
    <section class="custom-container my-lg-3 my-2">
      <div class="row">
        <div class="col-12 col-lg-6 mb-lg-2 my-3">
          <form action="/parse_google_fonts_file/search" method="POST">
            @csrf()
            <div class="d-block col-12 px-lg-0">
              <h4>Search:</h4>
            </div>
            <label for="font-search" class="input-group-append">
              <input name="query" id="font-search" type="search" class="form-control" value="" placeholder="Search font-family ..." style="border-radius:0;"><!-- onkeyup="live_search()"-->
              <button type="submit" style="all: unset;box-shadow: unset !important;">
                <i class="fa fa-search" style="line-height:48px;width:48px;background-color:#495057;color:#fefefe;border:1px solid #ced4da;text-align:center;box-shadow:0 0 3px 1px rgba(73,80,87,0.1);"></i>
              </button>
            </label>
          </form>
        </div>
        <div class="col-12 col-lg-6 my-3 text-right">
          <div class="d-block col-12 px-lg-0">
            <h4>Filter/Ordering:</h4>
          </div>
          <select id="ordering" class="form-control ordering-filter" data-base-href="parse_google_fonts_file/orderBy">
            <option value="NULL">Select Ordering...</option>
            <option value="family/asc">Font Family Name ASCending</option>
            <option value="family/desc">Font Family Name DESCending</option>
            <option value="lastModified/asc">Last Modified ASCending</option>
            <option value="lastModified/desc">Last Modified DESCending</option>
            <option value="version/asc">Version ASCending</option>
            <option value="version/desc">Version DESCending</option>
          </select>
        </div>
      </div>
    </section>
  </div>

  <div class="col d-block d-lg-none">
    <button class="mobile-modal-activate">Show Filters</button>
  </div>

  <section class="vault-google-fonts-grid">
    <div class="filters-container my-lg-3 my-0 d-none d-lg-block"><!-- container-fluid -->
      <div class="row mx-lg-3">
        <div class="col-12 mt-lg-0 my-3 px-lg-0 px-3">
          <form action="/parse_google_fonts_file/filter" method="POST">
            @csrf()
            <div class="d-block col-12 px-lg-0 px-3">
              <h4>Category:</h4>
            </div>
            <label for="font-category" class="w-100">
              <select name="category" id="font-category" class="form-control" style="border-radius:0;"><!-- onchange="selected()"-->
                <option value="NULL">Filter by Category...</option>
                @forelse($categories as $key=>$category)
                <option value="{{ $category }}">{{ ucwords( $category ) }}</option>
                @empty
                @endforelse
              </select>
            </label>
            <div class="form-group my-lg-3 my-2">
              <div class="d-block col-12 px-lg-0 px-3">
                <h4>Variants:</h4>
              </div>
              {{--
              @forelse($variants as $key=>$variant)
              <label class="col-auto d-flex flex-row mx-0 mb-lg-1 px-lg-0 px-3 hidden">
                <input name="variants[]" type="checkbox" class="form-control mr-lg-3 mr-2 w-auto hidden" value="{{ $variant }}">
                <span hidden>{{ ucwords( $variant ) }}</span>
              </label>
              @empty
              @endforelse
              --}}
              <select name="variants[]" id="font-variant" class="form-control" style="border-radius:0;" multiple>
              <!-- onchange="selected()"-->
                @forelse($variants as $key=>$variant)
                <option value="{{ $variant }}">{{ ucwords( $variant ) }}</option>
                @empty
                @endforelse
              </select>
            </div>
            <div class="form-group my-lg-3 my-2">
              <div class="d-block col-12 px-lg-0 px-3">
                <h4>Subsets:</h4>
              </div>
              {{--
              @forelse($subsets as $key=>$subset)
              <label class="col-auto d-flex flex-row mx-0 mb-lg-1 px-lg-0 px-3 hidden">
                <input name="subsets[]" type="checkbox" class="form-control mr-lg-3 mr-2 w-auto hidden" value="{{ $subset }}">
                <span hidden>{{ ucwords( $subset ) }}</span>
              </label>
              @empty
              @endforelse
              --}}
              <select name="subsets[]" id="font-subset" class="form-control" style="border-radius:0;" multiple>
              <!-- onchange="selected()"-->
                @forelse($subsets as $key=>$subset)
                <option value="{{ $subset }}">{{ ucwords( $subset ) }}</option>
                @empty
                @endforelse
              </select>
            </div>
            <div class="row justify-content-around">
              <button class="col-11 col-lg-11" type="submit">Filter</button>
              <button class="col-5 col-lg-5" type="reset">Reset</button>
              <button class="col-5 col-lg-5" type="button" onclick="window.location.href = '/parse_google_fonts_file';">Clear All</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <div class="fonts-container my-lg-3 my-4"><!-- container-fluid -->
      <section class="row cardcols my-lg-3 my-2 mx-lg-3 mx-auto" id="myUL">
        @forelse( $font_db as $key=>$font )
        <article class="col-lg-4 col-12 google_fonts card pb-lg-0 @if ( $key > 11 ) hidden @endif" data-font-family="{{ $font->family }}" data-category="{{ $font->category }}">
          @if( $key < 12 )
          @php $loaded[] = [ $key => $font->family ]; @endphp
          <script>
            (function (d=document, t='link') {
              var e = document.querySelector( '.google_fonts:nth-of-type( {{ $key+1 }} )' );
              var b = e.getBoundingClientRect();
              if( b.top > 0 && b.height > 0 ) {
                var gfonts_url_base = 'http://fonts.googleapis.com/css?family',
                    gfonts_font_sel = "<?php echo(str_replace(' ', '+', $font->family)); ?>",
                    gfonts_font_var = <?php echo('["' . implode('", "', $font->variants) . '"]'); ?>;

                var url_build, g, s;
                    url_build = gfonts_url_base + '=' + gfonts_font_sel + ':' + gfonts_font_var.join(',');

                try {
                  g = d.createElement(t);
                  g.rel = "stylesheet";
                  g.href = url_build + '&display=swap';
                  s = d.getElementsByTagName(t)[0];
                  s.parentNode.insertBefore(g, s);
                } catch( err ) {
                  console.clear();
                  console.warn( '\r\n' + err + '\r\n' );
                }
              }
            })(document, 'link');
          </script>
          @endif
          <section class="card-body pb-lg-0 px-3 px-lg-4" style="font-family:{{ $font->family }},{{ $font->category }} !important;font-display:swap;">
            <div class="card-title mb-lg-0 d-flex flex-lg-column flex-row flex-wrap justify-content-between ellipsis">
              <div class="row">
                <div class="col-12 col-lg font-title">
                  <h3 class="card-title mb-0" style="font-family:{{ $font->family }},{{ $font->category }} !important;font-display:swap;">
                    {{ ucwords( $font->family ) }} 
                  </h3>
                </div>
                @isset($font->version)
                <div class="col-12 col-lg-auto font-version">
                  <div class="text-center versioned" style="width:25px;height:25px;padding:0;border:1px solid #000;border-radius:50%;line-height:25px;">
                    <span style="font-family:fantasy!important;font-weight:300!important;font-size:11px!important;">{{ $font->version }}</span>
                  </div>
                </div>
                @endisset
                @isset($font->subsets)
                <div class="col-12 col-lg-auto font-subsets">
                  <span class="text-right-lg">
                    <abbr class="d-none d-lg-block" title="{{ ucwords( implode( ', ', $font->subsets ) ) }}" style="font-family:fantasy!important;font-weight:300!important;letter-spacing:calc(1em*(10/1000));">Subsets</abbr>
                    <span class="d-block d-lg-none" style="font-family:fantasy!important;font-weight:300!important;letter-spacing:calc(1em*(40/1000));">{{ ucwords( implode( ', ', $font->subsets ) ) }}</span>
                  </span>
                </div>
                @endisset
              </div>
            </div>
            @if( null != $font->variants && count( $font->variants ) > 0 )
            <select class="order-last mb-lg-3 mb-4" onchange="font_variant( this, this.value );">
              <option value="NULL">Select Font Variant...</option>
              @foreach( $font->variants as $key=>$variant )
              <option value="{{ $variant }}">{{ ucwords( $variant ) }}</option>
              @endforeach
            </select>
            @endif
            <section class="card-text font-text-example" style="margin-bottom:1rem;">
              <h1 class="ellipsis" style="font-family:{{ $font->family }},{{ $font->category }} !important;font-display:swap;">This is an example title</h1>
              <p class="ellipsis" style="font-family:{{ $font->family }},{{ $font->category }} !important;font-display:swap;">Example body text...</p>
              <a class="cta btn" style="font-family:{{ $font->family }},{{ $font->category }} !important;font-display:swap;">Example CTA</a>
            </section>
            <div class="row">
              <div class="col-auto mx-lg-3">
                <label for="title_font" class="d-flex">
                  <input id="title_font" class="form-control col-lg-auto col" type="radio" name="title_font" value="{{ str_replace( ' ', '+', $font->family ) }}">
                  <span style="line-height:50px;">Title</span>
                </label>
              </div>
              <div class="col-auto mx-lg-3">
                <label for="body_font" class="d-flex">
                  <input id="body_font" class="form-control col-lg-auto col" type="radio" name="body_font" value="{{ str_replace( ' ', '+', $font->family ) }}">
                  <span style="line-height:50px;">Body</span>
                </label>
              </div>
              <div class="col-auto mx-lg-3">
                <label for="cta_font" class="d-flex">
                  <input id="cta_font" class="form-control col-lg-auto col" type="radio" name="cta_font" value="{{ str_replace( ' ', '+', $font->family ) }}">
                  <span style="line-height:50px;">CTA</span>
                </label>
              </div>
            </div>
          </section>
        </article>
        @empty
        <div class="px-5">
          <h2>No results found!</h2>
          <a class="clear-filters" href="/parse_google_fonts_file">
            <span class="mr-3">&times;</span> Clear All Filters
          </a>
        </div>
        @endforelse
        @if ( null != $font_db->links() )
        <div class="container-fluid my-lg-3 my-5" style="overflow:hidden;">
          <div class="row">
            <?php echo($font_db->links()); ?>
          </div>
        </div>
        @endif
      </section>

    </div>
  </section>

  <section id="filter-modal" class="collapse d-lg-none">
    <div class="position-absolute" style="top:50px;right:2rem;z-index:1000000;">
      <span class="close-modal" style="font-size:3.5rem;font-weight:700;line-height:1;">&times;</span>
    </div>
  </section>

</section>
@endsection

