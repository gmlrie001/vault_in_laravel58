<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserGroup extends Model
{
    use SoftDeletes;
    
    public $orderable = true;
    public $orderField = "order";
    public $titleField = "title";
    public $statusField = "status";
    public $hasStatus = true;
    public $orderDirection = "asc";
    public $parentOrder = "";
    public $parentTable = "";
    public $orderOptions = ['title', 'discount_type', 'discount', 'activation_type'];
    public $relationships = [];
    public $mainDropdownField = "discount";
    public $imageDropdownField = "";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'discount_type',
        'discount',
        'activation_type'
    ];

    public $fields = [
    //  ['field_name', 'label', 'field_type', 'options_model', 'options_relationship', 'width', 'height', 'container_class', 'can_remove'],
        ['title', 'Title', 'text', '', '', '', '', 'col-xs-12 col-md-6', ''],
        ['open_parent', 'Discount Details', ''],
            ['discount', 'Discount', 'text', '', '', '', '', 'col-xs-12 col-md-6', ''],
            ['discount_type', 'Discount Type', 'select_from_array', ['0' => 'Percentage', '1' => 'Fixed Amount'], '', '', '', 'col-xs-12 col-md-6', ''],
        ['close_parent', 'Discount Details', ''],
        ['open_parent', 'Activation Details', ''],
            ['activation_type', 'Activation Type', 'select_from_array', ['1' => 'User Self Activation', '0' => 'Admin Activation'], '', '', '', 'col-xs-12 col-md-6', ''],
        ['close_parent', 'Activation Details', ''],
        ['open_row', 'Status'],
            ['status', 'Status', 'status', '', '', '', '', 'col-xs-12 col-md-6', ''],
        ['close_row', 'Status'],
    ];

    /**
     * Get the users for this user group.
     */
    public function users()
    {
        return $this->hasMany(User::class, 'user_group_id');
    }
}
