<div id="import" class="modal fade bs-example-modal-lg" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="myLargeModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			{!!Form::open(array('url' => '/vault/import/'.$table.'/', 'method' => 'post', 'enctype' => 'multipart/form-data'))!!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Import</h4>
				<label class="btn btn-default btn-file pull-right">
				    CHOOSE A FILE {!! Form::file('import_file', array('placeholder' => 'Import', 'class' => 'form-control import-file', 'style' => 'display:none')) !!}
				</label>
				
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12">
							<p class="message">Please Choose a file to uploadby pressing the button on the top right</p>
	                   		{!! Form::submit('SUBMIT', array('class' => 'btn btn-default pull-right')) !!}
	                    {!!Form::close()!!}
                    </div>
                </div>
			</div>				  
		</div>
	</div>
</div>