<?php

/*
|--------------------------------------------------------------------------
| Vault Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Dashboard */
Route::get('vault', 'DashboardController@index');

Route::get('vault/aws', 'AWSController@index');
Route::get('vault/laybyes/send', 'LaybyesController@sendReminders');
Route::get('vault/abandoned', 'AbandonedCartController@sendCodes');
Route::get('vault/notification/send', 'NotificationController@send');

/* Image Manipulation Routes */
Route::any('vault/image/crop/{dir}', 'ImageManipulationController@imageCrop');
Route::any('vault/temp/image/upload', 'ImageManipulationController@tempImageUpload');
Route::post('vault/image/upload/{dir}', 'ImageManipulationController@imageUpload');
Route::get('vault/image/remove/{table}/{field}/{id}', 'ImageManipulationController@removeImage');

/* Change show limit Routes */
Route::get('vault/limit/set/{count}', 'LimitController@limitSet');

/* Change display order field Routes */
Route::get('vault/order/set/{direction}/{field}', 'OrderFieldController@orderSet');

/* Status Routes */
Route::get('vault/{tableName}/status/change/{id}', 'StatusController@change');
Route::get('vault/{tableName}/change-selected-status', 'StatusController@changeSelectedStatus');

/* Export Routes */
Route::get('vault/{tableName}/export/pdf', 'ExportController@createPdf');
Route::get('vault/{tableName}/export/excel', 'ExportController@createExcel');
Route::get('vault/create-selected-pdf/{tableName}', 'ExportController@createSelectedPdf');
Route::get('vault/create-selected-excel/{tableName}', 'ExportController@createSelectedExcel');
Route::get('vault/{tableName}/export/trash/pdf', 'ExportController@createTrashPdf');
Route::get('vault/{tableName}/export/trash/excel', 'ExportController@createTrashExcel');
Route::get('vault/create-selected-pdf/trash/{tableName}', 'ExportController@createTrashSelectedPdf');
Route::get('vault/create-selected-excel/trash/{tableName}', 'ExportController@createTrashSelectedExcel');

/* Search Routes */
Route::any('vault/{tablename}/search', 'SearchController@search');
Route::any('vault/{tablename}/search/trash', 'SearchController@searchTrash');

/* Move Entry Up Routes */
Route::get('vault/{tableName}/up/{id}', 'OrderController@moveUp');

/* Move Entry Down Routes */
Route::get('vault/{tableName}/down/{id}', 'OrderController@moveDown');

/*Bulk Reorder Routes */
Route::post('vault/{tableName}/bulk/order', 'OrderController@bulkOrder');

/* Drag reorder Routes */
Route::post('vault/{tableName}/reorder/list', 'OrderController@reorderList');

/* Restore Order Routes */
Route::get('vault/{tableName}/order/restore', 'OrderController@restoreOrder');

/* Feed */
Route::get('vault/feed/products', 'FeedController@listing');
Route::get('vault/feed/get/options', 'FeedController@getGroupOptions');
Route::get('vault/shop/set/variants', 'FeedController@saveVariants');
Route::post('vault/shop/set/components', 'FeedController@saveComponent');
Route::post('vault/shop/delete/components', 'FeedController@deleteComponents');
Route::post('vault/shop/delete/variants', 'FeedController@deleteVariants');

/* Basket Routes */
Route::get('vault/orders', 'OrdersController@listing');
Route::get('vault/abandons', 'AbandonedCartController@listing');
Route::get('vault/quotes', 'QuotesController@listing');
Route::get('vault/laybyes', 'LaybyesController@listing');

/* CRUD */
/* Listing routes */
Route::get('vault/{tableName}', 'CRUDController@listing');
Route::get('vault/{tableName}/trash', 'TrashController@listing');
Route::get('vault/{tableName}/relation/{relationId}', 'CRUDController@listing');
/* Add routes */
Route::get('vault/{tableName}/add', 'CRUDController@add');
Route::post('vault/{tableName}/add', 'CRUDController@store');
/* Edit routes */
Route::get('vault/{tableName}/edit/{id}', 'CRUDController@edit');
Route::post('vault/{tableName}/edit/{id}', 'CRUDController@update');
/* Duplicate Routes */
Route::get('vault/{tableName}/duplicate/{id}', 'CRUDController@duplicate');
Route::post('vault/{tableName}/duplicate/{id}', 'CRUDController@store');
/* Delete Routes */
Route::get('vault/{tableName}/delete/{id}', 'CRUDController@delete');
Route::any('vault/{tableName}/delete-selected', 'CRUDController@deleteSelected');
Route::get('vault/{tableName}/delete/{id}/permanent', 'CRUDController@deletePermanent');
Route::get('vault/{tableName}/delete-selected/permanent', 'CRUDController@deletePermanentSelected');
/* Restore Routes */
Route::get('vault/{tableName}/restore/{id}', 'CRUDController@restore');

/* Report Routes */
Route::get('vault/report/page_views', 'ReportController@pageViews');
Route::post('vault/report/page_views', 'ReportController@filterPageViews');
