<?php namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Support\Facades\Schema;

class TableSectionExport implements FromQuery
{
    use Exportable;
    protected $model = null;
    protected $ids;

    function __construct($tableName, $ids) {
        if(Schema::hasTable($tableName)) {
            $tableNameSingular = studly_case(strtolower(str_singular($tableName)));

            $modelNamespace = str_replace(["{", "}"], ["", ""], "App\Models\{$tableNameSingular}");

			try  {
                $this->model = (new $modelNamespace);
            } catch (Exception $e){
                Session::flash('error', 'Model not found');
                Redirect::to('/vault')->send();
            }
            $this->ids = $ids;
        }else{
            Session::flash('error', 'Table does not exist');
            Redirect::to('/vault')->send();
        }
    }
    
    public function query()
    {
        return $this->model::query()->whereIn('id', $this->ids);
    }
}

