{!!Form::open(array('url' => '/vault/'.$table.'/search/trash', 'method' => 'post', 'class' => 'function-search tooltipster', 'data-tooltipster' => '{"side":"top","animation":"fade"}', 'data-tooltip-content' => '#tooltip_content_search'))!!}
    <button type="submit">
        <img src="/assets/images/vault/template/functions/search-icon.svg" />
    </button>
    {!! Form::text('search', null, array('placeholder' => 'Search...')) !!}
{!!Form::close()!!} 