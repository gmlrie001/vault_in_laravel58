<div class="col-xs-12 padding-0 tab-container hidden-xs">
  @if( Session::has('relation_id') && Session::get('relation_id') > 0 )
    <a href="/vault/{{$table}}/relation/{{ Session::get( 'relation_id' ) }}">
        {{ mb_convert_case( str_replace( '_', ' ', strtolower( $table ) ), MB_CASE_TITLE, "UTF-8" ) }}
    </a>
  @else
    <a href="/vault/{{$table}}">
        {{ mb_convert_case( str_replace( '_', ' ', strtolower( $table ) ), MB_CASE_TITLE, "UTF-8" ) }}
    </a>
  @endif
    <a href="#" class="active">Trash</a>
</div>
<div class="col-xs-12 padding-0 mobile-tab-container hidden-sm hidden-md hidden-lg">
    <div>
        TRASH <i class="fa fa-chevron-down"></i>
        <div>  
            <a href="/vault/{{$table}}">
                {{ mb_convert_case( str_replace( '_', ' ', strtolower( $table ) ), MB_CASE_TITLE, "UTF-8" ) }}
            </a>
            <a href="#" class="active">
                Trash
            </a>
        </div>
    </div>
</div>