<!DOCTYPE html>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->


<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="shortcut icon" href="/assets/images/vault/logo/favicon.ico" />

		<!-- SEO ? -->
		<title>Vault - Monza Media</title>

    <meta name="description" content="Locked" />
    <meta name="keywords" content="Locked" />
    <meta name="author" content="Monza Media <http://www.monzamedia.com>" />
    <meta name="robots" content="noindex, nofollow">
	
		@include('includes.vault.styleincludes')

		<!-- Front-End Styles -->
		<link href="{{ asset('/assets/css/vault/fonts.css') }}" rel="stylesheet" />
		<link href="{{ asset('/assets/css/vault/styles.css') }}" rel="stylesheet" />
		<link href="{{ asset('/assets/css/vault/vendor.css') }}" rel="stylesheet" />
		<link href="{{ asset('/assets/css/vault/header_sidebar.css') }}" rel="stylesheet" />
		<link href="{{ asset('/assets/css/vault/dashboard.css') }}" rel="stylesheet" />
		<link href="{{ asset('/assets/css/vault/tabs.css') }}" rel="stylesheet" />
		<link href="{{ asset('/assets/css/vault/forms.css') }}" rel="stylesheet" />
		<link href="{{ asset('/assets/css/vault/functions.css') }}" rel="stylesheet" />
		<link href="{{ asset('/assets/css/vault/listing.css') }}" rel="stylesheet" />
		<link href="{{ asset('/assets/css/vault/mass_operation.css') }}" rel="stylesheet" />
		<link href="{{ asset('/assets/css/vault/pagination.css') }}" rel="stylesheet" />
		
		<!--Scripts-->
		<!--Vendor Scripts-->
		@include('includes.vault.scriptIncludes')
		
		<style id="notifier">
			[hidden], .hidden {
				display: none !important;
			}
			.slick-track,
			a.col-xs-12.padding-0.slick-slide.slick-current.slick-active {
				width: 100% !important;;
			}
			.header-alert-circle.notifications-count {
				padding: 5px 10px;
				margin-bottom: 15px;
				border-radius: 50%;
				background-color: red;
				vertical-align: super;
				font-size: 11px;
				font-weight: 700;
				color: #ffffff;
				text-align: center;
			}
		</style>

    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/css/vault/modules/google-font-selector.css') }}" media="all">
		<link rel="stylesheet" type="text/css" href="{{ asset('/assets/css/vault/modules/bs4_classes.css') }}" media="all">

		@stack( 'vaultPageStyles' )

	</head>
	
	@php ob_flush(); @endphp

	<body>

		<div class="container-fluid header">
			@include('includes.vault.header')
		</div>
	
		<div class="vault-content">

			<div class="side-nav">
				@include('includes.vault.nav')
			</div>

			@yield('content')
		</div>
	
		@include('includes.vault.messages')
	
		<!--Front-End Scripts-->
		<script src="{{ asset('/assets/js/vault/scripts.js') }}" type="text/javascript"></script>
		<script src="{{ asset('/assets/js/vault/modules/google-fonts-selector.js') }}" type="text/javascript"></script>
	</body>
</html>