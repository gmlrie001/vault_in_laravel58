<?php
/**
 * Variability in the Page Banner image dimensions. Needs to be update when adding
 * new pages refernce by page_id on pages table.
 */

/* Setup defaulting values for the project just in case... */
/* The page_id=1 is the Site Settings, which does not produce a page banner */
/* All this non-sense for 1 page banner dimensions... */

return [
  'desktop' => [
    'default' => [
      'width'  => 1920,
      'height' => 250
    ],
    1  => [
      'width'  => 1920,
      'height' => 650
    ],
    2  => [
      'width'  => 1920,
      'height' => 250
    ],
    3  => [
      'width'  => 1920,
      'height' => 250
    ],
    4  => [
      'width'  => 1920,
      'height' => 250
    ],
    5  => [
      'width'  => 1920,
      'height' => 250
    ],
    6  => [
      'width'  => 1920,
      'height' => 250
    ],
    7  => [
      'width'  => 1920,
      'height' => 250
    ],
  ],
];
