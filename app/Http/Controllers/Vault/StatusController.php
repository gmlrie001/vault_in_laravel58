<?php namespace App\Http\Controllers\Vault;

/* Base Controller Include */
use App\Http\Controllers\Services\VaultController;

/* Facade Includes */
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;

class StatusController extends VaultController
{
    protected $model = null;

    public function __construct()
    {
        parent::__construct();
        $tableName = Route::input('tableName');

        if (Schema::hasTable($tableName)) {
            $tableNameSingular = studly_case(strtolower(str_singular($tableName)));

            $modelNamespace = str_replace(["{", "}"], ["", ""], "App\Models\{$tableNameSingular}");
            
            try {
                $this->model = (new $modelNamespace);
            } catch (Exception $e) {
                Session::flash('error', 'Model not found');
                Redirect::to('/vault')->send();
            }
        } else {
            Session::flash('error', 'Table does not exist');
            Redirect::to('/vault')->send();
        }
    }

    public function change($tableName, $id)
    {
        $entry = $this->model::find($id);

        $statuses = Config::get('enums', 'statuses');
        $statuses = array_values($statuses['statuses']);

        $key = array_search($entry->{$this->model->statusField}, $statuses);

        $lastItem = sizeof($statuses)-1;
        
        if ($key+1 > $lastItem) {
            $key = 0;
        } else {
            $key = $key + 1;
        }

        $entry->status = $statuses[$key];

        $entry->save();
        
        return Redirect::back()->with('message', 'Successfully changed statuses');
    }
    
    public function changeSelectedStatus($tableName)
    {
        $selectedItems = explode(',', Input::get('ids'));
        
        $results = $this->model::whereIn('id', $selectedItems)->get();

        $statuses = Config::get('enums', 'statuses');
        $statuses = array_values($statuses['statuses']);
        
        $lastItem = sizeof($statuses)-1;

        foreach ($results as $key => $result) {
            $key = array_search($result->{$this->model->statusField}, $statuses);
            
            if ($key+1 > $lastItem) {
                $key = 0;
            } else {
                $key = $key + 1;
            }
    
            $result->status = $statuses[$key];
    
            $result->save();
        }
        
        return Redirect::back()->with('message', 'Successfully changed statuses');
    }
}
