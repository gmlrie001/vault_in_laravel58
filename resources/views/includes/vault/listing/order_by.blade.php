<div class="pull-left function-button">
    <span class="tooltipster" data-tooltipster='{"side":"top","animation":"fade"}' data-tooltip-content="#tooltip_content_order">
        <img src="/assets/images/vault/template/functions/order-icon.svg" />
        <span>Order By</span>
        <img src="/assets/images/vault/template/functions/dropdown-icon.svg" />
    </span>
    <div class="function-dropdown">
        @foreach($orderOptions as $orderOption)
            <a href="/vault/order/set/asc/{{$orderOption}}">{{title_case(str_replace('_',  ' ', str_singular($orderOption)))}} <img src="/assets/images/vault/template/functions/order-down.svg" /></a>
            <a href="/vault/order/set/desc/{{$orderOption}}">{{title_case(str_replace('_',  ' ', str_singular($orderOption)))}} <img src="/assets/images/vault/template/functions/order-up.svg" /></a>
        @endforeach
    </div>
</div>