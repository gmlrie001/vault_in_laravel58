<div class="share-overlay"></div>
<div class="share-modal">
    <a class="close-share" href="#">
        <img class="img-responsive" src="/assets/images/template/share-close.png" />
    </a>
    <h1>Share Grand Domaine</h1>
    <div class="col-12 float-left text-center">
    @foreach( $item_sharing as $key=>$share )
        <a title="Share to {{ $key }}" class="@if( $key === 'email' ) fas fa-envelope @else fab {{ $share[1] }} @endif" rel="noopener noreferrer" href="{{ $share[0] }}" target="_blank"></a>
    @endforeach
    </div>
    <h1>Follow Grand Domaine</h1>
    <div class="col-12 float-left text-center">
    @foreach( $socials as $key=>$social )
        <a rel="noopener noreferrer" target="_blank" href=" {{ $social->social_media_url }}" class="fab {{ $social->social_media_icon }}" title="Follow/Like Us On {{ $social->title }}"></a>
    @endforeach
    </div>
</div>