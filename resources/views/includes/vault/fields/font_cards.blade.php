@extends( 'layouts.page' )

@push('pageStyles')
<style>
  html, body {
    font-size: 100%;
    max-width: 100%;
    overflow-x: hidden;
  }
  h1, .h1 {
    font-size: 1.618rem;
  }
  select, input {
    font-size: 0.85rem;
  }
  input[type=radio] {
    transform: scale(.675);
  }
  .form-control:focus {
    border-color: #ced4da;
    background-color: unset;
    box-shadow: none;
    color: unset;
    outline: 0;
  }
  .google_fonts.card {
    max-width: calc( 100% / 3 - 10px );
    margin: 5px;
    max-height: 300px;
    min-height: 300px;
    overflow: auto;
  }
  .cta.btn {
    background-color: hsl(180, 50%, 50%);
    box-shadow: 0 1px 2px 0 rgba(10, 10, 10, 0.3),
      0 2px 4px 1px rgba(10, 10, 10, .15);
    border:1px solid #000;
    border-radius:3rem;
    padding:0.618rem 3rem;
  }
  .show {
    display: block;
  }
  .ellipsis {
    width: 100%;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }
  select.order-last {
    font-family: sans-serif !important;
    padding: 0 15px;
    line-height: 40px;
    height: 40px;
    margin-bottom: 2rem;
    box-shadow: 1px 2px 3px 0px rgba(10,10,10,0.25);
  }
  .custom-container {
    width: 100%;
    margin-left: auto;
    margin-right: auto;
    padding-left: 15px !important;
    padding-right: 15px !important;
  }
  .custom-container {
    max-width: 1600px;
  }
  .vault-google-fonts-grid {
    max-width: 100%;
    display: grid;
    grid-template-columns: minmax(auto, 15%) minmax(auto, 85%);
    grid-template-rows: auto;
    grid-column-gap: 0;
  }
  /* .filters-container {
    padding-left: 5px !important;
    padding-right: 5px !important;
  }
  .fonts-container {
    padding-left: 5px !important;
    padding-right: 5px !important;
  }
  [class*=-container] {
    width: 100%;
  } */
  button[type=submit],
  button[type=reset],
  button[type=button],
  a.btn.btn-skip {
    height: 50px;
    margin-bottom: 15px;
    margin-top: 15px;
    padding: 0 1rem;
    border-radius: 0.375rem;
    border: none !important;
    box-shadow: -2px 2px 3px 0 rgba(10,10,10,1) !important;
    background-color: #b71c1c;
    font-size: 16px;
    font-weight: 600;
    color: #ffffff;
    line-height: 50px;
    letter-spacing: calc( 1em * ( 20 / 1000 ) );
  }
  input[type=checkbox] {
    line-height: 0;
    height: 25px;
  }
  input[type=checkbox]~span {
    line-height: 1.618;
  }
  select[multiple] {
    min-height: 415px;
    max-height: 415px;
    overflow: auto;
  }
  a.cta.btn {
    max-width: 300px;
    width: 100%;
  }
  @media only screen and (max-width:1760px) {
    .custom-container {
      max-width: 95%;
    }
  }
  @media only screen and (max-width:1440px) {
    .custom-container {
      max-width: 92.5%;
    }
  }
  @media only screen and (max-width:1200px) {
    .custom-container {
      max-width: 87.5%;
  }
  @media only screen and (max-width:1024px) {
    .custom-container {
      max-width: 960px;
    }
  }
  @media only screen and (max-width:992px) {
    header nav,
    nav {
      display: block !important;
    }
    section#myUL {
      max-width: 100%;
    }
    .custom-container {
      max-width: 100%;
    }
    .vault-google-fonts-grid {
      display: block;
      max-width: 100%;
      /* grid-template-columns: 1fr;
      grid-template-rows: auto;
      grid-column-gap: 1rem; */
    }
    .google_fonts.card {
      max-width: 100%;
      max-height: unset;
      min-height: unset;
      padding-left: 0;
      padding-right: 0;
    }
    .filters-container {
      padding-left: 15px !important;
      padding-right: 15px !important;
    }
    .fonts-container {
      padding-left: 15px !important;
      padding-right: 15px !important;
    }
    select[multiple] {
      min-height: unset;
      max-height: unset;
    }
    .center-in-modal {
      position: absolute;
      top: 50px;
      left: 0;
      bottom: 0;
      right: 0;
      max-height: 90vmax;
      max-width: 90%;
      width: 100%;
      height: 100vh;
      background-color: #ffffff;
      margin: auto;
      padding-top: 50px;
    }
    .card-title.d-flex {
      min-height: 120px;
      max-height: 125px;
    }
  }
  .versioned {
    max-width:30px;
    max-height:30px;
    padding:0;
    border:1px solid #000;
    border-radius:50%;
    line-height:30px;
  }
  .font-version sup {
    font-family:fantasy;
    font-size:11px;
    font-weight:300;
    text-decoration:underline;
    vertical-align:initial;
  }
  .font-subsets abbr {
    line-height:1;
  }
  .card-text > * {
    font-family:inherit;
    font-style:inherit;
    font-weight:inherit;
  }
  #filter-modal {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: rgba(10,10,10,0.8);
    padding: 0 50px;
    overflow: hidden;
    z-index: 909000;
  }
  #filter-modal > * {
    overflow: auto !important;
    z-index: 909010;
  }
</style>
@endpush

@section( 'content' )
@php
  $categories = ['display','serif','sans-serif','monospace','handwriting'];
  $loaded = [];
  $toLoad = [];
  $unLoad = [];
@endphp
<section class="container-fluid my-lg-3 my-2"><!-- container-fluid -->
  <div class="row">
    <div class="col-12 col-lg page-title">
      <h1>Google Fonts Selector</h1>
    </div>
    <div class="col-12 col-lg-4 d-flex flex-lg-row flex-column ml-auto">
      <h2>
        <small>Update Google Fonts</small>
      </h2>
      <a title="Update Google Fonts" class="cta btn px-lg-5 mx-lg-4 mx-3" href="/parse_google_fonts_file/update">Update</a>
    </div>
  </div>
</section>

<div class="container-fluid my-lg-3 my-0"><!-- container-fluid -->
  <section class="custom-container my-lg-3 my-2">
    <div class="row">
      <div class="col-12 col-lg-6 mb-lg-2 my-3">
        <form action="/parse_google_fonts_file/search" method="POST">
          @csrf()
          <div class="d-block col-12 px-lg-0">
            <h4>Search:</h4>
          </div>
          <label for="font-search" class="input-group-append">
            <input name="query" id="font-search" type="search" class="form-control" value="" placeholder="Search font-family ..." style="border-radius:0;"><!-- onkeyup="live_search()"-->
            <button type="submit" style="all: unset;box-shadow: unset !important;">
              <i class="fa fa-search" style="line-height:36px;width:36px;background-color:#495057;color:#fefefe;border:1px solid #ced4da;text-align:center;box-shadow:0 0 3px 1px rgba(73,80,87,0.1);"></i>
            </button>
          </label>
        </form>
      </div>
      <div class="col-12 col-lg-6 my-3 text-right">
        <div class="d-block col-12 px-lg-0">
          <h4>Filter/Ordering:</h4>
        </div>
        <select id="ordering" class="form-control ordering-filter" data-base-href="parse_google_fonts_file/orderBy">
          <option value="NULL">Select Ordering...</option>
          <option value="family/asc">Font Family Name ASCending</option>
          <option value="family/desc">Font Family Name DESCending</option>
          <option value="lastModified/asc">Last Modified ASCending</option>
          <option value="lastModified/desc">Last Modified DESCending</option>
          <option value="version/asc">Version ASCending</option>
          <option value="version/desc">Version DESCending</option>
        </select>
      </div>
    </div>
  </section>
</div>

<div class="col d-block d-lg-none">
  <button class="mobile-modal-activate">Show Filters</button>
</div>

<section class="vault-google-fonts-grid">
  <div class="filters-container my-lg-3 my-0 d-none d-lg-block"><!-- container-fluid -->
    <div class="row mx-lg-3">
      <div class="col-12 mt-lg-0 my-3 px-lg-0 px-3">
        <form action="/parse_google_fonts_file/filter" method="POST">
          @csrf()
          <div class="d-block col-12 px-lg-0 px-3">
            <h4>Category:</h4>
          </div>
          <label for="font-category" class="w-100">
            <select name="category" id="font-category" class="form-control" style="border-radius:0;"><!-- onchange="selected()"-->
              <option value="NULL">Filter by Category...</option>
              @forelse($categories as $key=>$category)
              <option value="{{ $category }}">{{ ucwords( $category ) }}</option>
              @empty
              @endforelse
            </select>
          </label>
          <div class="form-group my-lg-3 my-2">
            <div class="d-block col-12 px-lg-0 px-3">
              <h4>Variants:</h4>
            </div>
            {{--
            @forelse($variants as $key=>$variant)
            <label class="col-auto d-flex flex-row mx-0 mb-lg-1 px-lg-0 px-3 hidden">
              <input name="variants[]" type="checkbox" class="form-control mr-lg-3 mr-2 w-auto hidden" value="{{ $variant }}">
              <span hidden>{{ ucwords( $variant ) }}</span>
            </label>
            @empty
            @endforelse
            --}}
            <select name="variants[]" id="font-variant" class="form-control" style="border-radius:0;" multiple>
            <!-- onchange="selected()"-->
              @forelse($variants as $key=>$variant)
              <option value="{{ $variant }}">{{ ucwords( $variant ) }}</option>
              @empty
              @endforelse
            </select>
          </div>
          <div class="form-group my-lg-3 my-2">
            <div class="d-block col-12 px-lg-0 px-3">
              <h4>Subsets:</h4>
            </div>
            {{--
            @forelse($subsets as $key=>$subset)
            <label class="col-auto d-flex flex-row mx-0 mb-lg-1 px-lg-0 px-3 hidden">
              <input name="subsets[]" type="checkbox" class="form-control mr-lg-3 mr-2 w-auto hidden" value="{{ $subset }}">
              <span hidden>{{ ucwords( $subset ) }}</span>
            </label>
            @empty
            @endforelse
            --}}
            <select name="subsets[]" id="font-subset" class="form-control" style="border-radius:0;" multiple>
            <!-- onchange="selected()"-->
              @forelse($subsets as $key=>$subset)
              <option value="{{ $subset }}">{{ ucwords( $subset ) }}</option>
              @empty
              @endforelse
            </select>
          </div>
          <div class="row justify-content-around">
            <button class="col-11 col-lg-11" type="submit">Filter</button>
            <button class="col-5 col-lg-5" type="reset">Reset</button>
            <button class="col-5 col-lg-5" type="button" onclick="window.location.href = '/parse_google_fonts_file';">Clear All</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="fonts-container my-lg-3 my-4"><!-- container-fluid -->
    <section class="row cardcols my-lg-3 my-2 mx-lg-3 mx-auto" id="myUL">
      @forelse( $font_db as $key=>$font )
      <article class="col-lg-4 col-12 google_fonts card pb-lg-0 @if ( $key > 11 ) hidden @endif" data-font-family="{{ $font->family }}" data-category="{{ $font->category }}">
        @if( $key < 12 )
        @php $loaded[] = [ $key => $font->family ]; @endphp
        <script>
          (function (d=document, t='link') {
            var e = document.querySelector( '.google_fonts:nth-of-type( {{ $key+1 }} )' );
            var b = e.getBoundingClientRect();
            if( b.top > 0 && b.height > 0 ) {
              var gfonts_url_base = 'http://fonts.googleapis.com/css?family',
                  gfonts_font_sel = "<?php echo(str_replace(' ', '+', $font->family)); ?>",
                  gfonts_font_var = <?php echo('["' . implode('", "', $font->variants) . '"]'); ?>;

              var url_build, g, s;
                  url_build = gfonts_url_base + '=' + gfonts_font_sel + ':' + gfonts_font_var.join(',');

              try {
                g = d.createElement(t);
                g.rel = "stylesheet";
                g.href = url_build + '&display=swap';
                s = d.getElementsByTagName(t)[0];
                s.parentNode.insertBefore(g, s);
              } catch( err ) {
                console.clear();
                console.warn( '\r\n' + err + '\r\n' );
              }
            }
          })(document, 'link');
        </script>
        @else
          @php $toLoad[] = [ $key => $font->family ]; @endphp
        @endif
        <section class="card-body pb-lg-0 px-3 px-lg-4" style="font-family:{{ $font->family }},{{ $font->category }};font-display:swap;">
          <div class="card-title mb-lg-0 d-flex flex-lg-column flex-row flex-wrap justify-content-between ellipsis">
            <div class="row">
              <div class="col-12 col-lg font-title">
                <h3 class="card-title mb-0">
                  {{ ucwords( $font->family ) }} 
                </h3>
              </div>
              @isset($font->version)
              <div class="col-12 col-lg-auto font-version">
                <div class="text-center versioned" style="width:25px;height:25px;padding:0;border:1px solid #000;border-radius:50%;line-height:25px;">
                  <span style="font-family:fantasy!important;font-weight:300!important;font-size:11px!important;">{{ $font->version }}</span>
                </div>
              </div>
              @endisset
              @isset($font->subsets)
              <div class="col-12 col-lg-auto font-subsets">
                <span class="text-right-lg">
                  <abbr class="d-none d-lg-block" title="{{ ucwords( implode( ', ', $font->subsets ) ) }}" style="font-family:fantasy!important;font-weight:300!important;letter-spacing:calc(1em*(10/1000));">Subsets</abbr>
                  <span class="d-block d-lg-none" style="font-family:fantasy!important;font-weight:300!important;letter-spacing:calc(1em*(40/1000));">{{ ucwords( implode( ', ', $font->subsets ) ) }}</span>
                </span>
              </div>
              @endisset
            </div>
          </div>
          @if( null != $font->variants && count( $font->variants ) > 0 )
          <select class="order-last mb-lg-3 mb-4" onchange="font_variant( this, this.value );">
            <option value="NULL">Select Font Variant...</option>
            @foreach( $font->variants as $key=>$variant )
            <option value="{{ $variant }}">{{ ucwords( $variant ) }}</option>
            @endforeach
          </select>
          @endif
          <section class="card-text font-text-example" style="margin-bottom:1rem;">
            <h1 class="ellipsis">This is an example title</h1>
            <p class="ellipsis">Example body text...</p>
            <a class="cta btn">Example CTA</a>
          </section>
          <div class="row">
            <div class="col-auto mx-lg-3">
              <label for="title_font" class="d-flex">
                <input id="title_font" class="form-control col-lg-auto col" type="radio" name="title_font" value="{{ str_replace( ' ', '+', $font->family ) }}">
                <span style="line-height:38px;">Title</span>
              </label>
            </div>
            <div class="col-auto mx-lg-3">
              <label for="body_font" class="d-flex">
                <input id="body_font" class="form-control col-lg-auto col" type="radio" name="body_font" value="{{ str_replace( ' ', '+', $font->family ) }}">
                <span style="line-height:38px;">Body</span>
              </label>
            </div>
            <div class="col-auto mx-lg-3">
              <label for="cta_font" class="d-flex">
                <input id="cta_font" class="form-control col-lg-auto col" type="radio" name="cta_font" value="{{ str_replace( ' ', '+', $font->family ) }}">
                <span style="line-height:38px;">CTA</span>
              </label>
            </div>
          </div>
        </section>
      </article>
      @empty
      <div class="px-5">
        <h2>No results found!</h2>
        <a class="clear-filters" href="/parse_google_fonts_file">
          <span class="mr-3">&times;</span> Clear All Filters
        </a>
      </div>
      @endforelse
      @if ( null != $font_db->links() )
      <div class="container-fluid my-lg-3 my-5" style="overflow:hidden;">
        <div class="row">
          <?php echo($font_db->links()); ?>
        </div>
      </div>
      @endif
    </section>

  </div>
</section>

<section id="filter-modal" class="collapse d-lg-none">
  <div class="position-absolute" style="top:50px;right:2rem;z-index:1000000;">
    <span class="close-modal" style="font-size:3.5rem;font-weight:700;line-height:1;">&times;</span>
  </div>
</section>
@endsection

@push( 'pageScripts' )
<script>
var ordering = document.querySelector('#ordering');
    ordering.addEventListener( 'input', ordering_link, false );

function ordering_link(event) {
  if ( event == undefined || event == null || typeof( event ) != 'object' ) return;

  var evt = event;
  evt.preventDefault();

  var h;
  h = evt.target.getAttribute('data-base-href');

  var v, i, s, t;
  v = evt.target.value;
  i = evt.target.selectedIndex;
  s = evt.target.children[i];
  t = s.innerText;

  var tmp, url;
  tmp = [ '/', h, '/', encodeURI( v ) ];
  url = tmp.join('');

  delete( evt );
  delete( i ); delete( s ); delete( t );
  delete( tmp );

  setTimeout( ()=>{ window.location.href = url; }, 100 );

  return;
}

document.addEventListener(
  'click',
  (evt)=>{
    evt.stopPropagation();
    console.log( evt.target.classList.contains('close-modal') );
    if ( evt.target.classList.contains('close-modal') ) {
      var p = evt.target.parentNode.closest('#filter-modal'),
          r = evt.target.parentNode.nextElementSibling;
      p.classList.remove('show');
      r.parentNode.removeChild(r);
      return;
    } else if ( evt.target.classList.contains('mobile-modal-activate') ) {
      var m = document.querySelector('#filter-modal'),
          f = document.querySelector('.filters-container'),
          c = f.cloneNode(true);
      m.appendChild( c );
      c.classList.remove( 'd-none' );
      c.classList.add('center-in-modal');
      m.classList.add('show');
      return;
    }
  },
  false
);
</script>
<script id="debounce">
const debounce = (func, delay) => {
  let inDebounce;

  return function() {
    const context = this;
    const args = arguments;

    clearTimeout(inDebounce);

    inDebounce = setTimeout( 
      () => func.apply(context, args),
      delay
    );
  }
}
/**
 * USAGE:
 debounceBtn.addEventListener('click', debounce(function() {
  console.info('Hey! It is', new Date().toUTCString());
 }, 3000));
;
 */
</script>
<script id="throttle">
const throttle = (func, limit) => {
  let lastFunc;
  let lastRan;

  return function() {
    const context = this;
    const args = arguments;

    if (!lastRan) {
      func.apply(context, args)
      lastRan = Date.now()
    } else {
      clearTimeout(lastFunc)
      lastFunc = setTimeout( 
        () => {
          if ((Date.now() - lastRan) >= limit) {
            func.apply(context, args)
            lastRan = Date.now()
          }
        },
        limit - (Date.now() - lastRan)
      )
    }
  }
}
/**
 * USAGE:
 throttleBtn.addEventListener('click', throttle(function() {
  return console.log('Hey! It is', new Date().toUTCString());
 }, 1000));
 */
</script>
<script>
var i  = 0;
var o  = 11;
var m  = 1;
var n  = 1;
var es = [].slice.call( document.querySelectorAll( '.card.hidden' ) );
var d  = document;
var t  = 'link';
window.addEventListener( 'scroll', throttle( function() {
  var chk = Math.floor( document.querySelector( '.cardcols' ).clientHeight / 10 ) * 10;
  var offset = window.scrollY + window.innerHeight;
  var height = document.documentElement.offsetHeight;

  if ( offset >= height - 150 ) {
    if( es.slice(i, o).length == 0 ) {
      window.removeEventListener( 'scroll', (evt)=>{evt.preventDefault(); return;}, false );
    }
    es.slice(i, o).forEach( ( e ) => {
      e.classList.remove('hidden');
      var font2load = e.children[0].style.fontFamily
                      .split(',')[0]
                      .toString()
                      .replace( /\s+?/ig, '+' )
                      .replace( /\"/ig, '' );
      var gfonts_url_base = 'http://fonts.googleapis.com/css?family',
          gfonts_font_sel = font2load,
          gfonts_font_var = []; //<?php //echo('["' . implode('", "', $font->variants) . '"]');?>;

      var url_build, g, s;
          url_build = gfonts_url_base + '=' + gfonts_font_sel;// + ':' + gfonts_font_var.join(',');

      try {
        g = d.createElement(t),
        g.rel = "stylesheet";
        g.href = url_build + '&display=swap';
        s = d.getElementsByTagName(t)[0];
        s.parentNode.insertBefore(g, s);
      } catch( err ) {
        console.clear();
        console.warn( '\r\n' + err + '\r\n' );
      }
    });
    i += 3 * m + 1;
    o += i + 4;
    m++;
    n += 0.25;
  }
}, 500 ), false);
</script>
<script>
  function live_search() {
    // Declare variables
    var input, filter, ul, li, a, i, txtValue;
    input  = document.getElementById('font-search');
    filter = input.value.toUpperCase();
    ul     = document.getElementById("myUL");
    li     = ul.getElementsByTagName('article');
    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < li.length; i++) {
      a = li[i];//.getElementsByTagName("a")[0];
      txtValue = a.getAttribute('data-font-family');//a.textContent || a.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        li[i].style.display         = "";
        li[i].style.backgroundColor = "#1a1a1a";
        li[i].style.color           = "#fefefe";
        li[i].style.borderColor     = "#fefefe";
        li[i].querySelector( '.cta' ).style.borderColor = "#fefefe";
      } else {
        li[i].style.display         = "none";
      }
    }
  }
  function selected() {
    // Declare variables
    var input, filter, ul, li, a, i, txtValue;
    input  = document.getElementById('font-category');
    filter = input.value.toUpperCase();
    ul     = document.getElementById("myUL");
    li     = ul.getElementsByTagName('article');
    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < li.length; i++) {
      a = li[i];//.getElementsByTagName("a")[0];
      txtValue = a.getAttribute('data-category');//a.textContent || a.innerText;
      console.log( txtValue.toUpperCase() );
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        li[i].style.display         = "";
        li[i].style.backgroundColor = "#1a1a1a";
        li[i].style.color           = "#fefefe";
        li[i].style.borderColor     = "#fefefe";
        li[i].querySelector( '.cta' ).style.borderColor = "#fefefe";
      } else {
        li[i].style.display         = "none";
      }
    }
  }
  function font_variant(elem, style) {
    var kindaStyle = style.match(/(\d{3}|\w+)/ig);
    var p = elem.parentNode.closest( '.card-body' );
        p.style.fontStyle  = 'unset';
        p.style.fontWeight = 'unset';
    kindaStyle.forEach( ( e ) => {
      if ( isNaN( parseInt( e ) ) ) {
        if (e == 'regular') {
          p.style.fontWeight = 'normal';
        } else {
          p.style.fontStyle  = e;
        }
      } else {
        p.style.fontWeight = parseInt( e );
      }
    });
  }
</script>
@endpush
