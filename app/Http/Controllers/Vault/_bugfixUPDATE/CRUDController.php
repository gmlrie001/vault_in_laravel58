<?php namespace App\Http\Controllers\Vault;

/* Base Controller Include */
use App\Http\Controllers\Services\VaultController;

/* Facade Includes */
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

use App\Models\ProductCategoryProduct;

class CRUDController extends VaultController
{
    protected $model = null;
    protected $createRequest = null;
    protected $updateRequest = null;

    public function __construct()
    {
        parent::__construct();
        $tableName = Route::input('tableName');

        if (Schema::hasTable($tableName)) {
            $tableNameSingular = studly_case(strtolower(str_singular($tableName)));

            $modelNamespace = str_replace(["{", "}"], ["", ""], "App\Models\{$tableNameSingular}");
            $this->createRequest = str_replace(["{", "}"], ["", ""], "App\Http\Requests\{$tableNameSingular}\CreateRequest");
            $this->updateRequest = str_replace(["{", "}"], ["", ""], "App\Http\Requests\{$tableNameSingular}\UpdateRequest");
            
            try {
                $this->model = (new $modelNamespace);
            } catch (Exception $e) {
                Session::flash('error', 'Model not found');
                Redirect::to('/vault')->send();
            }
        } else {
            Session::flash('error', 'Table does not exist');
            Redirect::to('/vault')->send();
        }
    }

    private function getTabList($tbl, $id = 0, $reference = [])
    {
        if ($id == 0 || $tbl == "") {
            return $reference;
        } else {
            $currentTableNameSingular = studly_case(strtolower(str_singular($tbl)));
            
            $currentModelNamespace = str_replace(["{", "}"], ["", ""], "App\Models\{$currentTableNameSingular}");
            $currentModel = (new $currentModelNamespace);
    
    
            if ($currentModel->parentTable != "") {
                $currentEntry = $currentModel::find($id);
                $reference[sizeof($reference)]['edit'] = [$tbl => (int)$id];
                $reference[sizeof($reference)]['relation'] = [$tbl => (int)$currentEntry->{$currentModel->parentOrder}];
                return $this->getTabList($currentModel->parentTable, $currentEntry->{$currentModel->parentOrder}, $reference);
            } else {
                $reference[sizeof($reference)]['edit'] = [$tbl => (int)$id];
                return $reference;
            }
        }
    }
    

    public function listing($tableName, $relationId = 0)
    {
        $this->data['table'] = $tableName;
        $this->data['titleField'] = $this->model->titleField;
        if (isset($this->model->titleMethod)) {
            $this->data['titleMethod'] = $this->model->titleMethod;
        }
        $this->data['orderField'] = $this->model->orderField;
        $this->data['statusField'] = $this->model->statusField;
        $this->data['mainDropdownField'] = $this->model->mainDropdownField;
        $this->data['imageDropdownField'] = $this->model->imageDropdownField;
        $this->data['can_order'] = $this->model->orderable;
        $this->data['has_status'] = $this->model->hasStatus;
        if ($this->model->orderField == "" && $relationId == 0) {
            $this->data['can_order'] = false;
        }
        if (isset($this->model->dateField)) {
            $this->data['dateField'] = $this->model->dateField;
        }
        $this->data['orderOptions'] = $this->model->orderOptions;
        $this->data['showRelationships'] = false;
        Session::put('relation_id', $relationId);
        
        if ($this->model->parentTable == "") {
            $this->data['tab_list'] = [0 => ['edit' => [$tableName => 0]]];
        } elseif ($relationId == 0) {
            $this->data['tab_list'] = [0 => ['edit' => [$tableName => 0]]];
        } else {
            $this->data['tab_list'] = $this->getTabList($this->model->parentTable, $relationId);
        }
        
        $orderField = Schema::hasColumn($tableName, Session::has('order.field')) ? Session::get('order.field') : $this->model->orderField;
        $orderDirection = Schema::hasColumn($tableName, Session::has('order.field')) ? Session::get('order.direction') : $this->model->orderDirection;
        
        if (Session::has('search_results')) {
            $this->data['results'] = collect(Session::get('search_results'));
            $this->data['total_items'] = sizeof(Session::get('search_results'));
            $this->data['link_orders'] = null;
            $this->data['can_order'] = false;
        } else {
            if ($relationId == 0) {
                if ($this->model->parentOrder != "") {
                    $this->data['results'] = $this->model::orderBy($orderField, $orderDirection)->paginate(Session::get('limit'));
                } else {
                    $this->data['results'] = $this->model::orderBy($orderField, $orderDirection)->paginate(Session::get('limit'));
                }
                
                $this->data['total_items'] = $this->data['results']->total();
    
                if ($this->model->orderable) {
                    $this->data['link_orders'] = $this->model::orderBy($orderField, $orderDirection)->pluck($this->model->orderField, $this->model->orderField);
                } else {
                    $this->data['link_orders'] = null;
                }
            } else {
                $this->data['results'] = $this->model::where($this->model->parentOrder, $relationId)->orderBy($orderField, $orderDirection)->paginate(Session::get('limit'));
                if ($this->model->orderable) {
                    $this->data['link_orders'] = $this->model::where($this->model->parentOrder, $relationId)->orderBy($orderField, $orderDirection)->pluck($this->model->orderField, $this->model->orderField);
                } else {
                    $this->data['link_orders'] = null;
                }
            }
        }
            
        return view('vault.crud.listing', $this->data);
    }
    
    public function add($tableName)
    {
        $this->data['method'] = 'add';

        $this->data['showRelationships'] = true;

        $this->data['table'] = $tableName;

        if ($this->model->parentTable == "") {
            $this->data['tab_list'] = [0 => ['edit' => [$tableName => 0]]];
        } else {
            $this->data['tab_list'] = $this->getTabList($this->model->parentTable, Session::get('relation_id'));
        }

        $this->data['fields'] = $this->model->fields;

        $this->data['relationships'] = [];

        $this->data['entry'] = new $this->model();
        
        return view('vault.crud.form', $this->data);
    }
    
    public function store($tableName, Request $request)
    {
        $request = app($this->createRequest);

        $this->data['method'] = 'add';
        
        $this->data['showRelationships'] = true;

        $this->data['table'] = $tableName;

        $this->data['fields'] = $this->model->fields;

        $this->data['relationships'] = [];
        if ($this->model->parentTable == "") {
            $this->data['tab_list'] = [0 => ['edit' => [$tableName => 0]]];
        } else {
            $this->data['tab_list'] = $this->getTabList($this->model->parentTable, Session::get('relation_id'));
        }

        $entry = $this->model::create($request->all());
        if ($this->model->orderable == true) {
            if ($this->model->parentOrder == "") {
                $entry->{$this->model->orderField} = $this->model::count();
            } else {
                $entry->{$this->model->orderField} = $this->model::where($this->model->parentOrder, $entry->{$this->model->parentOrder})->count();
            }
        }

        $entry->save();

        if ($tableName == "products" && $request->has('category_id')) {
            $categoryIDs = explode(',', $request->get('category_id'));
            
            ProductCategoryProduct::where('product_id', $entry->id)->forceDelete();

            foreach ($categoryIDs as $categoryID) {
                $cat_prod = new ProductCategoryProduct();

                $cat_prod->product_id = $entry->id;
                $cat_prod->category_id = $categoryID;

                $cat_prod->save();
            }
        }

        Session::flash('message', 'Entry has been added successfully');
        
        $this->data['entry'] = new $this->model();
            
        return view('vault.crud.form', $this->data);
    }
    
    public function edit($tableName, $id)
    {
        $this->data['method'] = 'update';

        $this->data['showRelationships'] = true;

        $this->data['table'] = $tableName;

        $this->data['fields'] = $this->model->fields;

        $this->data['relationships'] = $this->model->relationships;

        $this->data['entry'] = $this->model::find($id);
        if ($this->model->parentTable == "") {
            $this->data['tab_list'] = [0 => ['edit' => [$tableName => 0]]];
        } else {
            $this->data['tab_list'] = $this->getTabList($this->model->parentTable, $this->data['entry']->{$this->model->parentOrder});
        }
        
        return view('vault.crud.form', $this->data);
    }
    
    public function update($tableName, $id)
    {
        $request = app($this->updateRequest); // FormRequest

        $this->data['method'] = 'update';

        $this->data['showRelationships'] = true;

        $this->data['entry'] = $this->model::find($id);

        $this->data['table'] = $tableName;
        if ($this->model->parentTable == "") {
            $this->data['tab_list'] = [0 => ['edit' => [$tableName => 0]]];
        } else {
            $this->data['tab_list'] = $this->getTabList($this->model->parentTable, $this->data['entry']->{$this->model->parentOrder});
        }

        $this->data['fields'] = $this->model->fields;

        $this->data['relationships'] = $this->model->relationships;
        
        $entry = $this->model::find($id);

        $entry->update($request->all());
        
        if ($tableName == "products" && $request->has('category_id')) {
            $categoryIDs = explode(',', $request->get('category_id'));
            
            ProductCategoryProduct::where('product_id', $entry->id)->forceDelete();

            foreach ($categoryIDs as $categoryID) {
                $cat_prod = new ProductCategoryProduct();

                $cat_prod->product_id = $entry->id;
                $cat_prod->category_id = $categoryID;

                $cat_prod->save();
            }
        }

        Session::flash('message', 'Entry has been updated successfully');

        return view('vault.crud.form', $this->data);
    }
    
    public function duplicate($tableName, $id)
    {
        $this->data['method'] = 'add';
        
        $this->data['entry'] = $this->model::find($id);

        $this->data['showRelationships'] = true;
        if ($this->model->parentTable == "") {
            $this->data['tab_list'] = [0 => ['edit' => [$tableName => 0]]];
        } else {
            $this->data['tab_list'] = $this->getTabList($this->model->parentTable, $this->data['entry']->{$this->model->parentOrder});
        }

        $this->data['table'] = $tableName;

        $this->data['fields'] = $this->model->fields;

        $this->data['relationships'] = [];
        
        return view('vault.crud.form', $this->data);
    }

    public function delete($tableName, $id)
    {
        $entry = $this->model::find($id);

        if ($tableName == 'products' && ($entry->parent_id != null || $entry->parent_id != 0)) {
            return Redirect::back()->with('error', 'Please unassign this variant before deleting it!');
        }

        if ($this->model->orderable == true) {
            if ($this->model->parentOrder == "") {
                $this->model::where($this->model->orderField, '>', $entry->{$this->model->orderField})->decrement($this->model->orderField);
            } else {
                $this->model::where($this->model->parentOrder, $entry->{$this->model->parentOrder})->where($this->model->orderField, '>', $entry->{$this->model->orderField})->decrement($this->model->orderField);
            }
        }

        $entry->delete();

        return Redirect::back()->with('message', 'Entry deleted successfully');
    }
    
    public function deletePermanent($tableName, $id)
    {
        $entry = $this->model::withTrashed()->find($id);
        if ($tableName == 'products' && ($entry->parent_id != null || $entry->parent_id != 0)) {
            return Redirect::back()->with('error', 'Please unassign this variant before deleting it!');
        }
        $entry->forceDelete();

        return Redirect::back()->with('message', 'Entry deleted permanently');
    }

    public function deleteSelected($tableName)
    {
        $selectedItems = explode(',', Input::get('ids'));
        $results = $this->model::whereIn('id', $selectedItems)->get();

        if ($tableName == 'products') {
            foreach ($results as $res) {
                if ($res->parent_id != null || $res->parent_id != 0) {
                    return Redirect::back()->with('error', 'Please unassign '.$res->title.' before deleting it!');
                }
            }
        }
        
        if ($this->model->orderable == true) {
            if ($this->model->parentOrder == "") {
                foreach ($results as $key => $result) {
                    $this->model::where('order', '>', $result->order)->decrement('order');
                    $this->model::where('id', $result->id)->delete();
                }
            } else {
                foreach ($results as $key => $result) {
                    $this->model::where($this->model->parentOrder, $result->{$this->model->parentOrder})->where('order', '>', $result->order)->decrement('order');
                    $this->model::where($this->model->parentOrder, $result->{$this->model->parentOrder})->where('id', $result->id)->delete();
                }
            }
        } else {
            foreach ($results as $key => $result) {
                $this->model::where('id', $result->id)->delete();
            }
        }
        return Redirect::back()->with('message', 'Successfully removed');
    }
    
    public function deletePermanentSelected($tableName)
    {
        $selectedItems = explode(',', Input::get('ids'));
        $results = $this->model::whereIn('id', $selectedItems)->onlyTrashed()->get();

        if ($tableName == 'products') {
            foreach ($results as $res) {
                if ($res->parent_id != null || $res->parent_id != 0) {
                    return Redirect::back()->with('error', 'Please unassign '.$res->title.' before deleting it!');
                }
            }
        }

        foreach ($results as $key => $result) {
            $this->model::where('id', $result->id)->forceDelete();
        }
                
        return Redirect::back()->with('message', 'Successfully deleted');
    }
    
    public function restore($tableName, $id)
    {
        $entry = $this->model::withTrashed()->where('id', $id)->first();
        
        $entry->restore();

        if ($this->model->orderable == true) {
            if ($this->model->parentOrder == "") {
                $entry->{$this->model->orderField} = $this->model::count();
            } else {
                $entry->{$this->model->orderField} = $this->model::where($this->model->parentOrder, $entry->{$this->model->parentOrder})->count();
            }
        }
        
        $entry->save();

        $entry->restore();

        return Redirect::back()->with('message', 'Entry restored');
    }
}
