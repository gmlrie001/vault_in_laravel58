<?php namespace App\Http\Controllers\Vault;

/* Base Controller Include */
use App\Http\Controllers\Services\VaultController;

/* Facade Includes */
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

/* Model Includes */

/* Request Includes */

class LimitController extends VaultController
{
    public function __invoke($count)
    {
        Session::put('limit', $count);
        
        return Redirect::back();
    }
}
