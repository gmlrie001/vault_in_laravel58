@extends('layouts.vault')

@section('content')
    <div class="container-fluid content">
		@include('includes.vault.listing.tabs')
		{!! Form::model($entry, array('files' => 'true', 'class' => 'form-page')) !!}
		<input type="hidden" value="{{$entry->id}}" name="id" />
				@if(! isset( $fields ) || empty( $fields ) || $fields == '' )
					@php
						$font_collection = App\Models\GoogleFont::status()->ordering()->paginate(36);
						$font_db = $font_collection;
						$categories = null;
						$variants = $subsets = [];
						$method = 'update';
					@endphp
					@include('includes.vault.fields.'.'font_cards', [
						'fonts' => $font_collection,
						'categories' => $categories,
						'variants' => $variants,
						'subsets' => $subsets,
						$method => 'update'
					]);
				@else
					@foreach($fields as $field)
						<?php switch ($field[0]) {
                            case 'open_parent':
                                            echo '<div class="col-xs-12 bordered">';
                                            echo '<h3 class="col-xs-12 section-title">'.$field[1].'</h3>';
                                            break;
                            case 'close_parent':
                                            echo '</div>';
                                            break;
                            case 'open_row':
                                            echo '<div class="col-xs-12 padding-0">';
                                            break;
                            case 'close_row':
                                            echo '</div>';
                                            break;
                            default:
                        ?>
								<div class="{{$field[7]}}">
									@if($field[2] != "select")
										@include('includes.vault.fields.'.$field[2], [
											'name' => $field[0], 
											'label' => $field[1], 
											'value' => $entry->{$field[0]},
											'options' => $field[3], 
											'options_selected' => $field[4], 
											'width' => $field[5], 
											'height' => $field[6], 
											'can_remove' => $field[8]
										])
									@elseif(sizeof($field) > 9)
										@include('includes.vault.fields.'.$field[2], [
											'name' => $field[0], 
											'label' => $field[1], 
											'value' => $entry->{$field[0]},
											'options' => $field[3], 
											'options_selected' => $field[4], 
											'width' => $field[5], 
											'height' => $field[6], 
											'can_remove' => $field[8], 
											'use_parent' => $field[9]
										])
									@else
										@include('includes.vault.fields.'.$field[2], [
											'name' => $field[0], 
											'label' => $field[1], 
											'value' => $entry->{$field[0]},
											'options' => $field[3], 
											'options_selected' => $field[4], 
											'width' => $field[5], 
											'height' => $field[6], 
											'can_remove' => $field[8]
										])
									@endif
								</div>
						<?php } ?>
					@endforeach
				@endif
			{!! Honeypot::generate('my_name', 'my_time') !!}
			@if($method == 'update')
				{!! Form::submit($method, array('class' => 'btn btn-success btn-lg btn-block edit-btn')) !!}
			@else
				{!! Form::submit($method, array('class' => 'btn btn-success btn-lg btn-block')) !!}
			@endif
		{!! Form::close() !!}
    </div>
@endsection