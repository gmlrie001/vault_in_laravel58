<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        // On-the-fly conversion to money format.
        // Blade::directive('convert', function ($money) {
        //     return number_format($money, 2);
        // });

        // Blade::component(
        //   'pages.includes.modals.share', 'social_share_modal'
        // );
    }
}
