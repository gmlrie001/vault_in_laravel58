<?php namespace App\Http\Controllers\Vault;

/* Base Controller Include */
use App\Http\Controllers\Controller;

/* Facade Includes */
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

/* Model Includes */
use App\Models\User;
use App\Models\LoginLog;

/* Request Includes */
use App\Http\Requests\Login\VaultLoginRequest;

class LoginController extends Controller
{
    public function index()
    {
        $this->data['bg_number'] = rand(1, 15);
                
        return view('vault.auth.login', $this->data);
    }
    
    public function login(VaultLoginRequest $request)
    {
        $this->data['bg_number'] = rand(1, 15);

        $user = User::where('email', $request->email)->where('status', 'PUBLISHED')->first();
        
        if (Auth::attempt(['password'=>$request->password, 'status'=>'PUBLISHED', 'email'=>$request->email])) {
            $log = new LoginLog();
            $log->user_id = $user->id;
            $log->ip_address = $request->getClientIp();
            $log->save();

            Session::put('user.id', $user->id);
            Session::put('user.name', $user->name);
            Session::put('user.surname', $user->surname);
            Session::put('user.admin_type', $user->admin_type);
            Session::put('user.user_group_id', $user->user_group_id);
            return Redirect::to('/vault');
        }

        return view('vault.auth.login', $this->data);
    }
    
    public function logout()
    {
        Auth::logout();
        
        Session::put('user.id', null);
        Session::put('user.name', null);
        Session::put('user.surname', null);
        Session::put('user.admin_type', null);

        Session::put('order', null);

        return Redirect::to('/vault/login');
    }
}
