<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;

class Collection extends Model
{
    use SoftDeletes;
    
    public $orderable = true;
    public $orderField = "order";
    public $titleField = "title";
    public $statusField = "status";
    public $hasStatus = true;
    public $orderDirection = "asc";
    // public $parentOrder = "category_id";
    // public $parentTable = "categories";
    public $parentOrder = "";
    public $parentTable = "";
    public $orderOptions = ['title', 'updated_at'];
    public $relationships = [
        'collection_options' => 'options',
        'collection_related_items' => 'related_items',
        // 'collection_share_links' => 'sharing_links',
        'collection_galleries' => 'galleries',
    ];
    public $mainDropdownField = "url_title";
    public $imageDropdownField = "listing_image";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'url_title',
        'description',
        'category_id',
        'listing_image',
        'video_link',
        'seo_title',
        'seo_keywords',
        'seo_description',
        'status',
        'status_date',
    ];
    
    public $fields = [
    //  ['field_name', 'label', 'field_type', 'options_model', 'options_relationship', 'width', 'height', 'container_class', 'can_remove'],
        ['title', 'Title', 'title', '', '', '', '', 'col-xs-12 col-md-6', ''],
        ['url_title', 'URL Title', 'url_title', '', '', '', '', 'col-xs-12 col-md-6', ''],
        // ['category_id', 'Select Category', 'select', 'Category', 'children', '', '', 'col-xs-12 col-md-6', ''],

        ['open_parent', 'Item Desciption', ''],
            ['open_row', '',''],
                ['description', 'Description', 'wysiwyg', '', '', '', '', 'col-xs-12 col-md-12', ''],
            ['close_row', '',''],
        ['close_parent', 'Item Desciption', ''],

        ['open_parent', 'Listing Image and Video Link Options', ''],
            ['open_row', '',''],
                ['listing_image', 'Listing', 'image', '', '', '270', '385', 'col-xs-12 col-md-6', 'can_remove'],
                ['video_link', 'Item Video Link', 'text', '', '', '', '', 'col-xs-12 col-md-6', ''],
            ['close_row', '',''],
        ['close_parent', 'Listing Image and Video Link Options', ''],

        ['open_parent', 'SEO Information', ''],
            ['open_row', '', ''],
                ['seo_title', 'SEO Title', 'text', '', '', '', '', 'col-xs-12 col-md-6', ''],
            ['close_row', '', ''],
            ['open_row', '', ''],
                ['seo_description', 'SEO Description', 'textarea', '', '', '', '', 'col-xs-12 col-md-6', ''],
                ['seo_keywords', 'SEO Keywords', 'tags', '', '', '', '', 'col-xs-12 col-md-6', ''],
            ['close_row', '', ''],
        ['close_parent', 'SEO Information', ''],

        ['open_row', '',''],
            ['status', 'Status', 'status', '', '', '', '', 'col-xs-12 col-md-6', ''],
        ['close_row', '',''],
    ];

    public function options()
    {
        return $this->hasMany(CollectionOption::class, 'collection_id')->orderBy('order', 'asc');
    }

    public function related_items()
    {
        return $this->hasMany(CollectionRelatedItem::class, 'collection_id')->orderBy('order', 'asc');
    }

    public function sharing_links()
    {
        return $this->hasMany(CollectionShareLink::class, 'collection_id')->orderBy('order', 'asc');
    }

    public function galleries()
    {
        return $this->hasMany(CollectionGallery::class, 'collection_id')->orderBy('order', 'asc');
    }

    public function category()
    {
        return $this->belongsToMany(Category::class, 'category_id')->withDefault();
    }

    /**
     * Scope a query to only include certain status'.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeStatus($query)
    {
        return $query->where('status', 'PUBLISHED')->orWhere('status', 'SCHEDULED')
                     ->where('status_date', '<=', now());
    }
}
