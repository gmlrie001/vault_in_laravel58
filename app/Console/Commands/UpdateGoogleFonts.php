<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\GoogleFonts\GoogleFontsHelper;

class UpdateGoogleFonts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start = time();
        $this->data['memini'] = memory_get_usage();

        $pth = 'assets/json/test.json';
        $key = 'AIzaSyAvMVD7pM9FyquH84i-CiBddZ0IAiFfAMk';
        $googleFonts = new App\Helpers\GoogleFonts\GoogleFontsHelper($pth, $key, true);

        if ($googleFonts->font_file_exists()) {
            $path = str_split($googleFonts->get_localPath());
            $extn = array_pop($path);
            $googleFonts->set_localPath($path.'_'.date('YmdHis', strtotime(now())).'.'.$extn);
            unset($path, $extn);
            dd(new \ReflectionClass($googleFonts));
        }

        $contents = file_get_contents(public_path($googleFonts->get_localPath()));
        dd($contents);

        $this->data['time_taken'] = time() - $start;
        $this->data['memend'] = memory_get_usage();

        unset($start, $pth, $key, $googleFonts);

        return;
    }
}
