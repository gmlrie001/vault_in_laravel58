<?php

use App\Helpers\GoogleFonts\GoogleFontsHelper;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Arr;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* ============================================================================================ */

Route::get('/', 'Page\HomeController')->name('home');

/* ============================================================================================ */
/* Listing */
// 'vault/parse_google_fonts_file',
Route::get(
    '/parse_google_fonts_file',
    'Vault\GoogleFontController'
)->name('fonts_listing');

/* ============================================================================================ */
/* Update */
// 'vault/parse_google_fonts_file/update',
Route::get(
    '/parse_google_fonts_file/update',
    'Vault\GoogleFontController@update'
)->name('fonts_update');

/* ============================================================================================ */
/* Search */
// 'vault/parse_google_fonts_file/search',
Route::any(
    '/parse_google_fonts_file/search',
    'Vault\GoogleFontController@search'
)->name('fonts_search');

/* ============================================================================================ */
/* Filter */
// 'vault/parse_google_fonts_file/filter',
Route::any(
    '/parse_google_fonts_file/filter',
    'Vault\GoogleFontController@filter'
)->name('fonts_filter');

/* ============================================================================================ */
/* Filter Ordering: LAST-MODIFIED and VERSION */
// 'vault/parse_google_fonts_file/orderBy/{col}/{dir}',
Route::any(
    '/parse_google_fonts_file/orderBy/{col}/{dir}',
    'Vault\GoogleFontController@ordering'
)->name('fonts_ordering');

/* ============================================================================================ */
