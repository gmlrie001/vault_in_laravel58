<?php

use App\Helpers\GoogleFonts\GoogleFontsHelper;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Arr;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* ============================================================================================ */

Route::get('/parse_google_fonts_file', 'Page\GoogleFontController')->name('fonts');

/* ============================================================================================ */

Route::get('/parse_google_fonts_file/update', 'GoogleFontController@update')-name('fonts_update');

/* ============================================================================================ */

Route::get('/parse_google_fonts_file', function () {
    if ((new App\Models\GoogleFont)::count() > 0) {
        $font_db = (new App\Models\GoogleFont)::paginate(35);

        /* Process WebFonts */
        $font_collection = (new App\Models\GoogleFont)::all();
        $categories = [];
        $variants = [];
        $subsets = [];
        foreach ($font_collection as $key=>$font) {
            $categories[] = $font->category;
            $variants[]   = $font->variants;
            $subsets[]    = $font->subsets;
        }
        $unique_categories = array_values(array_unique($categories));
        $unique_variants   = array_unique(array_flatten($variants));
        $unique_subsets    = array_unique(array_flatten($subsets));

        $this->data['categories'] = Arr::sort($unique_categories);
        $tmp = array_values($unique_variants);
        $this->data['variants']   = Arr::sort($tmp);
        $tmp = array_values($unique_subsets);
        $this->data['subsets']    = Arr::sort($tmp);
        // dd($this->data);

        $this->data['font_db'] = $font_db;
        /* Process WebFonts */

        return view('includes.pages.cards', $this->data);
    }
})->name('parse_google_fonts');

/* ============================================================================================ */

/* Filter */
Route::any('/parse_google_fonts_file/filter', function () {
})->name('filter_google_fonts');

/* ============================================================================================ */

/* Search by FAMILY */
Route::any('/parse_google_fonts_file/search', function () {
    // $variant_query = $subset_query = $categoryName = null;
    $input_filters = Request::all();
    if (isset($input_filters['query'])) {
        $search_query  = $input_filters['query'];
    }
    $filtered = App\Models\GoogleFont::where('status', 'PUBLISHED')
          ->where(
              function ($query) use ($search_query) {
                  $query->where('family', 'LIKE', '%' . e($search_query) . '%');
              }
          )
          ->paginate(35);

    $this->data['font_db'] = $filtered->appends($input_filters);
    $categories = [];
    $variants = [];
    $subsets = [];
    foreach ($filtered as $key=>$font) {
        $categories[] = $font->category;
        $variants[]   = $font->variants;
        $subsets[]    = $font->subsets;
    }
    $unique_categories = array_values(array_unique($categories));
    $unique_variants = array_unique(array_flatten($variants));
    $unique_subsets = array_unique(array_flatten($subsets));

    $this->data['categories'] = Arr::sort($unique_categories);
    $tmp = array_values($unique_variants);
    $this->data['variants']   = Arr::sort($tmp);
    $tmp = array_values($unique_subsets);
    $this->data['subsets']    = Arr::sort($tmp);

    return view('includes.pages.cards', $this->data);
})->name('search_google_fonts');

/* ============================================================================================ */

/* Filter Ordering: LAST-MODIFIED and VERSION */
Route::any('/parse_google_fonts_file/orderBy/{what}/{dir}', function ($what, $dir) {
    $filtered = App\Models\GoogleFont::where('status', 'PUBLISHED')
                ->orderBy($what, $dir)->paginate(35);
    $this->data['font_db'] = $filtered;
    $categories = [];
    $variants = [];
    $subsets = [];
    foreach ($filtered as $key=>$font) {
        $categories[] = $font->category;
        $variants[]   = $font->variants;
        $subsets[]    = $font->subsets;
    }
    $unique_categories = array_values(array_unique($categories));
    $unique_variants = array_unique(array_flatten($variants));
    $unique_subsets = array_unique(array_flatten($subsets));

    $this->data['categories'] = Arr::sort($unique_categories);
    $tmp = array_values($unique_variants);
    $this->data['variants']   = Arr::sort($tmp);
    $tmp = array_values($unique_subsets);
    $this->data['subsets']    = Arr::sort($tmp);

    // dd($what, $dir, $filtered->first());
    return view('includes.pages.cards', $this->data);
})->name('filtered_ordering');

/* ============================================================================================ */

/* ============================================================================================ */
/* Filter: CATEGORY */
Route::get('/parse_google_fonts_file/filter/category/{categoryName}', function ($categoryName) {
    $filtered = App\Models\GoogleFont::where('category', $categoryName)->paginate(35);
    $this->data['font_db'] = $filtered;
    // dd($categoryName, $filtered);
    return view('includes.pages.cards', $this->data);
})->name('filter_by_category_google_fonts');
/* ============================================================================================ */
/* Filter: VARIANTS */
Route::get('/parse_google_fonts_file/filter/variants/{variant}', function ($variant) {
    $variant_query = explode(',', $variant);
    // dd($variant, explode(',', $variant));
    $filtered = App\Models\GoogleFont::whereJsonContains('variants', $variant_query)->paginate(35);
    $this->data['font_db'] = $filtered;
    // dd($variant, $filtered->first());
    return view('includes.pages.cards', $this->data);
})->name('filter_by_variant_google_fonts');
/* ============================================================================================ */
/* Filter: SUBSETS */
Route::get('/parse_google_fonts_file/filter/subsets/{subset}', function ($subset) {
    $subset_query = explode(',', $subset);
    // dd($subset, explode(',', $subset));
    $filtered = App\Models\GoogleFont::whereJsonContains('subsets', $subset_query)->paginate(35);
    $this->data['font_db'] = $filtered;
    // dd($subset, $filtered->first());
    return view('includes.pages.cards', $this->data);
})->name('filter_by_subset_google_fonts');
/* ============================================================================================ */
