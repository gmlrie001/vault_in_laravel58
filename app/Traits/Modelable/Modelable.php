<?php

namespace App\Traits\Modelable;

trait Modelable {

    public function opening_condition( $model, $relation_id=null, $parent_model_id=null ) {
        $m = ( new $model() );
        $r = ( null == $relation_id || ! $relation_id ) ?: $relation_id;
        $p = ( null == $parent_model_id || ! $parent_model_id ) ?: $parent_model_id;

        return $this->hasMany( $m, $p, $r )->where( 'status', 'PUBLISHED' )
                    ->orWhere( 'status', 'SCHEDULED' )->where( 'status_date', '>=', now() );
    }
}