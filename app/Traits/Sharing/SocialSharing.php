<?php namespace App\Traits\Sharing;

use Illuminate\Database\Eloquent\Collection;

trait SocialSharing
{
    public function setup_sharing($sm)
    {
        if ($sm instanceof Collection) {
            $sm = $sm->pluck('platform')->toArray();
        }

        $this->data['item_sharing'] = collect(Parent::_socshr($this->data['page'], $sm));
    
        return $this->data['item_sharing']->reverse()->unique();
    }
}
