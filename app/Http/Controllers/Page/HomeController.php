<?php namespace App\Http\Controllers\Page;

/* Base Controller Include */
use App\Http\Controllers\Services\PageController;

/* HTTP Requests */
use Illuminate\Http\Request;

/* Model Includes */
use App\Models\Category;
use App\Models\Collection;
use App\Models\Page;
use App\Models\PageBanner;
use App\Models\PageBannerBlock;

/* Custom Helper Functions and Traits */
use App\Helpers\Strings\StringHelper;
use App\Helpers\DateTime\DateHelper;

use App\Traits\SEO\SEOsTraits;
use App\Traits\Sharing\SocialSharing;
use App\Traits\Banners\PageBanners;
use App\Helpers\MiscHelpers;

class HomeController extends PageController
{
    use SEOsTraits;
    use SocialSharing;
    use PageBanners;

    public function __construct()
    {
        parent::__construct();

        $this->data['page'] = Page::find(2);

        $this->strHelper  = new StringHelper();
        $this->dateHelper = new DateHelper();
        $this->miscHelper = new MiscHelpers();

        $this->data['soc_share'] = collect(Parent::_socshr($this->data['page']));
    }
    
    public function __invoke(Request $request)
    {
        $banners = $this->page_banners((new PageBanner), ['page_id'=>$this->data['page']->id], 'blocks');
        $this->data['banners'] = $banners->sortBy('order');
        $this->data['item_sharing'] = $this->setup_sharing($this->data['share_links']);
        unset($this->data['share_links'], $sm, $banners);

        $this->data['page']->title = 'Vault in Laravel 5.8';
        $this->data['seo'] = $this->setup_SEO($this->data['page']);

        $this->data['collection_categories'] = Category::where('category_id', null)->status()->with('categories', 'children', 'collections')->orderBy('order', 'asc')->take(4)->get();

        // dump(get_defined_vars(), $this->data);

        return view('welcome', $this->data);
    }
}
