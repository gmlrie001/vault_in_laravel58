@if($page->featured_image != null)
    <div class="container">
        <div class="row">
            <img src="/{{$page->featured_image}}" class="img-fluid" />
        </div>
    </div>
@endif