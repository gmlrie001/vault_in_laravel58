
      <script type="text/javascript" async defer src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&amp;render=explicit"></script>
      <script type="text/javascript">
        var onloadCallback = function () {
          try {
            clientId1 = grecaptcha.render(
              document.getElementById('contact-captcha'), {
                'sitekey': '6Lcqp8MUAAAAABBS-OtTvC4spixwmGx5QX6UjRI1',
                'theme': 'light'
              }
            );
            clientId2 = grecaptcha.render(
              document.getElementById('product-captcha'), {
                'sitekey': '6Lcqp8MUAAAAABBS-OtTvC4spixwmGx5QX6UjRI1',
                'theme': 'light'
              }
            );
            clientId3 = grecaptcha.render(
              document.getElementById('platinum-captcha'), {
                'sitekey': '6Lcqp8MUAAAAABBS-OtTvC4spixwmGx5QX6UjRI1',
                'theme': 'light'
              }
            );
          } catch ( err ) {
            console.warn("\r\nError with Google ReCaptcha Rendering Callback"+ err + "\r\n");
          }
        }
        if ( document.addEventListener ) {
          document.addEventListener( 'DOMContentLoaded', function(){
            onloadCallback()
            document.removeEventListener( 'DOMContentLoaded', function(evt){} );
            return;
          });
        } else if ( document.attachEvent ) {
          document.attachEvent('onreadystatechange', function(){
            if(document.readyState === "complete") {
              onloadCallback()
              document.detachEvent( 'onreadystatechange', function(evt){} );
              return;
            }
          });
        }
      </script>
