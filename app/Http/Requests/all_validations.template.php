<?php

return [

  'create' => [

    'email' => [
      'rules'=>'required|email|exists:users,email|unique:users',
      'messages'=>[]
    ],

    'password' => [
      'rules'=>'required|min:8',
      'messages'=>[]
    ],

    'password_confirm' => [
      'rules'=>'required|min:8|confirmed|same:password',
      'messages'=>[]
    ],

    'article_image' => [
      'rules'=>'image|distinct|dimensions:ratio=4/3|dimensions:min_width=400,min_height=300',
      'messages'=>[]
    ],

    'name' => [
      'rules'=>'required|string',
      'messages'=>[]
    ],

    'user_group_id' => [
      'rules'=>'required|numeric',
      'messages'=>[]
    ],

    'user_group' => [
      'rules'=>'required|numeric',
      'messages'=>[]
    ],

    'my_name'   => [
      'rules'=>'honeypot',
      'messages'=>[]
    ],

    'my_time'   => [
      'rules'=>'required|honeytime:5',
      'messages'=>[]
    ],

    'seo_keywords' => [
      'rules' => 'regex:/^(.+,?.+)?$/isgU', // Comma separated words/strings
      'messages' => []
    ],

    'external_link' => [
      'rules' => 'url',
      'messages' => []
    ],

  ],

  'update' => [
    'id'    => $id = $this->request->get("id"),

    'email' => [
      'rules'=>'required|email|unique:users,email,'.$id,
      'messages'=>[]
    ],

    'password' => [
      'rules'=>'required|min:8',
      'messages'=>[]
    ],

    'name' => [
      'rules'=>'required',
      'messages'=>[]
    ],

    'user_group_id' => [
      'rules'=>'required|numeric',
      'messages'=>[]
    ],

    'user_group' => [
      'rules'=>'required|numeric',
      'messages'=>[]
    ],

    'my_name'   => [
      'rules'=>'honeypot',
      'messages'=>[]
    ],

    'my_time'   => [
      'rules'=>'required|honeytime:5',
    'messages'=>[]
    ],
    
  ]

];
