@extends( 'layouts.page' )

@section( 'title', 'Vault' . " :: " . 'Database SETUP' )

@push( 'pageStyles' )
<style>
main * {
  transition: all 0.618s !important;
  transition-timing-function: ease;
}
.page-container {
  display: grid;
  grid-template-areas:
    "header top_navigation"
    "sidebar_navigation main_contain"
    "footer footer";
  grid-template-columns: 15vw 85vw;
  grid-template-rows: 10vh 90vh 0;
}
header,
header.header {
  grid-area: header;
  grid-column: 1;
  background-color: red;
}
nav {
  grid-area: top_navigation;
  grid-column: 2;
  background-color: grey;
  padding: calc( ( 10vh - 30px ) * 0.5 );
}
nav ul,
nav ul li a {
  line-height: 30px;
  padding: 0 !important;
}
main aside {
  grid-area: sidebar_navigation;
  grid-column: 1;
  background-color: black;
}
main .vault-content {
  grid-area: main_contain;
  grid-column: 2;
  background-color: white;
}
main .container.vault-content {
  top: 0;
  right: 0;
  left: 0;
  bottom: 0;
  max-width: 65%;
  max-height: 90vh;
  height: auto;
  margin: auto;
}
footer {
  grid-area: footer;
}
.card {
  border-color: #000000;
}
.card.card-outline-secondary .card-header {
  background-color: #000;
  color: #fff;
}

@media only screen and (max-width: 576px) {
  .page-container {
    grid-template-areas:
    "header"
    "top_navigation"
    "sidebar_navigation"
    "main_contain";
  grid-template-columns: auto;
  grid-template-rows: auto auto auto auto;
  }
}
</style>
@endpush

@section( 'content' )
<section class="container vault-content m-auto">
  <div class="row">
    <div class="col-md-6 offset-md-3">
      <span class="anchor" id="formRegister"></span>
      <hr class="mb-5">
    <!-- form card register -->
      <div class="card card-outline-secondary">
        <div class="card-header">
          <h3 class="mb-0">Vault Database Setup On:<br><?php echo($_SERVER['HTTP_HOST']); ?> (localhost)</h3>
        </div>
        <div class="card-body">
          <form class="form" role="form" method="POST" action="{{ route('setup_test') }}" autocomplete="off">
            <input type="hidden" name="_token" class="hidden collapse d-none" value="<?php echo csrf_token(); ?>">
            <div class="form-group">
              <label for="sitename">Sitename (Name of site)</label>
              <input type="text" name="sitename" class="form-control" id="sitename" placeholder="TheVault" required="">
            </div>
            <div class="form-group">
              <label for="hostname">Hostname (if accessing remote server DB)</label>
              <input type="text" name="hostname" class="form-control" id="hostname" placeholder="localhost (127.0.0.1)" value="127.0.0.1" readonly autocapitalization="off">
            </div>
            <div class="form-group">
              <label for="dbname">Database Name *</label>
              <input type="text" name="dbname" class="form-control" id="dbname" placeholder="Database Name" value="vault_lara58" autocapitalization="off" required="">
            </div>
            <div class="form-group">
              <label for="username">Username *</label>
              <input type="text" name="username" class="form-control" id="username" placeholder="Username" value="root" autocapitalization="off" required="">
            </div>
            <div class="form-group">
              <label for="password">Password *</label>
              <input type="password" class="form-control" id="password" placeholder="Password" value="" autocapitalization="off">
            </div>
            <div class="form-group">
              <label for="password">Verify Password *</label>
              <input type="password" class="form-control" id="password" placeholder="Verify Password" value="" autocapitalization="off">
            </div>
            <div class="collapse d-none hidden" hidden>
              {!! Honeypot::generate('my_name', 'my_time') !!}
            </div>
            @isset( $errors )
            @if($errors->any())
            <div class="alert alert-danger">
              <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
              </ul>
            </div>
            @endif
            @endisset
            <div class="form-group clearfix" style="margin-bottom: 0;">
              <button type="submit" class="btn btn-success btn-lg float-right" style="margin-bottom: 0;">Setup Database</button>
            </div>
          </form>
        </div>
      </div><!-- /form card register -->
      <hr class="mb-5">
    </div>
  </div>
</section>
@endsection
