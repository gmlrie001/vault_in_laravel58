<?php

namespace App\Helpers\GoogleFonts;

class GoogleFontsHelper
{
    public $font_json_path;// = 'assets/json/google_fonts.json';
    public $is_public;
    public $modified;
    public $needs_api_call = false;
    public $api_base_url = 'https://www.googleapis.com/webfonts/v1/webfonts';
    public $api_key;

    public function __construct(
        $path='assets/json/test.json',
        $apiKey='',
        $public=true
    ) {
        $this->font_json_path = ($path != null) ? $path : null;
        $this->set_api_key($apiKey);

        $this->is_public      = $public;
        if ($this->is_public) {
            $this->fontPath = public_path(ltrim($this->font_json_path, '/'));
        } else {
            $this->fontPath = '/' . ltrim($this->font_json_path, '/');
        }
        // dd(get_defined_vars(), $this->api_key);
        return $this;
    }

    public function get_localPath()
    {
        return $this->font_json_path;
    }

    public function set_localPath($pth)
    {
        $this->font_json_path = $pth;

        return $this;
    }

    public function font_file_exists()
    {
        return file_exists($this->fontPath);
    }

    public function get_contents()
    {
        return ($this->font_file_exists()) ? file_get_contents($this->fontPath) : null;
    }

    public function last_modified()
    {
        $stats = stat($this->fontPath);
        $this->modified = ($stats['mtime']) ? $stats['mtime'] : null;

        return $this;
    }

    public function get_api_key()
    {
        return $this->api_key;
    }

    public function set_api_key($key)
    {
        $this->api_key = $key;

        return $this;
    }

    public function fetch_from_api()
    {
        $font_file_path = public_path('assets/json/test.json');
        $build_api_url  = [
            'base'  => $this->api_base_url,
            'query' => [ 'key' => $this->api_key, 'sort' => 'alpha' ],
        ];

        $api_url  = $build_api_url['base'] . '?' . http_build_query($build_api_url['query']);
        $fonts    = file_get_contents($api_url);
        $fontData = $fonts;

        try {
            if (! file_exists($font_file_path)) {
                $fhandle = fopen($font_file_path, 'w');
                fwrite($fhandle, $fontData);
                fclose($fhandle);
            }/* else {
                return;
            }*/
        } catch (\Exception $error) {
            $log = $error;
            return;
        }

        unset($api_url, $fonts, $fontData);
        echo("\r\nRetrieved Updated Google Fonts from the API.\r\n");

        return $this;
    }

    private function writeJSON($data)
    {
        if ($data == null) {
            return;
        }
    }
}
