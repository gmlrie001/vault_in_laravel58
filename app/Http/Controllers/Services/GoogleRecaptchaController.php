<?php namespace App\Http\Controllers\Services;

/* Base Controller Include */
use App\Http\Controllers\Controller;

/* Facade Includes */
//use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Log;

class GoogleRecaptchaController {

    public function _GReCaptchaComms( $request_all, $options=array() ) {
        // Google ReCaptcha Comms

		// Extract the client ip and g-recaptcha-response if it exists
		$_ip = request()->getClientIp();
		$_gr = $request_all['g-recaptcha-response'];

		// Setup the url, number of POSTS and the POSTS DATA array
		$url = 'https://www.google.com/recaptcha/api/siteverify';
		$post_data = [
			'secret'   => env( 'RECAPTCHA_SECRET_KEY', false ),
			'remoteip' => $_ip,
			'response' => $_gr,
		];

		// Default CURL options
		$defaults = array(
			CURLOPT_POST           => 1,
			CURLOPT_HEADER         => 0,
			CURLOPT_URL            => $url,
			CURLOPT_FRESH_CONNECT  => 1,
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_FORBID_REUSE   => 1,
			CURLOPT_TIMEOUT        => 4,
			CURLOPT_POSTFIELDS     => http_build_query( $post_data ),
		);
		// Additional options
		$options  = [
			CURLOPT_USERAGENT      => 'MonzamediaGoogleRecaptcha/1.0 (+' . $_SERVER['HTTP_REFERER'] . ') Laravel/5',
		];

		// Initialize and set the options
		$ch = curl_init();
		curl_setopt_array( $ch, ( $defaults + $options ) );

		// POST Request
		if ( ! $result = curl_exec( $ch ) ) {
			//Log::warning( '' );
			Log::error( 'ERROR: Google ReCaptcha CURL Error: ' . curl_error( $ch ) . "\r\n" );
        	trigger_error( curl_error( $ch ) );
		}

		$ch_resp = [
			'response_code' => curl_getinfo( $ch,    CURLINFO_HTTP_CODE ),
			'content_type'  => curl_getinfo( $ch, CURLINFO_CONTENT_TYPE ),
		];

		curl_close( $ch );

		Log::info( 'INFO: Google ReCaptcha Success. Client IP: ' . $_ip . ' and email set as: ' . $request_all['email'] . "\r\n" );

		return $this->_handleGRResponse( $result, $ch_resp )['success'];
	}

	private function _handleGRResponse( $result, $response ) {
		if ( ( $response['response_code'] !== 200 && ! $response['content_type'] ) ) return;

		try {
			if ( preg_match( '/json/iU', $response['content_type'], $m ) ) {
				$handler = json_decode( $result );
				$success = $handler->success;
			}
		} catch( Exception $e ) {
			echo 'Caught exception: ',  $e->getMessage(), "\n";
			$handler = [];
			$success = false;
		}

		return [ 'success' => $success, 'handler' => $handler ];
	}
    
}
