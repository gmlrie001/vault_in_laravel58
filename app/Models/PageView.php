<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PageView extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'page_url', 
        'visitor_ip', 
        'countryCode', 
        'regionName', 
        'cityName', 
        'zipCode'
    ];
}
