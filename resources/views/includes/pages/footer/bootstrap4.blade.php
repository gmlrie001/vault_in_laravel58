<div class="container-fluid formRequest">
  <div class="row">
    <div class="container px-4 px-sm-3">
      <div class="row title">
        <h3 class="textWhite">Join 100's of business leaders growing their brand with the help of The Vault. Request a demo.</h1>
      </div>

      <form class="needs-validation" action="{{ route( 'demo_request' ) }}" method="POST">
	{{ csrf_field() }}

        <div class="form-row">
          <div class="col-12 col-md-3 col-lg-3">
            <input name="full_name" type="text" class="form-control" placeholder="Name" required>
          </div>
          <div class="col-12 col-md-3 col-lg-3">
            <input name="email" type="email" class="form-control" placeholder="Email" required>
          </div>
          <div class="col-12 col-md-3 col-lg-3">
            <input name="number" type="text" class="form-control" placeholder="Contact number">
          </div>

	  {!! Honeypot::generate( 'my_name', 'my_time' ) !!}

          <div class="col-12 col-md-3 col-lg-3">
            <button class="btn btnSubmit" type="submit">Request a demo</button>
          </div>
        </div>
      </form>

    </div>
  </div>
</div>

<footer class="container-fluid d-none d-sm-block">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-12 col-lg-auto m-auto text-center py-3 py-lg-0 order-0">
          <p>&copy 2019 The Vault. All Rights Reserved.</p>
        </div>
        <div class="col-12 col-lg-auto mx-auto text-center py-3 py-lg-0 order-12">
          <img class="img-fluid" src="/backup_html/images/assets/country.png" alt="Countries we serve">
        </div>
        <div class="col-12 col-lg-auto m-auto text-lg-right text-center py-3 py-lg-0 order-lg-12">
          <a class="d-inline" href="/terms-of-use">Terms of use</a>
          <a class="d-inline" href="/privacy-policy">Privacy Policy</a>
        </div>
      </div>
    </div>
  </div>
</footer>

<footer class="container-fluid d-block d-sm-none">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-12 text-center links">
          <a class="d-block text-center" href="/terms-of-use">Terms of use</a>
          <a class="d-block text-center" href="/privacy-policy">Privacy Policy</a>
        </div>
        <div class="col-12 d-block text-center copy">
          <p>&copy 2019 The Vault. All Rights Reserved.</p>
        </div>
        <div class="mx-auto">
          <img class="img-fluid" src="/backup_html/images/assets/country.png" alt="Countries we serve">
        </div>
      </div>
    </div>
  </div>
</footer>
