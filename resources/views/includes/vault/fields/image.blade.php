<span class="small">{{$label}} Image</span>
<p class="small">Recommended Size: {{$width}}px x {{$height}}px</p>
<p class="small">Max File Size: 2MB</p>
    <div class="col-xs-12 padding-0 upload-image">
        <label class="upload-button">
            <i class="fa fa-upload" aria-hidden="true"></i> Upload presized
            {!! Form::file($name."_upload", ['id' => $name."_upload"]) !!}
        </label>
        <label id="upload_{{$name}}" class="upload-button"><i class="fa fa-crop" aria-hidden="true"></i> Crop image to size</label>
        <script>
            $('{{"#".$name."_upload"}}').bind('change', function() {
                if( this.files[0].size / ( 1024 * 1024 ) > 2 ) {
                    $('{{"#".$name."_upload"}}').replaceWith($('{{"#".$name."_upload"}}').clone());
                    
                    $.notify("Files cannot exceed 2MB!", {
                        autoHide: false, 
                        position: 'right bottom', 
                        className: 'error', 
                        gap: 5
                    });
                }else{
                    var image = this.files[0];
                    var form = new FormData();
                    form.append('image', image);
                    form.append("_token", $('input[name="_token"]').val());
                    
                    $.ajax({
                        url: '/vault/image/upload/{{$name}}',
                        data: form,
                        cache: false,
                        contentType: false,
                        processData: false,
                        type: 'POST',
                        success:function(response) {
                            $('img.img_preview_{{$name}}').attr('src', '/'+response.location);
                            $('img.img_preview_{{$name}}').removeClass( 'no-img' ).addClass( 'img' );
                            $("{{'#myOutput_'.$name}}").val(response.location);
                        }, 
                        error:function(response){
                            $.notify("File could not be uploaded!", {
                                autoHide: false, 
                                position: 'right bottom', 
                                className: 'error', 
                                gap: 5
                            });
                        }
                    });
                }
            });
        </script>
    </div>
    <div class="col-xs-12 padding-0 cropping-block">
        <style>
            #your_{{$name}} {
                width: {{$width}}px;
                height: {{$height}}px;
                position:relative;
            }
        </style>
        <div id="your_{{$name}}" class="adaptable-height hidden"></div>
        <script>
            var cropperOptions_{{$name}} = {
                modal:true, 
                customUploadButtonId:'upload_{{$name}}', 
                uploadUrl:'/vault/temp/image/upload', 
                rotateControls: false, 
                uploadData:{
                    "_token" : $('input[name="_token"]').val()
                }, 
                cropUrl:'/vault/image/crop/{{$name}}', 
                cropData:{
                    "_token" : $('input[name="_token"]').val()
                }, 
                outputUrlId:'myOutput_{{$name}}', 
                onAfterImgCrop: function(){
                    $('#your_{{$name}} img.croppedImg').attr('src', '/'+$('#your_{{$name}} img.croppedImg').attr('src'));
                    $('img.img_preview_{{$name}}').attr('src', $('#your_{{$name}} img.croppedImg').attr('src'));
                }
            }

            var cropperHeader_{{$name}} = new Croppic('your_{{$name}}', cropperOptions_{{$name}});
        </script>
    </div>
    {!! Form::text($name, null, array('class' => 'hidden', 'id' => 'myOutput_'.$name)) !!}
    <div class="col-xs-12 form-group padding-0">
        {!! Form::label('image_preview', 'Image Preview') !!}
        <img class="img-responsive img_preview_{{$name}} {{ ( $value != null ) ? 'img' : 'no-img' }}" src="/{{(($value != null) ? $value : 'assets/images/vault/template/no_img.jpg' )}}" />
        @if ( $method == 'update' && $can_remove == true && $value != null )
            <a href="/vault/image/remove/{{$table}}/{{$name}}/{{$entry->id}}" class="remove-image btn"><i class="fa fa-trash"></i> Remove image</a>
        @endif
    </div>
