<div id="ohsnap"></div>
<script>
	@if(Session::has('message'))
		$.notify("{{ Session::get('message')}}!", {
			autoHide: false, 
			position: 'right bottom', 
			className: 'success', 
			gap: 5
		});
		{{ Session::forget('message')}}
	@endif
	@if(Session::has('error'))
		$.notify("{{ Session::get('error')}}!", {
			autoHide: false, 
			position: 'right bottom', 
			className: 'error', 
			gap: 5
		});
		{{ Session::forget('error')}}
	@endif
	@if ($errors->any())
		@foreach ($errors->all() as $error)
			@if($error != "")
				$.notify("{{ $error }}", {
					autoHide: false, 
					position: 'right bottom', 
					className: 'error', 
					gap: 5
				});
			@endif
		@endforeach
	@endif
</script>