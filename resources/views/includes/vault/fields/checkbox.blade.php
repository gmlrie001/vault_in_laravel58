{!! Form::label($name, $label) !!}
    @if($option == $value)
        {!! Form::checkbox($name, $option, true, ['class' => 'checkbox_'.$name]); !!}
    @else
        {!! Form::checkbox($name, $option, false, ['class' => 'checkbox_'.$name]); !!}
    @endif
<script>
    $('.checkbox_{{$name}}').iCheck({
        checkboxClass: 'icheckbox_flat',
        radioClass: 'iradio_flat'
    });
</script>